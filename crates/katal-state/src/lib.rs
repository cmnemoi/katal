use async_trait::async_trait;
use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};
use std::time::Duration;

pub mod sqlite;

/// A key may be exchanged for tokens
#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub struct IdempotencyKey {
  /// Unique name identifying this key
  pub name: String,
  /// Min duration between two successful token creations for this key.
  ///
  /// `None` means "infinite" (never reuse a key).
  pub expiry: Option<Duration>,
}

/// A token granting the right to run an action.
///
/// The token ensures the action is only executed once.
///
/// This also acts like a lock guard.
#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub struct IdempotencyToken {
  pub secret: [u8; 32],
  pub creation_time: DateTime<Utc>,
}

pub mod command {
  use crate::IdempotencyKey;
  use serde::{Deserialize, Serialize};

  #[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
  pub struct AcquireIdempotencyToken {
    key: IdempotencyKey,
  }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub enum StateCommand {
  AcquireIdempotencyToken(command::AcquireIdempotencyToken),
}

#[async_trait]
pub trait StateStore {
  async fn acquire_idempotency_token(&self, key: &IdempotencyKey) -> Result<Option<IdempotencyToken>, String>;
}

#[async_trait]
impl StateStore for () {
  async fn acquire_idempotency_token(&self, _key: &IdempotencyKey) -> Result<Option<IdempotencyToken>, String> {
    Err("()::acquire_idempotency_token is only a stub".to_string())
  }
}
