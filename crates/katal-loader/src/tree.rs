use async_trait::async_trait;
use chrono::{DateTime, NaiveDateTime, TimeZone, Utc};
use itertools::Itertools;
use once_cell::sync::Lazy;
use regex::Regex;
use reqwest::Client;
use scraper::{Html, Selector};
use serde_json::{Map as JsonMap, Value as JsonValue};
use std::error::Error as StdError;
use std::fmt::Debug;
use std::future::Future;
use std::io;
use std::io::{Cursor, Read};
use std::path::{Component, Path, PathBuf};
use std::pin::Pin;
use thiserror::Error;
use url::Url;

pub enum ChildrenError {
  NotFound,
}

#[derive(Debug, Error)]
pub enum ReadError {
  #[error("some path component does not exist")]
  NotFound,
  #[error("other error")]
  Other(#[source] Box<dyn StdError + Send + Sync>),
}

#[derive(Debug, Error)]
pub enum ReadDirError {
  #[error("some path component does not exist")]
  NotFound,
  #[error("other error")]
  Other(#[source] Box<dyn StdError + Send + Sync>),
}

pub trait FsxDirEntry<Path> {
  fn path(&self) -> &Path;
  fn file_type(&self) -> FileType;
}

/// Minimal directory entry
#[derive(Debug, Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct SimpleDirEntry<PathBuf> {
  pub path: PathBuf,
  pub file_type: FileType,
}

impl<Path> FsxDirEntry<Path> for SimpleDirEntry<Path> {
  fn path(&self) -> &Path {
    &self.path
  }

  fn file_type(&self) -> FileType {
    self.file_type
  }
}

#[derive(Debug, Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum FileType {
  File,
  Dir,
  Symlink,
  Other,
}

trait FsxPath<Segment>: IntoIterator<Item = Segment> {}

trait FsxPathBuf<Segment> {
  fn push(&mut self, segment: Segment);
}

/// Read-only Abstract File System
#[async_trait]
pub trait ReadFsx<Path: ?Sized> {
  /// Owned type representing a path
  type PathBuf: AsRef<Path>;
  /// Owned directory entry
  type DirEntry: FsxDirEntry<Self::PathBuf>;

  async fn read<P: AsRef<Path> + Send + Sync>(&self, path: P) -> Result<Vec<u8>, ReadError>;
  async fn read_dir<P: AsRef<Path> + Send + Sync>(&self, path: P) -> Result<Vec<Self::DirEntry>, ReadDirError>;
}

pub struct GitlabRepo {
  base: Url,
}

#[async_trait]
impl<'a> ReadFsx<[&'a str]> for GitlabRepo {
  type PathBuf = [&'a str; 1];
  type DirEntry = SimpleDirEntry<Self::PathBuf>;

  async fn read<P: AsRef<[&'a str]> + Send + Sync>(&self, path: P) -> Result<Vec<u8>, ReadError> {
    let _url = {
      let mut url = self.base.clone();
      url.path_segments_mut().unwrap().extend(path.as_ref().iter());
      url
    };
    todo!()
  }

  async fn read_dir<P: AsRef<[&'a str]> + Send + Sync>(&self, _path: P) -> Result<Vec<Self::DirEntry>, ReadDirError> {
    todo!()
  }
}

pub struct TarGz {
  /// Dezipped tar content (decompression occurs in the constructor)
  archive: Cursor<Vec<u8>>,
  /// The `TarGz` tree auto-detects if there's a singe directory at the root
  root_dir: Option<String>,
}

#[derive(Debug, Error)]
pub enum TarGzError {
  #[error("io error")]
  Io(#[from] io::Error),
  #[error("invalid entry name")]
  InvalidEntryName,
}

impl TarGz {
  pub fn new(tgz: &[u8]) -> Result<Self, TarGzError> {
    let mut stream = flate2::read::GzDecoder::new(tgz);
    let mut tar: Vec<u8> = Vec::new();
    stream.read_to_end(&mut tar)?;
    let tar = Cursor::new(tar);

    let mut archive = tar::Archive::new(tar.clone());
    let mut root_dir = None;
    let mut has_multiple_entries_at_root = false;
    let mut has_multiple_entries = false;
    let entries = archive.entries_with_seek()?;
    for e in entries {
      let e = e?;
      let p = e.path()?;
      match p.components().next() {
        Some(Component::Normal(segment)) => {
          let segment = segment.to_str().ok_or(TarGzError::InvalidEntryName)?;
          match root_dir.as_deref() {
            Some(r) => {
              has_multiple_entries = true;
              if r != segment {
                has_multiple_entries_at_root = true;
              }
            }
            None => root_dir = Some(segment.to_string()),
          }
        }
        _ => has_multiple_entries_at_root = true,
      }
    }
    if has_multiple_entries_at_root || !has_multiple_entries {
      root_dir = None;
    }

    Ok(Self { archive: tar, root_dir })
  }
}

#[derive(Debug, Error)]
enum ComponentError {
  #[error("found non-normal component")]
  NotNormal,
  #[error("non-utf8 component")]
  Utf8,
}

trait PathExt {
  fn eq_utf8_segments<'a>(&self, segments: impl Iterator<Item = String>) -> Result<bool, ComponentError>;
}

impl<P> PathExt for P
where
  P: AsRef<Path>,
{
  fn eq_utf8_segments<'a>(&self, segments: impl Iterator<Item = String>) -> Result<bool, ComponentError> {
    let path = self.as_ref();
    let mut components = path.components().map(|c| match c {
      Component::Normal(c) => c.to_str().ok_or(ComponentError::Utf8),
      _ => Err(ComponentError::NotNormal),
    });
    for segment in segments {
      match components.next().transpose()? {
        Some(c) if c == segment => continue,
        _ => return Ok(false),
      }
    }
    Ok(components.next().transpose()?.is_none())
  }
}

#[async_trait]
impl ReadFsx<[String]> for TarGz {
  type PathBuf = Vec<String>;
  type DirEntry = SimpleDirEntry<Self::PathBuf>;

  async fn read<P: AsRef<[String]> + Send + Sync>(&self, path: P) -> Result<Vec<u8>, ReadError> {
    let path = path.as_ref();
    let path = self.root_dir.as_ref().into_iter().chain(path.iter()).cloned();
    let mut archive = tar::Archive::new(self.archive.clone());

    let entries = archive.entries_with_seek().map_err(|e| ReadError::Other(Box::new(e)))?;
    for e in entries {
      let mut e = e.map_err(|e| ReadError::Other(Box::new(e)))?;
      let p = e.path().map_err(|e| ReadError::Other(Box::new(e)))?;
      let is_file = p
        .eq_utf8_segments(path.clone())
        .map_err(|e| ReadError::Other(Box::new(e)))?;
      if is_file {
        let mut body: Vec<u8> = Vec::new();
        e.read_to_end(&mut body).map_err(|e| ReadError::Other(Box::new(e)))?;
        return Ok(body);
      }
    }
    Err(ReadError::NotFound)
  }

  async fn read_dir<P: AsRef<[String]> + Send + Sync>(&self, path: P) -> Result<Vec<Self::DirEntry>, ReadDirError> {
    let base_path = path.as_ref();
    let path = self.root_dir.as_ref().into_iter().chain(base_path.iter()).cloned();
    let mut archive = tar::Archive::new(self.archive.clone());

    let mut result: Vec<SimpleDirEntry<Self::PathBuf>> = Vec::new();
    let mut found_self = false;

    let entries = archive
      .entries_with_seek()
      .map_err(|e| ReadDirError::Other(Box::new(e)))?;
    'entries: for e in entries {
      let e = e.map_err(|e| ReadDirError::Other(Box::new(e)))?;
      let entry_path = e.path().map_err(|e| ReadDirError::Other(Box::new(e)))?;
      let mut entry_components = entry_path.components().map(|c| match c {
        Component::Normal(c) => c.to_str().ok_or(ComponentError::Utf8),
        _ => Err(ComponentError::NotNormal),
      });
      for main_component in path.clone() {
        let entry_component = entry_components
          .next()
          .transpose()
          .map_err(|e| ReadDirError::Other(Box::new(e)))?;
        if entry_component != Some(main_component.as_str()) {
          continue 'entries;
        }
      }
      // At this point `path` is a prefix or equal to `entry_path`
      let entry_name = entry_components
        .next()
        .transpose()
        .map_err(|e| ReadDirError::Other(Box::new(e)))?;
      let child_path = match entry_name {
        None => {
          // The entry is the base path itself
          found_self = true;
          continue 'entries;
        }
        Some(entry) => {
          if entry_components.next().is_some() {
            continue 'entries; // There are some deeper components, this indicates a nested directory
          }
          let mut child_path = Vec::new();
          child_path.extend(base_path.iter().cloned());
          child_path.push(entry.to_string());
          child_path
        }
      };
      let is_dir = entry_path.to_str().map(|p| p.ends_with('/')).unwrap_or(false);
      result.push(SimpleDirEntry {
        path: child_path,
        file_type: if is_dir { FileType::Dir } else { FileType::File },
      });
    }
    if !found_self {
      return Err(ReadDirError::NotFound);
    }
    Ok(result)
  }
}

// TODO: Provide implementation based on `cap-std`

#[derive(Debug, Clone)]
pub struct LocalFs {
  start: PathBuf,
}

impl LocalFs {
  pub fn new(base: PathBuf) -> Self {
    Self { start: base }
  }
}

#[async_trait]
impl ReadFsx<Path> for LocalFs {
  type PathBuf = PathBuf;
  type DirEntry = SimpleDirEntry<Self::PathBuf>;

  async fn read<P: AsRef<Path> + Send + Sync>(&self, path: P) -> Result<Vec<u8>, ReadError> {
    match std::fs::read(self.start.join(path)) {
      Ok(data) => Ok(data),
      Err(e) if e.kind() == io::ErrorKind::NotFound => Err(ReadError::NotFound),
      Err(e) => Err(ReadError::Other(Box::new(e))),
    }
  }

  async fn read_dir<P: AsRef<Path> + Send + Sync>(&self, _path: P) -> Result<Vec<Self::DirEntry>, ReadDirError> {
    todo!()
  }
}

#[async_trait]
impl ReadFsx<[String]> for LocalFs {
  type PathBuf = Vec<String>;
  type DirEntry = SimpleDirEntry<Self::PathBuf>;

  async fn read<P: AsRef<[String]> + Send + Sync>(&self, path: P) -> Result<Vec<u8>, ReadError> {
    let mut p = self.start.clone();
    for segment in path.as_ref() {
      p.push(segment)
    }

    match std::fs::read(&p) {
      Ok(data) => Ok(data),
      Err(e) if e.kind() == io::ErrorKind::NotFound => Err(ReadError::NotFound),
      Err(e) => Err(ReadError::Other(Box::new(e))),
    }
  }

  async fn read_dir<P: AsRef<[String]> + Send + Sync>(&self, path: P) -> Result<Vec<Self::DirEntry>, ReadDirError> {
    let mut base: Vec<String> = Vec::new();
    let mut p = self.start.clone();
    for segment in path.as_ref() {
      base.push(segment.to_string());
      p.push(segment)
    }
    let read_dir = match std::fs::read_dir(&p) {
      Ok(read_dir) => read_dir,
      Err(e) if e.kind() == io::ErrorKind::NotFound => return Err(ReadDirError::NotFound),
      Err(e) => return Err(ReadDirError::Other(Box::new(e))),
    };
    let mut entries = Vec::new();
    for entry in read_dir {
      let entry = entry.unwrap();
      let segment = entry.file_name().to_str().unwrap().to_string();
      let mut path_buf = base.clone();
      let meta = entry.metadata().unwrap();
      let file_type = if meta.is_symlink() {
        FileType::Symlink
      } else if meta.is_dir() {
        FileType::Dir
      } else if meta.is_file() {
        FileType::File
      } else {
        FileType::Other
      };
      path_buf.push(segment);
      entries.push(SimpleDirEntry {
        path: path_buf,
        file_type,
      })
    }
    Ok(entries)
  }
}

#[derive(Debug, Clone)]
pub struct PostgresqlFtp {
  base: Url,
  client: Client,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct PostgresqlFtpDirEntry {
  path: Vec<String>,
  file_type: FileType,
  /// Last update time, 1 minute precision
  pub time: DateTime<Utc>,
  /// If the entry is a file, file size
  pub file_size: Option<u64>,
}

impl FsxDirEntry<Vec<String>> for PostgresqlFtpDirEntry {
  fn path(&self) -> &Vec<String> {
    &self.path
  }

  fn file_type(&self) -> FileType {
    self.file_type
  }
}

impl PostgresqlFtp {
  pub fn new(client_name: String) -> Self {
    Self {
      base: Url::parse("https://ftp.postgresql.org/pub/").unwrap(),
      client: Client::builder()
        .user_agent(client_name)
        .build()
        .expect("building the PostgresqlFtp client should always succeed"),
    }
  }

  fn resolve_path(&self, path: &[String], trailing_slash: bool) -> Url {
    let mut result = self.base.clone();
    {
      let mut segments = result.path_segments_mut().expect("base URL should support segments");
      segments.extend(path);
      if trailing_slash {
        segments.push("");
      };
    }
    result
  }

  fn read_directory_index(base: &[String], body: &str) -> Result<Vec<PostgresqlFtpDirEntry>, ReadDirError> {
    /// Regular expression to extract the component name and trailing slash from a link
    static LINK_RE: Lazy<Regex> = Lazy::new(|| Regex::new(r#"([^/]+)(/)?"#).unwrap());
    /// Regular expression to extract the update time and file size
    static META_RE: Lazy<Regex> = Lazy::new(|| {
      Regex::new(r#"(\d{2}-(?:Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)-\d{4} \d{2}:\d{2})\s+(-|\d+)?"#).unwrap()
    });

    let doc = Html::parse_document(body);
    let root = doc.root_element();
    let entry_list = root
      .select(&Selector::parse("pre").unwrap())
      .exactly_one()
      .expect("non-unique entry list");
    let mut inputs = entry_list.children().peekable();
    let mut outputs = Vec::new();
    while let Some(link) = inputs.next() {
      let link = match link.value().as_element() {
        Some(elem) if elem.name() == "a" => elem,
        _ => continue,
      };
      let href = link.attr("href").expect("anchor link should have href attr");
      let link_groups = LINK_RE.captures(href).expect("unexpected link format");
      let name = link_groups.get(1).expect("group 1 always exists");
      if name.as_str() == ".." {
        continue;
      }
      let trailing_slash = link_groups.get(2).is_some();
      let file_type = if trailing_slash { FileType::Dir } else { FileType::File };
      let meta = inputs
        .peek()
        .and_then(|m| m.value().as_text())
        .expect("missing entry metadata");
      let meta: &str = meta.text.as_ref().trim();
      let meta_groups = META_RE.captures(meta).expect("unexpected metadata format");
      let time = meta_groups.get(1).expect("group 1 always exists").as_str();
      let time = NaiveDateTime::parse_from_str(time, "%d-%b-%Y %H:%M").expect("unexpected time format");
      let time = Utc.from_utc_datetime(&time);
      let file_size = meta_groups.get(2).expect("group 2 always exists").as_str();
      let file_size = if file_size == "-" {
        None
      } else {
        Some(u64::from_str_radix(file_size, 10).expect("failed to parse file size"))
      };
      if !matches!(
        (file_type, file_size),
        (FileType::Dir, None) | (FileType::File, Some(_))
      ) {
        panic!("unexpected file_type/file_size combination");
      }
      outputs.push(PostgresqlFtpDirEntry {
        path: {
          let mut p = Vec::from(base);
          p.push(name.as_str().to_string());
          p
        },
        file_type,
        time,
        file_size,
      })
    }
    Ok(outputs)
  }
}

#[async_trait]
impl ReadFsx<[String]> for PostgresqlFtp {
  type PathBuf = Vec<String>;
  type DirEntry = PostgresqlFtpDirEntry;

  async fn read<P: AsRef<[String]> + Send + Sync>(&self, path: P) -> Result<Vec<u8>, ReadError> {
    let u = self.resolve_path(path.as_ref(), false);

    let res = self
      .client
      .get(u)
      .send()
      .await
      .expect("sending the request should succeed");
    let body = res.bytes().await.expect("reading the body should succeed");
    Ok(body.to_vec())
  }

  async fn read_dir<P: AsRef<[String]> + Send + Sync>(&self, path: P) -> Result<Vec<Self::DirEntry>, ReadDirError> {
    let path = path.as_ref();
    let u = self.resolve_path(path, true);

    let res = self
      .client
      .get(u)
      .send()
      .await
      .expect("sending the request should succeed");
    let body = res.text().await.expect("reading the body should succeed");

    Self::read_directory_index(path, body.as_str())
  }
}

#[derive(Debug)]
pub enum LoadTargetError {
  Read(Vec<String>, ReadError),
  Format(Vec<String>, serde_json::Error),
  ReadDir(Vec<String>, ReadDirError),
  RecursionLimit(Vec<String>),
  Other,
}

pub async fn load_config<Fs: ReadFsx<[String]>>(fs: &Fs) -> Result<JsonValue, LoadTargetError> {
  let result = load_config_from(fs, &[], 5).await?;
  Ok(result.into())
  // let entries = fs.read_dir(["neoparc".to_string()]).unwrap();
  // for entry in entries {
  //   let content = fs.read(entry.path).unwrap();
  //   dbg!(content.len());
  // }
  //
  // let f = "production.json".to_string();
  // let content = fs.read(["neoparc".to_string(), f]).unwrap();
  // serde_json::from_slice(&content).map_err(drop)
  // // Ok(content.unwrap().len().try_into().unwrap())
}

fn load_config_from<'a, Fs: ReadFsx<[String]>>(
  fs: &'a Fs,
  base: &'a [String],
  recursion_limit: usize,
) -> Pin<Box<dyn Future<Output = Result<JsonMap<String, JsonValue>, LoadTargetError>> + 'a>> {
  Box::pin(async move {
    let mut result = JsonMap::new();
    let entries = fs
      .read_dir(base)
      .await
      .map_err(|e| LoadTargetError::ReadDir(base.to_vec(), e))?;
    for child_dir in entries.iter().filter(|e| e.file_type() == FileType::Dir) {
      let child_path = child_dir.path().as_ref();
      let child_limit = match recursion_limit.checked_sub(1) {
        Some(child_limit) => child_limit,
        None => return Err(LoadTargetError::RecursionLimit(child_path.to_vec())),
      };
      let key = child_path.last().expect("path is non-empty").clone();
      let value = load_config_from(fs, child_path, child_limit).await?;
      result.insert(key, value.into());
    }
    let mut dir_file: Option<&[String]> = None;
    for child_file in entries.iter().filter(|e| e.file_type() == FileType::File) {
      let child_path = child_file.path().as_ref();
      let name = child_path.last().expect("path is non-empty");
      let key = match name.strip_suffix(".json") {
        Some(s) => {
          if s.is_empty() {
            dir_file = Some(child_path);
            continue;
          } else {
            s
          }
        }
        _ => continue,
      };
      let content = fs
        .read(child_path)
        .await
        .map_err(|e| LoadTargetError::Read(child_path.to_vec(), e))?;
      let value: JsonValue =
        serde_json::from_slice(&content).map_err(|e| LoadTargetError::Format(child_path.to_vec(), e))?;
      match result.entry(key.to_string()) {
        serde_json::map::Entry::Occupied(mut e) => merge_value(e.get_mut(), value),
        serde_json::map::Entry::Vacant(e) => e.insert(value),
      };
    }
    if let Some(dir_file) = dir_file {
      let content = fs
        .read(dir_file)
        .await
        .map_err(|e| LoadTargetError::Read(dir_file.to_vec(), e))?;
      let value: JsonMap<String, JsonValue> =
        serde_json::from_slice(&content).map_err(|e| LoadTargetError::Format(dir_file.to_vec(), e))?;
      merge_map(&mut result, value);
    }
    Ok(result)
  })
}

fn merge_value(old: &mut JsonValue, new: JsonValue) -> &mut JsonValue {
  match (&mut *old, new) {
    (JsonValue::Object(old_map), JsonValue::Object(new_map)) => {
      merge_map(old_map, new_map);
    }
    (old, new) => *old = new,
  }
  old
}

fn merge_map(old: &mut JsonMap<String, JsonValue>, new: JsonMap<String, JsonValue>) -> &mut JsonMap<String, JsonValue> {
  for (key, value) in new {
    match old.entry(key) {
      serde_json::map::Entry::Occupied(mut e) => merge_value(e.get_mut(), value),
      serde_json::map::Entry::Vacant(e) => e.insert(value),
    };
  }
  old
}

#[cfg(test)]
mod test {
  use crate::tree::{FileType, ReadFsx, SimpleDirEntry, TarGz};

  #[tokio::test]
  async fn read_tgz() {
    let bytes = include_bytes!("../../../test-resources/tree/targz/archive.tar.gz");
    let tree = TarGz::new(bytes).unwrap();
    {
      let actual = tree.read(["foo.txt".to_string()]).await.unwrap();
      let expected = b"Hello, World!\n".to_vec();
      assert_eq!(actual, expected);
    }
    {
      let actual = tree.read(["bar".to_string(), "baz.txt".to_string()]).await.unwrap();
      let expected = b"bar/baz\n".to_vec();
      assert_eq!(actual, expected);
    }
    {
      let actual = tree.read_dir([]).await.unwrap();
      let expected = vec![
        SimpleDirEntry {
          path: vec!["foo.txt".to_string()],
          file_type: FileType::File,
        },
        SimpleDirEntry {
          path: vec!["empty".to_string()],
          file_type: FileType::Dir,
        },
        SimpleDirEntry {
          path: vec!["bar".to_string()],
          file_type: FileType::Dir,
        },
      ];
      assert_eq!(actual, expected);
    }
    {
      let actual = tree.read_dir(["bar".to_string()]).await.unwrap();
      let expected = vec![
        SimpleDirEntry {
          path: vec!["bar".to_string(), "baz.txt".to_string()],
          file_type: FileType::File,
        },
        SimpleDirEntry {
          path: vec!["bar".to_string(), "empty.txt".to_string()],
          file_type: FileType::File,
        },
      ];
      assert_eq!(actual, expected);
    }
    {
      let actual = tree.read_dir(["empty".to_string()]).await.unwrap();
      let expected = vec![];
      assert_eq!(actual, expected);
    }
  }
}

//
// // trait FsxPath: Debug {
// //
// // }
//
//
// // https://www.kernel.org/doc/html/latest/filesystems/overlayfs.html
// struct OverlayFs<Upper, Lower> {
//   backends: (Upper, Lower),
// }
//
// impl<Path: Clone, Upper: ReadAfs<Path>, Lower: ReadAfs<Path>> ReadAfs<Path> for OverlayFs<Upper, Lower> {
//   fn read<P: Into<Path>>(&self, path: P) -> Result<Vec<u8>, ReadError> {
//     let p: Path = p.into();
//     match self.backends.0.read(p.clone()) {
//       Err(ReadError::NotFound) => self.backends.1.read(p),
//       res => res,
//     }
//   }
// }
//

//
// #[derive(Debug, Clone)]
// struct ChildFs<Parent, Root> {
//   parent: Parent,
//   root: Root,
// }
//
// impl RootLocalFs {
//   fn resolve<P: Into<Path>>(&self, path: P) -> Result<>
// }
//
// impl ReadAfs<Path> for RootLocalFs {
//   fn read<P: Into<Path>>(&self, path: P) -> Result<Vec<u8>, ReadError> {
//     let p: Path = path.into();
//     let full = self.root.join(&p);
//     full.is_equal_private()
//
//     match std::fs::read(path) {
//       Ok(data) => Ok(data),
//       Err(e) if e.kind() == io::ErrorKind::NotFound => Err(ReadError::NotFound),
//       Err(e) => Err(ReadError::Other(Box::new(e))),
//     }
//   }
// }
//
