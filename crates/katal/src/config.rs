use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::fs;
use std::io;
use std::path::{Path, PathBuf};
use thiserror::Error;

#[derive(Clone, Debug, PartialEq, Eq, Deserialize)]
pub struct KatalConfig {
  pub loader: GitLoaderConfig,
  pub state: SqliteStateConfig,
  pub daemon: DaemonConfig,
  pub config: Option<RawConfig>,
}

#[derive(Clone, Debug, PartialEq, Eq, Deserialize)]
pub struct GitLoaderConfig {
  pub repo: String,
  pub dir: Option<String>,
  pub r#ref: String,
}

#[derive(Clone, Debug, PartialEq, Eq, Deserialize)]
pub struct SqliteStateConfig {
  pub path: String,
}

#[derive(Clone, Debug, PartialEq, Eq, Deserialize)]
pub struct DaemonConfig {
  pub key: String,
}

#[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
pub struct RawConfig {
  pub vault: Option<RawVaultConfig>,
  pub apps: Option<RawAppsConfig>,
}

#[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
pub struct RawAppsConfig {
  pub brute: Option<RawAppConfig<RawBruteChannelConfig>>,
  pub dinorpg: Option<RawAppConfig<RawDinorpgChannelConfig>>,
  pub dinocard: Option<RawAppConfig<RawAutoChannelConfig>>,
  pub emush: Option<RawAppConfig<RawEmushChannelConfig>>,
  pub epopotamo: Option<RawAppConfig<RawEpopotamoChannelConfig>>,
  pub eternalfest: Option<RawAppConfig<RawEternalfestChannelConfig>>,
  pub etwin: Option<RawAppConfig<RawEtwinChannelConfig>>,
  pub frutibandas: Option<RawAppConfig<RawFrutibandasChannelConfig>>,
  pub kadokadeo: Option<RawAppConfig<RawAutoChannelConfig>>,
  pub kingdom: Option<RawAppConfig<RawKingdomChannelConfig>>,
  pub neoparc: Option<RawAppConfig<RawNeoparcChannelConfig>>,
}

#[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
pub struct RawVaultConfig {
  pub sudo_password: Option<String>,
  /// Borgbase API key (to manage _all_ repositories)
  pub borgbase: Option<String>,
  #[serde(default)]
  pub brute: HashMap<String, RawBruteVault>,
  #[serde(default)]
  pub dinocard: HashMap<String, RawAutoVault>,
  #[serde(default)]
  pub dinorpg: HashMap<String, RawDinorpgVault>,
  #[serde(default)]
  pub emush: HashMap<String, RawEmushVault>,
  #[serde(default)]
  pub epopotamo: HashMap<String, RawEpopotamoVault>,
  #[serde(default)]
  pub eternalfest: HashMap<String, RawEternalfestVault>,
  #[serde(default)]
  pub etwin: HashMap<String, RawEtwinVault>,
  #[serde(default)]
  pub kadokadeo: HashMap<String, RawAutoVault>,
  #[serde(default)]
  pub kingdom: HashMap<String, RawKingdomVault>,
  #[serde(default)]
  pub neoparc: HashMap<String, RawNeoparcVault>,
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize, Serialize)]
pub struct RawAutoVault {
  pub borgbase_repo: String,
  pub db_admin_password: String,
  pub db_main_password: String,
  pub db_read_password: String,
  pub secret: String,
  pub etwin_client_secret: String,
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize, Serialize)]
pub struct RawBruteVault {
  pub borgbase_repo: String,
  pub db_admin_password: String,
  pub db_main_password: String,
  pub db_read_password: String,
  pub etwin_client_secret: String,
  pub discord_webhook_token: Option<String>,
  pub logs_webhook_token: Option<String>,
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize, Serialize)]
pub struct RawDinorpgVault {
  pub borgbase_repo: String,
  pub db_admin_password: String,
  pub db_main_password: String,
  pub db_read_password: String,
  pub secret_key: String,
  pub etwin_client_secret: String,
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize, Serialize)]
pub struct RawEtwinVault {
  pub borgbase_repo: String,
  pub db_admin_password: String,
  pub db_main_password: String,
  pub db_read_password: String,
  pub secret_key: String,
  pub brute_production_secret: String,
  pub brute_staging_secret: String,
  pub dinocard_production_secret: String,
  pub dinocard_staging_secret: String,
  pub dinorpg_production_secret: String,
  pub dinorpg_staging_secret: String,
  pub directquiz_secret: String,
  pub emush_production_secret: String,
  pub emush_staging_secret: String,
  pub epopotamo_production_secret: String,
  pub epopotamo_staging_secret: String,
  pub eternalfest_production_secret: String,
  pub eternalfest_staging_secret: String,
  pub kadokadeo_production_secret: String,
  pub kadokadeo_staging_secret: String,
  pub kingdom_production_secret: String,
  pub kingdom_staging_secret: String,
  pub mjrt_secret: String,
  pub myhordes_secret: String,
  pub neoparc_production_secret: String,
  pub neoparc_staging_secret: String,
  pub wiki_secret: String,
  pub twinoid_client_id: String,
  pub twinoid_client_key: String,
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize, Serialize)]
pub struct RawEternalfestVault {
  pub borgbase_repo: String,
  pub db_admin_password: String,
  pub db_main_password: String,
  pub db_read_password: String,
  pub secret_key: String,
  pub cookie_key: String,
  pub etwin_client_secret: String,
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize, Serialize)]
pub struct RawEmushVault {
  pub borgbase_repo: String,
  pub db_admin_password: String,
  pub db_main_password: String,
  pub db_read_password: String,
  pub app_secret: String,
  pub access_passphrase: String,
  pub etwin_client_secret: String,
  pub discord_webhook: Option<String>,
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize, Serialize)]
pub struct RawEpopotamoVault {
  pub borgbase_repo: String,
  pub db_admin_password: String,
  pub db_main_password: String,
  pub db_read_password: String,
  pub etwin_client_secret: String,
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize, Serialize)]
pub struct RawKingdomVault {
  pub borgbase_repo: String,
  pub db_admin_password: String,
  pub db_main_password: String,
  pub db_read_password: String,
  pub app_secret: String,
  pub etwin_client_secret: String,
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize, Serialize)]
pub struct RawNeoparcVault {
  pub borgbase_repo: String,
  pub db_admin_password: String,
  pub db_main_password: String,
  pub db_read_password: String,
  pub salt: String,
  pub seed: String,
  pub test: String,
  pub etwin_client_secret: String,
}

#[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
pub struct RawAppConfig<ChanConfig: ChannelConfig> {
  pub user: Option<String>,
  pub group: Option<String>,
  pub repository: String,
  pub channels: HashMap<String, ChanConfig>,
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize, Serialize)]
pub struct RawAutoChannelConfig {
  pub state: Option<RawStateConfig>,
  pub domain: String,
  pub release: String,
  pub https: Option<RawHttpsConfig>,
  pub main_port: u16,
  pub fallback_port: u16,
  pub etwin_uri: String,
  pub etwin_client_id: String,
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub struct RawBruteChannelConfig {
  pub state: Option<RawStateConfig>,
  pub domain: String,
  pub release: String,
  pub https: Option<RawHttpsConfig>,
  pub main_port: u16,
  pub fallback_port: u16,
  pub etwin_uri: String,
  pub etwin_client_id: String,
  pub discord_webhook_id: Option<String>,
  pub logs_webhook_id: Option<String>,
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub struct RawDinorpgChannelConfig {
  pub state: Option<RawStateConfig>,
  pub domain: String,
  pub release: String,
  pub https: Option<RawHttpsConfig>,
  pub main_port: u16,
  pub fallback_port: u16,
  pub etwin_uri: String,
  pub etwin_client_id: String,
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize, Serialize)]
pub struct RawEmushChannelConfig {
  pub state: Option<RawStateConfig>,
  pub domain: String,
  pub release: String,
  pub https: Option<RawHttpsConfig>,
  pub main_port: u16,
  pub fallback_port: u16,
  pub etwin_uri: String,
  pub etwin_client_id: String,
  pub admin_user_id: Option<String>,
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize, Serialize)]
pub struct RawEpopotamoChannelConfig {
  pub state: Option<RawStateConfig>,
  pub domain: String,
  pub release: String,
  pub https: Option<RawHttpsConfig>,
  pub main_port: u16,
  pub fallback_port: u16,
  pub etwin_uri: String,
  pub etwin_client_id: String,
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize, Serialize)]
pub struct RawFrutibandasChannelConfig {
  pub state: Option<RawStateConfig>,
  pub domain: String,
  pub release: String,
  pub https: Option<RawHttpsConfig>,
  pub main_port: u16,
  pub fallback_port: u16,
  // pub etwin_uri: String,
  // pub etwin_client_id: String,
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub struct RawEternalfestChannelConfig {
  pub state: Option<RawStateConfig>,
  pub domain: String,
  pub release: String,
  pub https: Option<RawHttpsConfig>,
  pub main_port: u16,
  pub fallback_port: u16,
  pub etwin_uri: String,
  pub etwin_client_id: String,
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub struct RawEtwinChannelConfig {
  pub state: Option<RawStateConfig>,
  pub domain: String,
  pub release: String,
  pub https: Option<RawHttpsConfig>,
  pub main_port: u16,
  pub backend_port: u16,
  pub fallback_port: u16,
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize, Serialize)]
pub struct RawKingdomChannelConfig {
  pub state: Option<RawStateConfig>,
  pub domain: String,
  pub release: String,
  pub https: Option<RawHttpsConfig>,
  pub main_port: u16,
  pub fallback_port: u16,
  pub etwin_uri: String,
  pub etwin_client_id: String,
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub struct RawNeoparcChannelConfig {
  pub state: Option<RawStateConfig>,
  pub domain: String,
  pub release: String,
  pub https: Option<RawHttpsConfig>,
  pub main_port: u16,
  pub fallback_port: u16,
  pub etwin_uri: String,
  pub etwin_client_id: String,
}

pub trait ChannelConfig {
  fn state(&self) -> Option<&RawStateConfig>;
}

macro_rules! impl_channel_config {
  ($config:ty) => {
    impl ChannelConfig for $config {
      fn state(&self) -> Option<&RawStateConfig> {
        self.state.as_ref()
      }
    }
  };
}

impl_channel_config!(RawAutoChannelConfig);
impl_channel_config!(RawBruteChannelConfig);
impl_channel_config!(RawDinorpgChannelConfig);
impl_channel_config!(RawEmushChannelConfig);
impl_channel_config!(RawEpopotamoChannelConfig);
impl_channel_config!(RawEternalfestChannelConfig);
impl_channel_config!(RawEtwinChannelConfig);
impl_channel_config!(RawFrutibandasChannelConfig);
impl_channel_config!(RawKingdomChannelConfig);
impl_channel_config!(RawNeoparcChannelConfig);

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize, Serialize)]
pub struct RawHttpsConfig {
  pub email: String,
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize, Serialize)]
pub struct RawStateConfig {
  pub token: String,
  pub action: StateAction,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub enum StateAction {
  /// Reset the state
  Reset,
  /// Restore from a backup
  Restore,
}

#[derive(Debug, Error)]
pub enum FindConfigFileError {
  #[error("No configuration file found from: {:?}", .0)]
  NotFound(PathBuf),
  #[error("Unexpected I/O error when resolving configuration from: {:?}: {:?}", .0, .1)]
  Other(PathBuf, io::Error),
}

#[derive(Debug, Error)]
pub enum FindConfigError {
  #[error("No configuration file found from: {:?}", .0)]
  NotFound(PathBuf),
  #[error("Failed to parse configuration file at: {:?}: {:?}", .0, .1)]
  ParseError(PathBuf, toml::de::Error),
  #[error("Unexpected I/O error when resolving configuration from: {:?}: {:?}", .0, .1)]
  Other(PathBuf, io::Error),
}

fn find_config_file(dir: PathBuf) -> Result<(PathBuf, String), FindConfigFileError> {
  for d in dir.ancestors() {
    let config_path = d.join("katal.toml");
    match fs::read_to_string(&config_path) {
      Ok(toml) => return Ok((config_path, toml)),
      Err(e) if e.kind() == io::ErrorKind::NotFound => continue,
      Err(e) => return Err(FindConfigFileError::Other(dir, e)),
    }
  }
  Err(FindConfigFileError::NotFound(dir))
}

pub fn parse_config(_file: &Path, config_toml: &str) -> Result<RawConfig, toml::de::Error> {
  let raw: RawConfig = toml::from_str(config_toml)?;
  Ok(raw)
}

pub fn find_config(dir: PathBuf) -> Result<RawConfig, FindConfigError> {
  match find_config_file(dir) {
    Ok((file, config_toml)) => match parse_config(&file, &config_toml) {
      Ok(config) => Ok(config),
      Err(e) => Err(FindConfigError::ParseError(file, e)),
    },
    Err(FindConfigFileError::NotFound(dir)) => Err(FindConfigError::NotFound(dir)),
    Err(FindConfigFileError::Other(dir, cause)) => Err(FindConfigError::Other(dir, cause)),
  }
}

pub fn parse_new_config(_file: &Path, config_toml: &str) -> Result<KatalConfig, toml::de::Error> {
  let raw: KatalConfig = toml::from_str(config_toml)?;
  Ok(raw)
}

pub fn find_new_config(dir: PathBuf) -> Result<KatalConfig, FindConfigError> {
  match find_config_file(dir) {
    Ok((file, config_toml)) => match parse_new_config(&file, &config_toml) {
      Ok(config) => Ok(config),
      Err(e) => Err(FindConfigError::ParseError(file, e)),
    },
    Err(FindConfigFileError::NotFound(dir)) => Err(FindConfigError::NotFound(dir)),
    Err(FindConfigFileError::Other(dir, cause)) => Err(FindConfigError::Other(dir, cause)),
  }
}
