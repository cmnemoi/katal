use crate::net_ipc::NetIpcSender;
use clap::Parser;

/// Arguments to the `askpass` command
///
/// The `askpass` command receives an optional prompt and must print the
/// password to stdout.
///
/// - <https://github.com/openssh/openssh-portable>
/// - <https://github.com/sudo-project/sudo>
#[derive(Debug, Parser)]
pub struct AskpassArgs {
  ipc_server: String,
  prompt: Option<String>,
}

pub async fn run(args: &AskpassArgs) -> Result<(), anyhow::Error> {
  let (mut rx, mut tx) = NetIpcSender::connect(&args.ipc_server).await?;
  tx.send(&args.prompt).await?;
  let passwd: Option<Option<String>> = rx.recv().await?;
  if let Some(Some(passwd)) = passwd {
    println!("{}", passwd);
    Ok(())
  } else {
    Err(anyhow::Error::msg("missing password: broken server pipe"))
  }
}
