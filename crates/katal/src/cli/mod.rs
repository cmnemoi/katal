pub mod askpass;
pub mod main;
pub mod plan;
pub mod state;
pub mod stdio_agent;
pub mod sync;
pub mod vault;

use crate::cli::askpass::AskpassArgs;
use crate::cli::main::MainArgs;
use crate::cli::plan::PlanArgs;
use crate::cli::stdio_agent::StdioAgentArgs;
use crate::cli::vault::VaultArgs;
use clap::Parser;

#[derive(Debug, Parser)]
#[clap(author = "Demurgos")]
pub struct CliArgs {
  #[clap(subcommand)]
  command: CliCommand,
}

#[derive(Debug, Parser)]
pub enum CliCommand {
  /// Run the IPC askpass command
  #[clap(name = "askpass")]
  Askpass(AskpassArgs),
  /// Run the main command
  #[clap(name = "main")]
  Main(MainArgs),
  /// Run the `plan` command (no changes will be applied to the target host)
  #[clap(name = "plan")]
  Plan(PlanArgs),
  /// Manage the state store
  #[clap(name = "state")]
  State(state::Args),
  /// Run the STD-IO agent command
  #[clap(name = "stdio-agent")]
  StdioAgent(StdioAgentArgs),
  /// Run the `sync` command (changes WILL BE applied to the target host)
  #[clap(name = "sync")]
  Sync(sync::Args),
  /// Manage secret values in the vault
  #[clap(name = "vault")]
  Vault(VaultArgs),
}

pub async fn run(args: &CliArgs) -> Result<(), anyhow::Error> {
  match &args.command {
    CliCommand::Askpass(ref args) => crate::cli::askpass::run(args).await,
    CliCommand::Main(ref args) => crate::cli::main::run(args).await,
    CliCommand::Plan(ref args) => crate::cli::plan::run(args).await,
    CliCommand::State(ref args) => state::run(args).await,
    CliCommand::StdioAgent(ref args) => crate::cli::stdio_agent::run(args).await,
    CliCommand::Sync(ref args) => sync::run(args).await,
    CliCommand::Vault(ref args) => crate::cli::vault::run(args).await,
  }
}
