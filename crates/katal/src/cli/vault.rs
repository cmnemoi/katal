use clap::Parser;
use dialoguer::{Input, Password};

/// Arguments to the `vault` command
#[derive(Debug, Parser)]
pub struct VaultArgs {
  #[clap(subcommand)]
  command: VaultCommand,
}

#[derive(Debug, Parser)]
pub enum VaultCommand {
  /// Encrypt a value
  #[clap(name = "encrypt")]
  Encrypt(EncryptArgs),
  /// Decrypt a value
  #[clap(name = "decrypt")]
  Decrypt(DecryptArgs),
}

/// Arguments to the `vault encrypt` command
#[derive(Debug, Parser)]
pub struct EncryptArgs {}

/// Arguments to the `vault decrypt` command
#[derive(Debug, Parser)]
pub struct DecryptArgs {}

pub async fn run(args: &VaultArgs) -> Result<(), anyhow::Error> {
  match &args.command {
    VaultCommand::Encrypt(ref args) => run_encrypt(args).await,
    VaultCommand::Decrypt(ref args) => run_decrypt(args).await,
  }
}

pub async fn run_encrypt(_args: &EncryptArgs) -> Result<(), anyhow::Error> {
  let password = Password::new().with_prompt("Vault super-key?").interact()?;

  let path: String = Input::new().with_prompt("Id for the value?").interact_text()?;
  let path = path.split('.').map(String::from).collect::<Vec<_>>();
  let value = Password::new().with_prompt("Value to encrypt?").interact()?;

  let encrypted: String = katal_vault::encrypt(path, value.into_bytes(), password.as_bytes());

  println!("{encrypted}");

  Ok(())
}

pub async fn run_decrypt(_args: &DecryptArgs) -> Result<(), anyhow::Error> {
  let password = Password::new().with_prompt("Vault super-key?").interact()?;

  let path: String = Input::new().with_prompt("Id for the value?").interact_text()?;
  let path = path.split('.').map(String::from).collect::<Vec<_>>();
  let value: String = Input::new().with_prompt("Value to decrypt?").interact()?;

  let decrypted: Vec<u8> = katal_vault::decrypt(&path, value.as_str(), password.as_bytes()).unwrap();
  let decrypted = String::from_utf8(decrypted).unwrap();

  println!("{decrypted}");

  Ok(())
}
