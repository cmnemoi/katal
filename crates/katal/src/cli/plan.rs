use clap::Parser;
use dialoguer::Password;
use katal_loader::git::GitLoader;
use katal_loader::resolve_vault;

/// Arguments to the `plan` command.
#[derive(Debug, Parser)]
pub struct PlanArgs {}

pub async fn run(_args: &PlanArgs) -> Result<(), anyhow::Error> {
  let target_loader = GitLoader::new(
    "https://gitlab.com/eternaltwin/config.git".to_string(),
    Some("norray".to_string()),
    "main".to_string(),
  );

  let target_public = target_loader.load().await.unwrap();
  println!("{}\n", serde_json::to_string_pretty(&target_public).unwrap());

  let password = Password::new().with_prompt("Vault super-key?").interact()?;

  let target_secret = resolve_vault(password.as_bytes(), target_public).unwrap();
  println!("{}\n", serde_json::to_string_pretty(&target_secret).unwrap());

  Ok(())
}
