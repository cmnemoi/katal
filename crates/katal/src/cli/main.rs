use crate::config::{find_config, FindConfigError};
use crate::etwin::channel::{BorgTarget, CertTarget, ChannelDomainTarget, ChannelPostgresTarget, ChannelTarget};
use crate::etwin::eternalfest::{
  DeployEternalfest, EternalfestChannelTarget, EternalfestReleaseTarget, EternalfestVault,
};
use crate::etwin::etwin::{DeployEtwin, EtwinChannelTarget, EtwinReleaseTarget, EtwinVault};
use crate::task::fused::StateAndHost;
use crate::task::AsyncFn;
use asys::local_linux::LocalLinux;
use clap::Parser;
use katal_state::sqlite::SqliteStore;

/// Arguments to the `main` command.
#[derive(Debug, Parser)]
pub struct MainArgs {
  app: String,
  channel: Option<String>,
}

pub async fn run(args: &MainArgs) -> Result<(), anyhow::Error> {
  let path = std::env::current_dir().unwrap();
  let config = match find_config(path) {
    Ok(config) => Some(config),
    Err(FindConfigError::NotFound(_)) => None,
    Err(e) => return Err(anyhow::Error::from(e)),
  };

  let config_password = config
    .as_ref()
    .and_then(|c| c.vault.as_ref())
    .and_then(|f| f.sudo_password.as_ref());

  let sudo_password = match config_password {
    Some(pass) => pass.clone(),
    None => dialoguer::Password::new().with_prompt("Sudo password").interact()?,
  };

  let app_name = args.app.as_str();
  let channel_name = args.channel.as_deref().unwrap_or("dev");

  let state = SqliteStore::new("./katald.sqlite", false).await;
  let host = StateAndHost::new(LocalLinux, state);

  match app_name {
    "emush" => {}
    "eternalfest" => {
      let app_config = config
        .as_ref()
        .and_then(|c| c.apps.as_ref())
        .and_then(|apps| apps.eternalfest.as_ref())
        .expect("Missing config for eternalfest");
      let channel_config = app_config.channels.get(channel_name).expect("Missing channel config");
      let vault = config
        .as_ref()
        .and_then(|c| c.vault.as_ref())
        .and_then(|channels| channels.eternalfest.get(channel_name))
        .expect("Missing vault for eternalfest");
      let full_name = format!("{}.{}", app_name, channel_name);
      let channel_target = {
        let mut target = ChannelTarget::new(full_name.clone());
        target.domain = Some(ChannelDomainTarget {
          main: channel_config.domain.clone(),
          www: true,
          cert: channel_config.https.as_ref().map(|conf| CertTarget {
            name: full_name.clone(),
            email: conf.email.clone(),
          }),
        });
        target.main_port = Some(channel_config.main_port);
        target.fallback_server = Some(channel_config.fallback_port);
        target.data_dir = true;
        target.postgres = Some(ChannelPostgresTarget {
          admin_password: vault.db_admin_password.clone(),
          main_password: vault.db_main_password.clone(),
          read_password: vault.db_read_password.clone(),
          reset: false,
        });
        target.nvm = true;
        if let Some(borgbase_api_key) = config
          .as_ref()
          .and_then(|c| c.vault.as_ref())
          .and_then(|v| v.borgbase.clone())
        {
          target.borg = Some(BorgTarget {
            borgbase_api_key,
            repo_password: vault.borgbase_repo.clone(),
          });
        }
        target
      };
      let channel_target = EternalfestChannelTarget {
        inner: channel_target,
        vault: EternalfestVault {
          secret_key: vault.secret_key.clone(),
          cookie_key: vault.cookie_key.clone(),
          etwin_client_secret: vault.etwin_client_secret.clone(),
        },
        etwin_uri: channel_config.etwin_uri.clone(),
        etwin_client_id: channel_config.etwin_client_id.clone(),
      };
      let release_target = EternalfestReleaseTarget {
        channel: channel_target,
        repository: app_config.repository.clone(),
        git_ref: channel_config.release.clone(),
      };
      let deploy_task = DeployEternalfest {
        pass: sudo_password.as_str(),
        target: release_target,
      };
      deploy_task.run(&host).await;
    }
    "etwin" => {
      let app_config = config
        .as_ref()
        .and_then(|c| c.apps.as_ref())
        .and_then(|apps| apps.etwin.as_ref())
        .expect("Missing config for etwin");
      let channel_config = app_config.channels.get(channel_name).expect("Missing channel config");
      let vault = config
        .as_ref()
        .and_then(|c| c.vault.as_ref())
        .and_then(|channels| channels.etwin.get(channel_name))
        .expect("Missing vault for etwin");
      let full_name = format!("{}.{}", app_name, channel_name);
      let channel_target = {
        let mut target = ChannelTarget::new(full_name.clone());
        target.domain = Some(ChannelDomainTarget {
          main: channel_config.domain.clone(),
          www: true,
          cert: channel_config.https.as_ref().map(|conf| CertTarget {
            name: full_name.clone(),
            email: conf.email.clone(),
          }),
        });
        target.main_port = Some(channel_config.main_port);
        target.fallback_server = Some(channel_config.fallback_port);
        target.postgres = Some(ChannelPostgresTarget {
          admin_password: vault.db_admin_password.clone(),
          main_password: vault.db_main_password.clone(),
          read_password: vault.db_read_password.clone(),
          reset: false,
        });
        target.nvm = true;
        if let Some(borgbase_api_key) = config
          .as_ref()
          .and_then(|c| c.vault.as_ref())
          .and_then(|v| v.borgbase.clone())
        {
          target.borg = Some(BorgTarget {
            borgbase_api_key,
            repo_password: vault.borgbase_repo.clone(),
          });
        }
        target
      };
      let channel_target = EtwinChannelTarget {
        inner: channel_target,
        backend_port: channel_config.backend_port,
        vault: EtwinVault {
          secret_key: vault.secret_key.clone(),
          brute_production_secret: vault.brute_production_secret.clone(),
          brute_staging_secret: vault.brute_staging_secret.clone(),
          dinocard_production_secret: vault.dinocard_production_secret.clone(),
          dinocard_staging_secret: vault.dinocard_staging_secret.clone(),
          dinorpg_production_secret: vault.dinorpg_production_secret.clone(),
          dinorpg_staging_secret: vault.dinorpg_staging_secret.clone(),
          directquiz_secret: vault.directquiz_secret.clone(),
          emush_production_secret: vault.emush_production_secret.clone(),
          emush_staging_secret: vault.emush_staging_secret.clone(),
          epopotamo_production_secret: vault.epopotamo_production_secret.clone(),
          epopotamo_staging_secret: vault.epopotamo_staging_secret.clone(),
          eternalfest_production_secret: vault.eternalfest_production_secret.clone(),
          eternalfest_staging_secret: vault.eternalfest_staging_secret.clone(),
          kadokadeo_production_secret: vault.kadokadeo_production_secret.clone(),
          kadokadeo_staging_secret: vault.kadokadeo_staging_secret.clone(),
          kingdom_production_secret: vault.kingdom_production_secret.clone(),
          kingdom_staging_secret: vault.kingdom_staging_secret.clone(),
          myhordes_secret: vault.myhordes_secret.clone(),
          neoparc_production_secret: vault.neoparc_production_secret.clone(),
          neoparc_staging_secret: vault.neoparc_staging_secret.clone(),
          mjrt_secret: vault.mjrt_secret.clone(),
          wiki_secret: vault.wiki_secret.clone(),
          twinoid_client_id: vault.twinoid_client_id.clone(),
          twinoid_client_key: vault.twinoid_client_key.clone(),
        },
      };
      let release_target = EtwinReleaseTarget {
        channel: channel_target,
        repository: app_config.repository.clone(),
        git_ref: channel_config.release.clone(),
      };
      let deploy_task = DeployEtwin {
        pass: sudo_password.as_str(),
        target: release_target,
      };
      deploy_task.run(&host).await;
    }
    app => return Err(anyhow::Error::msg(format!("Unknown app: {}", app))),
  }
  Ok(())
}
