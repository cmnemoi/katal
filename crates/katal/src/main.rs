use clap::Parser;
use katal::cli::{run, CliArgs};

#[tokio::main]
async fn main() -> Result<(), anyhow::Error> {
  let args: CliArgs = CliArgs::parse();
  run(&args).await
}
