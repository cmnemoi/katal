use crate::task::fs::EnsureFile;
use crate::task::pacman::EnsurePacmanPackages;
use crate::task::systemd::{SystemdState, SystemdUnit};
use crate::task::{AsyncFn, TaskResult, TaskSuccess};
use async_trait::async_trait;
use asys::common::fs::FsHost;
use asys::linux::fs::{FileMode, LinuxFsHost, LinuxMetadata};
use asys::linux::user::Uid;
use asys::ExecHost;
use serde::{Deserialize, Serialize};
use std::collections::BTreeSet;
use std::fmt;
use std::net::{SocketAddrV4, SocketAddrV6};
use std::path::PathBuf;

pub const PHP_INI: &str = include_str!("../../files/php/php.ini");

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Default, Serialize, Deserialize)]
pub struct PhpAvailable {
  pub ext_curl: bool,
  pub ext_ctype: bool,
  pub ext_iconv: bool,
  pub ext_intl: bool,
  pub ext_pdo_pgsql: bool,
  pub ext_pgsql: bool,
  pub ext_sodium: bool,
  pub ext_zip: bool,
}

#[async_trait]
impl<'h, H: ExecHost + LinuxFsHost> AsyncFn<&'h H> for PhpAvailable
where
  <H as FsHost>::Metadata: LinuxMetadata,
{
  type Output = TaskResult<(), anyhow::Error>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let mut changed = false;
    {
      let mut pacman = EnsurePacmanPackages::new().present("composer").present("php-fpm");
      if self.ext_intl {
        pacman = pacman.present("php-intl");
      }
      if self.ext_pdo_pgsql || self.ext_pgsql {
        pacman = pacman.present("php-pgsql");
      }
      if self.ext_sodium {
        pacman = pacman.present("php-sodium");
      }
      changed = pacman.run(host).await?.changed || changed;
    }
    changed = EnsureFile::new("/etc/php/php.ini")
      .content(PHP_INI)
      .owner(Uid::ROOT)
      .mode(FileMode::ALL_READ)
      .run(host)
      .await?
      .changed
      || changed;
    let mut extensions: BTreeSet<&'static str> = BTreeSet::new();
    // if self.ext_curl {
    //   extensions.insert("curl");
    // }
    // if self.ext_ctype {
    //   extensions.insert("ctype");
    // }
    if self.ext_iconv {
      extensions.insert("iconv");
    }
    if self.ext_intl {
      extensions.insert("intl");
    }
    if self.ext_pdo_pgsql {
      extensions.insert("pdo_pgsql");
    }
    if self.ext_sodium {
      extensions.insert("sodium");
    }
    // if self.ext_zip {
    //   extensions.insert("zip");
    // }
    for ext in extensions.into_iter() {
      let path = PathBuf::from("/etc/php/conf.d").join(format!("ext_{}.ini", ext));
      let content = format!("extension={}\n", ext);
      changed = EnsureFile::new(path)
        .content(content)
        .owner(Uid::ROOT)
        .mode(FileMode::ALL_READ)
        .run(host)
        .await?
        .changed
        || changed;
    }

    changed = SystemdUnit::new("php-fpm.service")
      .enabled(true)
      .state(SystemdState::Active)
      .run(host)
      .await?
      .changed
      || changed;
    Ok(TaskSuccess { changed, output: () })
  }
}

/// https://www.php.net/manual/en/install.fpm.configuration.php
#[derive(Debug, Clone, Eq, PartialEq)]
pub struct NamedPhpFpmPoolConfig {
  pub name: String,
  pub config: PhpFpmPoolConfig,
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct PhpFpmPoolConfig {
  pub user: String,
  pub group: PhpFpmUnixGroup,
  pub listen: PhpFpmSocketAddr,
  pub listen_owner: Option<String>,
  pub listen_group: Option<String>,
  pub pm: Option<String>,
  pub pm_max_children: Option<u16>,
  pub pm_start_servers: Option<u16>,
  pub pm_min_spare_servers: Option<u16>,
  pub pm_max_spare_servers: Option<u16>,
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub enum PhpFpmSocketAddr {
  V4(SocketAddrV4),
  V6(SocketAddrV6),
  All(u16),
  Unix(PathBuf),
}

impl fmt::Display for PhpFpmSocketAddr {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    match self {
      PhpFpmSocketAddr::V4(ref addr) => fmt::Display::fmt(addr, f),
      PhpFpmSocketAddr::V6(ref addr) => fmt::Display::fmt(addr, f),
      PhpFpmSocketAddr::All(port) => fmt::Display::fmt(port, f),
      PhpFpmSocketAddr::Unix(ref sock) => fmt::Display::fmt(&sock.display(), f),
    }
  }
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub enum PhpFpmUnixGroup {
  FromUser,
  Named(String),
}

impl fmt::Display for NamedPhpFpmPoolConfig {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    writeln!(f, "[{}]", self.name.as_str())?;
    writeln!(f, "user = {}", self.config.user.as_str())?;
    match &self.config.group {
      PhpFpmUnixGroup::FromUser => {}
      PhpFpmUnixGroup::Named(ref group) => writeln!(f, "group = {}", group.as_str())?,
    }
    writeln!(f, "listen = {}", &self.config.listen)?;
    if let Some(listen_owner) = self.config.listen_owner.as_ref() {
      writeln!(f, "listen.owner = {}", listen_owner.as_str())?;
    }
    if let Some(listen_group) = self.config.listen_group.as_ref() {
      writeln!(f, "listen.group = {}", listen_group.as_str())?;
    }
    if let Some(pm) = self.config.pm.as_ref() {
      writeln!(f, "pm = {}", pm.as_str())?;
    }
    if let Some(pm_max_children) = self.config.pm_max_children {
      writeln!(f, "pm.max_children = {}", pm_max_children)?;
    }
    if let Some(pm_start_servers) = self.config.pm_start_servers {
      writeln!(f, "pm.start_servers = {}", pm_start_servers)?;
    }
    if let Some(pm_min_spare_servers) = self.config.pm_min_spare_servers {
      writeln!(f, "pm.min_spare_servers = {}", pm_min_spare_servers)?;
    }
    if let Some(pm_max_spare_servers) = self.config.pm_max_spare_servers {
      writeln!(f, "pm.max_spare_servers = {}", pm_max_spare_servers)?;
    }
    Ok(())
  }
}
