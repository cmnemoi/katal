use crate::task::fs::{EnsureFile, EnsureFileError, EnsureFileSymlink};
use crate::task::systemd::{get_status, ActiveState};
use crate::task::{AsyncFn, TaskResult, TaskSuccess};
use async_trait::async_trait;
use asys::common::fs::FsHost;
use asys::linux::fs::{FileMode, LinuxFsHost, LinuxMetadata};
use asys::linux::user::{LinuxUserHost, Uid};
use asys::{Command, ExecHost};
use serde::{Deserialize, Serialize};
use std::path::PathBuf;
use thiserror::Error;

/// Check that the config is valid.
/// If the config is valid, reload nginx.
pub struct TrySyncNginxConfig;

#[async_trait]
impl<'h, H: ExecHost> AsyncFn<&'h H> for TrySyncNginxConfig {
  type Output = TaskResult<(), anyhow::Error>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let test = Command::new("nginx").arg("-t").hint_no_host_mutation();
    let out = host.try_exec(&test)?;
    if !out.termination.success() {
      return Err(anyhow::Error::msg(format!(
        "Invalid nginx config: {}",
        String::from_utf8_lossy(&out.stderr)
      )));
    }
    let status = get_status("nginx").await;
    if status.active_state == ActiveState::Active {
      let reload = Command::new("nginx").arg("-s").arg("reload");
      host.exec(&reload)?;
    }
    Ok(TaskSuccess {
      changed: true,
      output: (),
    })
  }
}

pub struct NginxAvailableSite {
  name: String,
  config: String,
}

impl NginxAvailableSite {
  pub fn new(name: impl AsRef<str>, config: impl AsRef<str>) -> Self {
    Self {
      name: name.as_ref().to_string(),
      config: config.as_ref().to_string(),
    }
  }
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize, Error)]
pub enum NginxAvailableSiteError {
  #[error("failed to ensure nginx config file")]
  EnsureConfig(#[from] EnsureFileError),
}

#[async_trait]
impl<'h, H: LinuxFsHost> AsyncFn<&'h H> for NginxAvailableSite
where
  <H as FsHost>::Metadata: LinuxMetadata,
{
  type Output = TaskResult<(), anyhow::Error>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let path = PathBuf::from("/etc/nginx/sites-available").join(format!("{}.nginx", &self.name));
    Ok(
      EnsureFile::new(path)
        .content(self.config.as_bytes())
        .mode(FileMode::ALL_READ)
        .owner(Uid::ROOT)
        .run(host)
        .await?,
    )
  }
}

pub struct NginxEnableSite {
  name: String,
}

impl NginxEnableSite {
  pub fn new(name: impl AsRef<str>) -> Self {
    Self {
      name: name.as_ref().to_string(),
    }
  }
}

#[async_trait]
impl<'h, H: ExecHost + LinuxUserHost + LinuxFsHost> AsyncFn<&'h H> for NginxEnableSite
where
  <H as FsHost>::Metadata: LinuxMetadata,
{
  type Output = TaskResult<(), anyhow::Error>;

  async fn run(&self, host: &'h H) -> Self::Output {
    eprintln!("Enabling Nginx site: {}", &self.name);
    let path = PathBuf::from("/etc/nginx/sites-enabled").join(format!("{}.nginx", &self.name));
    let pointee = PathBuf::from("/etc/nginx/sites-available").join(format!("{}.nginx", &self.name));
    EnsureFileSymlink::new(path)
      .points_to(pointee)
      .owner(Uid::ROOT)
      .run(host)
      .await?;

    TrySyncNginxConfig.run(host).await
  }
}
