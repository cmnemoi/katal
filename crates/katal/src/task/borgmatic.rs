use crate::task::fs::{EnsureFile, EnsureFileError, FileModeTarget};
use crate::task::systemd::{SystemdState, SystemdUnit};
use crate::task::{AsyncFn, TaskResult, TaskSuccess};
use async_trait::async_trait;
use asys::common::fs::FsHost;
use asys::linux::fs::{FileMode, LinuxFsHost, LinuxMetadata};
use asys::linux::user::{Gid, Uid};
use asys::ExecHost;
use chrono::Duration;
use serde::{Deserialize, Serialize};
use std::path::PathBuf;
use thiserror::Error;

#[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
pub struct BorgmaticConfig {
  pub location: BorgmaticLocationConfig,
  #[serde(skip_serializing_if = "Option::is_none")]
  pub storage: Option<BorgmaticStorageConfig>,
  pub retention: BorgmaticRetentionConfig,
  #[serde(skip_serializing_if = "Option::is_none")]
  pub consistency: Option<BorgmaticConsistencyConfig>,
  #[serde(skip_serializing_if = "Option::is_none")]
  pub output: Option<BorgmaticOutputConfig>,
  #[serde(skip_serializing_if = "Option::is_none")]
  pub hooks: Option<BorgmaticHooks>,
}

#[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
pub struct BorgmaticLocationConfig {
  pub source_directories: Vec<PathPattern>,
  pub repositories: Vec<BorgRepository>,
  #[serde(skip_serializing_if = "Option::is_none")]
  pub one_file_system: Option<bool>,
  #[serde(skip_serializing_if = "Option::is_none")]
  pub numeric_owner: Option<bool>,
  #[serde(skip_serializing_if = "Option::is_none")]
  pub atime: Option<bool>,
  #[serde(skip_serializing_if = "Option::is_none")]
  pub ctime: Option<bool>,
  #[serde(skip_serializing_if = "Option::is_none")]
  pub birthtime: Option<bool>,
  #[serde(skip_serializing_if = "Option::is_none")]
  pub read_special: Option<bool>,
  #[serde(skip_serializing_if = "Option::is_none")]
  pub bsd_flags: Option<bool>,
  #[serde(skip_serializing_if = "Option::is_none")]
  pub files_cache: Option<bool>,
  #[serde(skip_serializing_if = "Option::is_none")]
  pub local_path: Option<String>,
  #[serde(skip_serializing_if = "Option::is_none")]
  pub remote_path: Option<String>,
  #[serde(skip_serializing_if = "Option::is_none")]
  pub patterns: Option<Vec<String>>,
  #[serde(skip_serializing_if = "Option::is_none")]
  pub patterns_from: Option<Vec<String>>,
  #[serde(skip_serializing_if = "Option::is_none")]
  pub exclude_patterns: Option<Vec<String>>,
  #[serde(skip_serializing_if = "Option::is_none")]
  pub exclude_patterns_from: Option<Vec<String>>,
  #[serde(skip_serializing_if = "Option::is_none")]
  pub exclude_caches: Option<bool>,
  #[serde(skip_serializing_if = "Option::is_none")]
  pub exclude_if_present: Option<Vec<String>>,
  #[serde(skip_serializing_if = "Option::is_none")]
  pub keep_exclude_tags: Option<bool>,
  #[serde(skip_serializing_if = "Option::is_none")]
  pub exclude_nodump: Option<bool>,
  #[serde(skip_serializing_if = "Option::is_none")]
  pub borgmatic_source_directory: Option<String>,
}

#[derive(Default, Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
pub struct BorgmaticStorageConfig {
  #[serde(skip_serializing_if = "Option::is_none")]
  pub encryption_passcommand: Option<String>,
  #[serde(skip_serializing_if = "Option::is_none")]
  pub encryption_passphrase: Option<String>,
  #[serde(skip_serializing_if = "Option::is_none")]
  pub checkpoint_interval: Option<u64>,
  #[serde(skip_serializing_if = "Option::is_none")]
  pub chunker_params: Option<String>,
  #[serde(skip_serializing_if = "Option::is_none")]
  pub compression: Option<String>,
  #[serde(skip_serializing_if = "Option::is_none")]
  pub remote_rate_limit: Option<u64>,
  #[serde(skip_serializing_if = "Option::is_none")]
  pub temporary_directory: Option<String>,
  #[serde(skip_serializing_if = "Option::is_none")]
  pub ssh_command: Option<String>,
  #[serde(skip_serializing_if = "Option::is_none")]
  pub borg_base_directory: Option<String>,
  #[serde(skip_serializing_if = "Option::is_none")]
  pub borg_config_directory: Option<String>,
  #[serde(skip_serializing_if = "Option::is_none")]
  pub borg_cache_directory: Option<String>,
  #[serde(skip_serializing_if = "Option::is_none")]
  pub borg_security_directory: Option<String>,
  #[serde(skip_serializing_if = "Option::is_none")]
  pub borg_keys_directory: Option<String>,
  #[serde(skip_serializing_if = "Option::is_none")]
  pub umask: Option<u32>,
  #[serde(skip_serializing_if = "Option::is_none")]
  pub lock_wait: Option<u32>,
  #[serde(skip_serializing_if = "Option::is_none")]
  pub archive_name_format: Option<String>,
  #[serde(skip_serializing_if = "Option::is_none")]
  pub relocated_repo_access_is_ok: Option<bool>,
  #[serde(skip_serializing_if = "Option::is_none")]
  pub unknown_unencrypted_repo_access_is_ok: Option<bool>,
  #[serde(skip_serializing_if = "Option::is_none")]
  pub extra_borg_options: Option<ExtraBorgOptions>,
}

#[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
pub struct ExtraBorgOptions {
  pub init: Vec<String>,
  pub prune: Vec<String>,
  pub create: Vec<String>,
  pub check: Vec<String>,
}

#[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
pub struct BorgmaticRetentionConfig {
  #[serde(skip_serializing_if = "Option::is_none")]
  pub keep_within: Option<String>,
  #[serde(skip_serializing_if = "Option::is_none")]
  pub keep_secondly: Option<u64>,
  #[serde(skip_serializing_if = "Option::is_none")]
  pub keep_minutely: Option<u64>,
  #[serde(skip_serializing_if = "Option::is_none")]
  pub keep_hourly: Option<u64>,
  pub keep_daily: u64,
  #[serde(skip_serializing_if = "Option::is_none")]
  pub keep_weekly: Option<u64>,
  #[serde(skip_serializing_if = "Option::is_none")]
  pub keep_monthly: Option<u64>,
  #[serde(skip_serializing_if = "Option::is_none")]
  pub keep_yearly: Option<u64>,
  #[serde(skip_serializing_if = "Option::is_none")]
  pub prefix: Option<String>,
}

#[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
pub struct BorgmaticConsistencyConfig {
  #[serde(skip_serializing_if = "Option::is_none")]
  pub checks: Option<Vec<String>>,
  #[serde(skip_serializing_if = "Option::is_none")]
  pub check_repositories: Option<Vec<String>>,
  #[serde(skip_serializing_if = "Option::is_none")]
  pub check_last: Option<u64>,
  #[serde(skip_serializing_if = "Option::is_none")]
  pub prefix: Option<String>,
}

#[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
pub struct BorgmaticOutputConfig {
  #[serde(skip_serializing_if = "Option::is_none")]
  pub color: Option<bool>,
}

#[derive(Default, Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
pub struct BorgmaticHooks {
  #[serde(skip_serializing_if = "Option::is_none")]
  pub before_backup: Option<Vec<String>>,
  #[serde(skip_serializing_if = "Option::is_none")]
  pub before_prune: Option<Vec<String>>,
  #[serde(skip_serializing_if = "Option::is_none")]
  pub before_check: Option<Vec<String>>,
  #[serde(skip_serializing_if = "Option::is_none")]
  pub before_extract: Option<Vec<String>>,
  #[serde(skip_serializing_if = "Option::is_none")]
  pub after_backup: Option<Vec<String>>,
  #[serde(skip_serializing_if = "Option::is_none")]
  pub after_prune: Option<Vec<String>>,
  #[serde(skip_serializing_if = "Option::is_none")]
  pub after_check: Option<Vec<String>>,
  #[serde(skip_serializing_if = "Option::is_none")]
  pub after_extract: Option<Vec<String>>,
  #[serde(skip_serializing_if = "Option::is_none")]
  pub on_error: Option<Vec<String>>,
  #[serde(skip_serializing_if = "Option::is_none")]
  pub before_everything: Option<Vec<String>>,
  #[serde(skip_serializing_if = "Option::is_none")]
  pub after_everything: Option<Vec<String>>,
  #[serde(skip_serializing_if = "Option::is_none")]
  pub postgresql_databases: Option<Vec<BorgmaticPostgresHook>>,
  #[serde(skip_serializing_if = "Option::is_none")]
  pub mysql_databases: Option<Vec<BorgmaticMysqlHook>>,
  #[serde(skip_serializing_if = "Option::is_none")]
  pub healthchecks: Option<String>,
  #[serde(skip_serializing_if = "Option::is_none")]
  pub cronitor: Option<String>,
  #[serde(skip_serializing_if = "Option::is_none")]
  pub pagerduty: Option<String>,
  #[serde(skip_serializing_if = "Option::is_none")]
  pub cronhub: Option<String>,
  #[serde(skip_serializing_if = "Option::is_none")]
  pub umask: Option<u32>,
}

#[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
pub struct BorgmaticPostgresHook {
  #[serde(skip_serializing_if = "Option::is_none")]
  pub name: Option<String>,
  #[serde(skip_serializing_if = "Option::is_none")]
  pub hostname: Option<String>,
  #[serde(skip_serializing_if = "Option::is_none")]
  pub port: Option<String>,
  #[serde(skip_serializing_if = "Option::is_none")]
  pub username: Option<String>,
  #[serde(skip_serializing_if = "Option::is_none")]
  pub password: Option<String>,
  #[serde(skip_serializing_if = "Option::is_none")]
  pub format: Option<String>,
  #[serde(skip_serializing_if = "Option::is_none")]
  pub ssl_mode: Option<String>,
  #[serde(skip_serializing_if = "Option::is_none")]
  pub ssl_cert: Option<String>,
  #[serde(skip_serializing_if = "Option::is_none")]
  pub ssl_crl: Option<String>,
  #[serde(skip_serializing_if = "Option::is_none")]
  pub options: Option<String>,
}

#[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
pub struct BorgmaticMysqlHook {
  #[serde(skip_serializing_if = "Option::is_none")]
  pub name: Option<String>,
  #[serde(skip_serializing_if = "Option::is_none")]
  pub hostname: Option<String>,
  #[serde(skip_serializing_if = "Option::is_none")]
  pub port: Option<String>,
  #[serde(skip_serializing_if = "Option::is_none")]
  pub username: Option<String>,
  #[serde(skip_serializing_if = "Option::is_none")]
  pub password: Option<String>,
  #[serde(skip_serializing_if = "Option::is_none")]
  pub options: Option<String>,
}

#[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
pub struct PathPattern(pub String);

#[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
pub struct BorgRepository(pub String);

impl BorgmaticConfig {
  pub fn new(source_directories: Vec<PathPattern>, repositories: Vec<BorgRepository>, keep_daily: u64) -> Self {
    Self {
      location: BorgmaticLocationConfig::new(source_directories, repositories),
      storage: None,
      retention: BorgmaticRetentionConfig::new(keep_daily),
      consistency: None,
      output: None,
      hooks: None,
    }
  }
}

impl BorgmaticLocationConfig {
  pub fn new(source_directories: Vec<PathPattern>, repositories: Vec<BorgRepository>) -> Self {
    Self {
      source_directories,
      repositories,
      one_file_system: None,
      numeric_owner: None,
      atime: None,
      ctime: None,
      birthtime: None,
      read_special: None,
      bsd_flags: None,
      files_cache: None,
      local_path: None,
      remote_path: None,
      patterns: None,
      patterns_from: None,
      exclude_patterns: None,
      exclude_patterns_from: None,
      exclude_caches: None,
      exclude_if_present: None,
      keep_exclude_tags: None,
      exclude_nodump: None,
      borgmatic_source_directory: None,
    }
  }
}

impl BorgmaticRetentionConfig {
  pub fn new(keep_daily: u64) -> Self {
    Self {
      keep_within: None,
      keep_secondly: None,
      keep_minutely: None,
      keep_hourly: None,
      keep_daily,
      keep_weekly: None,
      keep_monthly: None,
      keep_yearly: None,
      prefix: None,
    }
  }
}

pub struct BorgmaticConfigAt {
  file: EnsureFile,
  config: BorgmaticConfig,
}

impl BorgmaticConfigAt {
  pub fn new(path: impl Into<PathBuf>, config: BorgmaticConfig) -> Self {
    Self {
      file: EnsureFile::new(path),
      config,
    }
  }

  pub fn owner(mut self, owner: Uid) -> Self {
    self.file = self.file.owner(owner);
    self
  }

  pub fn group(mut self, group: Gid) -> Self {
    self.file = self.file.group(group);
    self
  }

  pub fn mode(mut self, mode: impl Into<FileModeTarget>) -> Self {
    self.file = self.file.mode(mode);
    self
  }
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize, Error)]
pub enum BorgmaticConfigAtError {
  #[error("failed to serialize borgmatic config")]
  Serialize(String),
  #[error("failed to ensure borgmatic config file")]
  EnsureConfig(#[from] EnsureFileError),
}

#[async_trait]
impl<'h, H> AsyncFn<&'h H> for BorgmaticConfigAt
where
  H: ExecHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
{
  type Output = TaskResult<(), BorgmaticConfigAtError>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let mut config =
      serde_yaml::to_string(&self.config).map_err(|e| BorgmaticConfigAtError::Serialize(e.to_string()))?;
    config.push('\n');
    Ok(self.file.clone().content(config).run(host).await?)
    // TODO: validate-borgmatic-config
  }
}

pub struct BorgmaticTimer {
  pub name: String,
  pub user: String,
  pub cwd: PathBuf,
  pub config: PathBuf,
  pub active: bool,
}

#[async_trait]
impl<'h, H: ExecHost + LinuxFsHost> AsyncFn<&'h H> for BorgmaticTimer
where
  <H as FsHost>::Metadata: LinuxMetadata,
{
  type Output = TaskResult<(), anyhow::Error>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let service_name = format!("{}.service", self.name);
    let timer_name = format!("{}.timer", self.name);
    let systemd_dir = PathBuf::from("/etc/systemd/system");
    let service = format!(
      r#"[Unit]
Description=Backup using Borgmatic ({name})

[Service]
Type=oneshot
User={user}
WorkingDirectory={cwd}

ExecStart=borgmatic -c "{config}"
"#,
      name = &self.name,
      user = &self.user,
      cwd = &self.cwd.to_str().unwrap(),
      config = self.config.to_str().unwrap()
    );
    let timer = format!(
      r#"[Unit]
Description=Timer for Borgmatic ({name})

[Timer]
# Try to renew once a day, between 01:00 and 08:00
OnCalendar=*-*-* 01:00:00
RandomizedDelaySec={interval}
# Run immediately if the previous start time was missed (e.g. due to the server being powered off)
Persistent=true

[Install]
WantedBy=timers.target
"#,
      interval = Duration::hours(7).num_seconds(),
      name = &self.name,
    );

    let mut changed = false;
    changed = EnsureFile::new(systemd_dir.join(&service_name))
      .content(service)
      .owner(Uid::ROOT)
      .mode(FileMode::ALL_READ)
      .run(host)
      .await?
      .changed
      || changed;
    changed = EnsureFile::new(systemd_dir.join(&timer_name))
      .content(timer)
      .owner(Uid::ROOT)
      .mode(FileMode::ALL_READ)
      .run(host)
      .await?
      .changed
      || changed;
    changed = SystemdUnit::new(&timer_name)
      .enabled(true)
      .state(if self.active {
        SystemdState::Active
      } else {
        SystemdState::Inactive
      })
      .run(host)
      .await?
      .changed
      || changed;
    Ok(TaskSuccess { changed, output: () })
  }
}
