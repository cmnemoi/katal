use crate::task::{AsyncFn, TaskResult, TaskSuccess};
use async_trait::async_trait;
use asys::local_linux::LocalLinux;
use asys::{Command, ExecHost};
use serde::{Deserialize, Serialize};
use thiserror::Error;

pub struct SystemdUnit {
  name: String,
  target: SystemdTarget,
}

pub struct SystemdTarget {
  enabled: Option<bool>,
  state: Option<SystemdState>,
}

#[derive(Copy, Clone)]
pub enum SystemdState {
  Inactive,
  Active,
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize, Error)]
pub enum SystemdUnitError {
  #[error("failed to set `enabled` status")]
  Enabled(String),
  #[error("failed to set active state")]
  State(String),
}

#[async_trait]
impl<'h, H: ExecHost> AsyncFn<&'h H> for SystemdUnit {
  type Output = TaskResult<(), SystemdUnitError>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let mut changed = false;
    if let Some(enabled) = self.target.enabled {
      let mut cmd = Command::new("systemctl");
      cmd = cmd.arg(if enabled { "enable" } else { "disable" });
      cmd = cmd.arg(&self.name);
      host.exec(&cmd).map_err(|e| SystemdUnitError::Enabled(e.to_string()))?;
      changed = true;
    }
    if let Some(state) = self.target.state {
      let mut cmd = Command::new("systemctl");
      cmd = cmd.arg(match state {
        SystemdState::Active => "start",
        SystemdState::Inactive => "stop",
      });
      cmd = cmd.arg(&self.name);
      host.exec(&cmd).map_err(|e| SystemdUnitError::State(e.to_string()))?;
      changed = true;
    }

    Ok(TaskSuccess { changed, output: () })
  }
}

impl SystemdUnit {
  pub fn new(name: impl ToString) -> Self {
    Self {
      name: name.to_string(),
      target: SystemdTarget {
        enabled: None,
        state: None,
      },
    }
  }

  pub fn enabled(mut self, enabled: bool) -> Self {
    self.target.enabled = Some(enabled);
    self
  }

  pub fn state(mut self, state: SystemdState) -> Self {
    self.target.state = Some(state);
    self
  }
}

#[derive(Debug, Copy, Clone, Ord, PartialOrd, Eq, PartialEq, Hash)]
pub struct SystemdStatus {
  pub restart: bool,
  pub active_state: ActiveState,
}

#[derive(Debug, Copy, Clone, Ord, PartialOrd, Eq, PartialEq, Hash)]
pub enum ActiveState {
  Active,
  Inactive,
}

pub async fn get_status(name: &str) -> SystemdStatus {
  let cmd = Command::new("systemctl").arg("show").arg(name);
  let out = LocalLinux.exec(&cmd).unwrap().stdout;
  let out = String::from_utf8(out).unwrap();
  let mut restart: Option<bool> = None;
  let mut active_state: Option<ActiveState> = None;
  for l in out.lines() {
    if let Some(eq) = l.find('=') {
      let name = &l[0..eq];
      let val = &l[(eq + 1)..];
      match name {
        "Restart" => {
          restart = Some(match val {
            "yes" => true,
            "no" => false,
            _ => todo!(),
          })
        }
        "ActiveState" => {
          active_state = Some(match val {
            "active" => ActiveState::Active,
            "inactive" => ActiveState::Inactive,
            val => panic!("UnexpectedActiveState: {}", val),
          })
        }
        _ => {}
      }
    }
  }
  SystemdStatus {
    restart: restart.unwrap(),
    active_state: active_state.unwrap(),
  }
}

pub fn restart_systemd_unit<H>(name: &str, host: &H) -> Result<(), anyhow::Error>
where
  H: ExecHost,
{
  let cmd = Command::new("systemctl").arg("restart").arg(name);
  host.exec(&cmd)?;
  Ok(())
}
