use crate::task::git::GitCheckout;
use crate::task::{exec_as, AsyncFn, NamedTask, TaskName, TaskResult, TaskSuccess};
use async_trait::async_trait;
use asys::common::fs::FsHost;
use asys::linux::fs::{LinuxFsHost, LinuxMetadata};
use asys::linux::user::{LinuxUserHost, Uid, UserRef};
use asys::local_linux::LocalLinux;
use asys::ExecHost;
use serde::{Deserialize, Serialize};
use std::path::{Path, PathBuf};

#[derive(Debug, Copy, Clone, Serialize, Deserialize)]
pub enum NvmVersion {
  V0_38_0,
  V0_39_1,
}

impl NvmVersion {
  pub fn latest() -> Self {
    Self::V0_39_1
  }

  pub fn to_git_ref(&self) -> &'static str {
    match self {
      NvmVersion::V0_38_0 => "v0.38.0",
      NvmVersion::V0_39_1 => "v0.39.1",
    }
  }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Nvm {
  /// NVM home path
  ///
  /// Default: `~/.nvm` (depends on `owner`)
  path: Option<PathBuf>,
  owner: Uid,
  version: NvmVersion,
}

impl Nvm {
  pub fn new(owner: Uid) -> Self {
    Self {
      path: None,
      owner,
      version: NvmVersion::latest(),
    }
  }

  pub fn path(mut self, p: impl AsRef<Path>) -> Self {
    self.path = Some(p.as_ref().to_path_buf());
    self
  }
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct NvmEnv {
  /// Absolute path to the NVM home
  ///
  /// This corresponds to the `NVM_DIR` environment variable.
  pub home: PathBuf,
}

#[async_trait]
impl<'h, H: ExecHost> AsyncFn<&'h H> for Nvm {
  type Output = TaskResult<NvmEnv, anyhow::Error>;

  async fn run(&self, _host: &'h H) -> Self::Output {
    let res = exec_as::<LocalLinux, NvmCommand>(UserRef::Id(self.owner), None, &NvmCommand(self.clone())).await?;
    match res {
      Ok(p) => Ok(TaskSuccess {
        changed: true,
        output: p,
      }),
      Err(()) => Err(anyhow::Error::msg("FailedNvmInitialization")),
    }
  }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct NvmCommand(Nvm);

impl<'h, H: ExecHost + LinuxUserHost + LinuxFsHost> NamedTask<&'h H> for NvmCommand
where
  <H as FsHost>::Metadata: LinuxMetadata,
{
  const NAME: TaskName = TaskName::new("NvmCommand");
}

#[async_trait]
impl<'h, H: ExecHost + LinuxUserHost + LinuxFsHost> AsyncFn<&'h H> for NvmCommand
where
  <H as FsHost>::Metadata: LinuxMetadata,
{
  type Output = Result<NvmEnv, ()>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let nvm = &self.0;
    let path = match nvm.path.clone() {
      Some(p) => p,
      None => dirs::home_dir().unwrap().join(".nvm"),
    };
    let gc = GitCheckout {
      path: path.clone(),
      remote: "https://github.com/nvm-sh/nvm.git".to_string(),
      r#ref: nvm.version.to_git_ref().to_string(),
    };
    gc.run(host).await.map_err(drop)?;
    Ok(NvmEnv { home: path })
  }
}
