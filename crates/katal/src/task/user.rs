use crate::task::{AsyncFn, TaskResult, TaskSuccess};
use async_trait::async_trait;
use asys::linux::user::{
  CreateGroupError, GetUserGroupsError, Gid, GroupFromIdError, GroupRef, LinuxGroup, LinuxUser, LinuxUserHost,
  TryGroupFromNameError, TryUserFromNameError, Uid, UpdateGroupError,
};
use asys::{Command, ExecHost};
use serde::{Deserialize, Serialize};
use sha_crypt::{sha512_simple, CryptError, Sha512Params};
use std::collections::{BTreeMap, BTreeSet};
use std::fmt::Debug;
use std::path::{Path, PathBuf};
use thiserror::Error;

pub trait SecondaryGroupsTarget: Debug + Clone + Eq + PartialEq {
  fn test(&self, value: &BTreeSet<String>) -> bool;

  fn fix(&self, value: &mut BTreeSet<String>);
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize)]
pub struct AnySecondaryGroupsTarget;

impl SecondaryGroupsTarget for AnySecondaryGroupsTarget {
  fn test(&self, _value: &BTreeSet<String>) -> bool {
    true
  }

  fn fix(&self, _value: &mut BTreeSet<String>) {
    /* Empty: any value is considered valid */
  }
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize)]
pub struct ExactSecondaryGroupsTarget {
  groups: BTreeSet<String>,
}

impl Default for ExactSecondaryGroupsTarget {
  fn default() -> Self {
    Self::new()
  }
}

impl ExactSecondaryGroupsTarget {
  pub fn new() -> Self {
    Self {
      groups: BTreeSet::new(),
    }
  }
}

impl SecondaryGroupsTarget for ExactSecondaryGroupsTarget {
  fn test(&self, value: &BTreeSet<String>) -> bool {
    value == &self.groups
  }

  fn fix(&self, value: &mut BTreeSet<String>) {
    *value = self.groups.clone()
  }
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize)]
pub struct PartialSecondaryGroupsTarget {
  // Secondary groups to add or remove
  // `true`: add
  // `false`: remove
  groups: BTreeMap<String, bool>,
}

impl SecondaryGroupsTarget for PartialSecondaryGroupsTarget {
  fn test(&self, value: &BTreeSet<String>) -> bool {
    self
      .groups
      .iter()
      .all(|(name, present)| value.contains(name) == *present)
  }

  fn fix(&self, value: &mut BTreeSet<String>) {
    for (name, present) in self.groups.iter() {
      if *present {
        value.insert(name.clone());
      } else {
        value.remove(name);
      }
    }
  }
}

#[derive(Serialize, Deserialize)]
pub struct UpsertGroupByName {
  name: String,
  gid: Option<Gid>,
}

fn is_system_user(uid: Uid) -> bool {
  // TODO: Move this function to the LinuxUserHost trait so the constant can be retrieve from the host (it's not always 1000).
  uid.into_raw() < 1000
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize, Error)]
pub enum UpsertGroupByNameError {
  #[error("failed old value check")]
  CheckOldValue(#[source] TryGroupFromNameError),
  #[error("failed new value check")]
  CheckNewValue(#[source] TryGroupFromNameError),
  #[error("failed to update group")]
  Update(#[from] UpdateGroupError),
  #[error("failed to create group")]
  Create(#[from] CreateGroupError),
  #[error("failed to read back group after changes")]
  Confirm,
}

#[async_trait]
impl<'h, H: LinuxUserHost> AsyncFn<&'h H> for UpsertGroupByName {
  type Output = TaskResult<H::Group, UpsertGroupByNameError>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let old_group = host
      .try_group_from_name(&self.name)
      .map_err(UpsertGroupByNameError::CheckOldValue)?;

    // https://github.com/shadow-maint/shadow
    if let Some(old_group) = old_group.as_ref() {
      // Diff the fields and apply updates
      let new_gid = match self.gid {
        Some(gid) if gid != old_group.gid() => Some(gid),
        _ => None,
      };
      if new_gid.is_some() {
        host.update_group(&self.name, new_gid)?;
      }
    } else {
      // Missing group, create it
      host.create_group(&self.name, self.gid)?;
    }

    let new_group = host
      .try_group_from_name(&self.name)
      .map_err(UpsertGroupByNameError::CheckNewValue)?;
    let changed = new_group != old_group;

    match new_group {
      Some(group) => Ok(TaskSuccess { changed, output: group }),
      None => Err(UpsertGroupByNameError::Confirm),
    }
  }
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct UpsertUserByName<SecondaryGroups: SecondaryGroupsTarget> {
  name: String,
  uid: Option<Uid>,
  group: Option<GroupRef>,
  secondary_groups: SecondaryGroups,
  shell: Option<PathBuf>,
  password: Option<String>,
  comment: Option<String>,
  home: Option<PathBuf>,
  system: Option<bool>,
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize, Error)]
pub enum UpsertUserByNameError {
  #[error("failed old value check")]
  CheckOldValue(#[source] TryUserFromNameError),
  #[error("failed new value check")]
  CheckNewValue(#[source] TryUserFromNameError),
  #[error("get user groups")]
  GetUserGroups(#[from] GetUserGroupsError),
  #[error("group not found for id: {0}")]
  GroupNotFound(Gid, #[source] GroupFromIdError),
  #[error("failed to update user: {0}")]
  Update(String),
  #[error("failed to create user: {0}")]
  Create(String),
  #[error("failed to read back group after changes")]
  Confirm,
}

#[async_trait]
impl<'h, H, SecondaryGroups> AsyncFn<&'h H> for UpsertUserByName<SecondaryGroups>
where
  H: ExecHost + LinuxUserHost,
  H::User: Debug,
  SecondaryGroups: SecondaryGroupsTarget + Send + Sync,
{
  type Output = TaskResult<H::User, UpsertUserByNameError>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let old_user = host
      .try_user_from_name(&self.name)
      .map_err(UpsertUserByNameError::CheckOldValue)?;

    // https://github.com/shadow-maint/shadow
    if let Some(old_user) = old_user.as_ref() {
      let old_groups = {
        let primary_gid = old_user.gid();
        let group_ids: Vec<Gid> = host.get_user_groups(old_user.name(), primary_gid)?;
        let mut group_names: BTreeSet<String> = BTreeSet::new();
        for gid in group_ids.into_iter() {
          let group = host
            .group_from_id(gid)
            .map_err(|e| UpsertUserByNameError::GroupNotFound(gid, e))?;
          group_names.insert(group.name().to_string());
        }
        group_names
      };

      // Diff the fields and apply updates
      if let Some(diff) = self.diff(old_user, &old_groups) {
        if let Some(target_is_system) = diff.system {
          // TODO: Proper system id detection
          let old_is_system = (0..1000).contains(&old_user.uid().into_raw());
          if target_is_system != old_is_system {
            eprintln!(
              "[UpsertUserByName] User {:?} already exists: skipping enforcing `system`",
              &self.name
            );
          }
        }

        let mut cmd = Command::new("usermod");
        if let Some(uid) = diff.uid {
          cmd = cmd.arg("--uid").arg(uid.to_string());
        }
        if let Some(group) = diff.group {
          cmd = cmd.arg("--gid").arg(group.to_string());
        }
        if let Some(secondary_groups) = diff.secondary_groups {
          let groups: String = secondary_groups
            .iter()
            .map(String::as_str)
            .collect::<Vec<_>>()
            .as_slice()
            .join(",");
          cmd = cmd.arg("--groups").arg(groups);
        }
        if let Some(shell) = diff.shell {
          cmd = cmd.arg("--shell").arg(shell.to_str().unwrap());
        }
        if let Some(password) = diff.password {
          cmd = cmd.arg("--password").arg(password);
        }
        if let Some(comment) = diff.comment {
          cmd = cmd.arg("--comment").arg(comment.clone());
        }
        if let Some(home) = diff.home {
          cmd = cmd.arg("--home-dir").arg(home.to_str().unwrap());
        }
        cmd = cmd.arg(&self.name);
        host
          .exec(&cmd)
          .map_err(|e| UpsertUserByNameError::Update(e.to_string()))?;
      }
    } else {
      // Missing user, create it
      let mut cmd = Command::new("useradd");
      if let Some(uid) = self.uid {
        cmd = cmd.arg("--uid").arg(uid.to_string());
      }
      if let Some(group) = self.group.as_ref() {
        cmd = cmd.arg("--gid").arg(group.to_string());
      }
      {
        let mut secondary_groups = BTreeSet::new();
        self.secondary_groups.fix(&mut secondary_groups);
        if !secondary_groups.is_empty() {
          let groups: String = secondary_groups
            .iter()
            .map(String::as_str)
            .collect::<Vec<_>>()
            .as_slice()
            .join(",");
          cmd = cmd.arg("--groups").arg(groups);
        }
      }
      if let Some(shell) = &self.shell {
        cmd = cmd.arg("--shell").arg(shell.to_str().unwrap());
      }
      if let Some(password) = &self.password {
        cmd = cmd.arg("--password").arg(password);
      }
      if let Some(comment) = &self.comment {
        cmd = cmd.arg("--comment").arg(comment.clone());
      }
      cmd = cmd.arg("--no-create-home");
      if let Some(home) = &self.home {
        cmd = cmd.arg("--home-dir").arg(home.to_str().unwrap());
      }
      if self.system.unwrap_or(false) {
        cmd = cmd.arg("--system");
      }
      cmd = cmd.arg(&self.name);
      host
        .exec(&cmd)
        .map_err(|e| UpsertUserByNameError::Create(e.to_string()))?;
    }

    let new_user = host
      .try_user_from_name(&self.name)
      .map_err(UpsertUserByNameError::CheckNewValue)?;
    let changed = old_user != new_user;

    match new_user {
      Some(user) => Ok(TaskSuccess { changed, output: user }),
      None => Err(UpsertUserByNameError::Confirm),
    }
  }
}

impl<SecondaryGroups: SecondaryGroupsTarget> UpsertUserByName<SecondaryGroups> {
  /// Checks if `user` matches the properties defined by `self`
  ///
  /// Returns the difference:
  /// - `None` indicates that the user is already up-to-date
  /// - `Some` returns a diff with the updates that should be applied in order
  ///   for the user to match the expected properties.
  fn diff<LU: LinuxUser + ?Sized>(&self, user: &LU, secondary_groups: &BTreeSet<String>) -> Option<UpsertUserDiff> {
    let uid = match self.uid {
      Some(uid) if uid != user.uid() => Some(uid),
      _ => None,
    };
    let group = match self.group.as_ref() {
      // TODO: Once `bindings_after_at` is stable, flatten with `Some(group @ GroupRef::Id(gid)) if *gid != Gid::from(user.gid) => Some(group)`
      Some(group) => match group {
        GroupRef::Name(_) => Some(group),
        GroupRef::Id(gid) if *gid != user.gid() => Some(group),
        _ => None,
      },
      None => None,
    };
    let secondary_groups = if self.secondary_groups.test(secondary_groups) {
      None
    } else {
      let mut fixed = secondary_groups.clone();
      self.secondary_groups.fix(&mut fixed);
      Some(fixed)
    };
    let shell = match self.shell.as_ref() {
      Some(shell) if *shell != user.shell() => Some(shell),
      _ => None,
    };
    let password = self.password.as_ref();
    let comment = self.password.as_ref();
    let home = match self.home.as_ref() {
      Some(home) if *home != user.home() => Some(home),
      _ => None,
    };
    let system = if let Some(expected_system) = self.system {
      if expected_system != is_system_user(user.uid()) {
        self.system
      } else {
        None
      }
    } else {
      None
    };

    let is_all_none = uid.is_none()
      && group.is_none()
      && secondary_groups.is_none()
      && shell.is_none()
      && password.is_none()
      && comment.is_none()
      && home.is_none()
      && system.is_none();

    if is_all_none {
      None
    } else {
      Some(UpsertUserDiff {
        uid,
        group,
        secondary_groups,
        shell,
        password,
        comment,
        home,
        system,
      })
    }
  }
}

#[derive(Debug, Clone, PartialEq, Eq)]
struct UpsertUserDiff<'a> {
  uid: Option<Uid>,
  group: Option<&'a GroupRef>,
  secondary_groups: Option<BTreeSet<String>>,
  shell: Option<&'a PathBuf>,
  password: Option<&'a String>,
  comment: Option<&'a String>,
  home: Option<&'a PathBuf>,
  system: Option<bool>,
}

pub struct Group;

impl Group {
  pub fn upsert_by_name(name: &str) -> UpsertGroupByNameBuilder {
    UpsertGroupByNameBuilder::new(name)
  }
}

pub struct User;

impl User {
  pub fn upsert_by_name(name: &str) -> UpsertUserByNameBuilder<AnySecondaryGroupsTarget> {
    UpsertUserByNameBuilder::new(name)
  }
}

pub enum Password {
  /// Disable logging as this user using a password.
  Locked,
  /// Set the raw password value using the provided string (MCH/PHC format)
  Raw(String),
}

impl Password {
  /// Generate the encrypted password hash from the provided clear password string.
  ///
  /// The string will be encrypted with `sha512_crypt`, compatible with Linux.
  ///
  /// - <https://passlib.readthedocs.io/en/stable/modular_crypt_format.html>
  /// - <https://github.com/P-H-C/phc-string-format/blob/master/phc-sf-spec.md>
  pub fn encrypt_sha512(s: impl AsRef<str>) -> Result<Self, CryptError> {
    let params = Sha512Params::default();
    let phc = sha512_simple(s.as_ref(), &params)?;
    Ok(Self::Raw(phc))
  }
}

pub struct UpsertUserByNameBuilder<SecondaryGroups: SecondaryGroupsTarget> {
  name: String,
  password: Option<String>,
  system: Option<bool>,
  comment: Option<String>,
  group: Option<GroupRef>,
  secondary_groups: SecondaryGroups,
  shell: Option<PathBuf>,
  home: Option<PathBuf>,
  uid: Option<Uid>,
}

impl UpsertUserByNameBuilder<AnySecondaryGroupsTarget> {
  pub fn new(name: &str) -> Self {
    Self {
      name: name.into(),
      password: None,
      system: None,
      comment: None,
      group: None,
      secondary_groups: AnySecondaryGroupsTarget,
      shell: None,
      home: None,
      uid: None,
    }
  }
}

impl<SecondaryGroups: SecondaryGroupsTarget> UpsertUserByNameBuilder<SecondaryGroups> {
  pub fn i_am_really_sure_i_want_an_unlocked_passwordless_user(mut self) -> Self {
    self.password = Some(String::new());
    self
  }

  pub fn password(mut self, password: &Password) -> Self {
    let password = match password {
      Password::Locked => "!*".to_string(),
      Password::Raw(s) => s.clone(),
    };
    self.password = Some(password);
    self
  }

  pub fn system(mut self, system: bool) -> Self {
    self.system = Some(system);
    self
  }

  pub fn comment(mut self, comment: impl AsRef<str>) -> Self {
    self.comment = Some(comment.as_ref().to_string());
    self
  }

  pub fn shell(mut self, shell: impl AsRef<Path>) -> Self {
    self.shell = Some(shell.as_ref().to_path_buf());
    self
  }

  pub fn home(mut self, home: impl AsRef<Path>) -> Self {
    self.home = Some(home.as_ref().to_path_buf());
    self
  }

  pub fn uid(mut self, uid: impl Into<Uid>) -> Self {
    self.uid = Some(uid.into());
    self
  }

  pub fn group(mut self, group: GroupRef) -> Self {
    self.group = Some(group);
    self
  }

  pub fn secondary_groups<NewSecondaryGroups: SecondaryGroupsTarget>(
    self,
    secondary_groups: NewSecondaryGroups,
  ) -> UpsertUserByNameBuilder<NewSecondaryGroups> {
    UpsertUserByNameBuilder {
      name: self.name,
      password: self.password,
      system: self.system,
      comment: self.comment,
      group: self.group,
      secondary_groups,
      shell: self.shell,
      home: self.home,
      uid: self.uid,
    }
  }

  pub fn clear_secondary_groups(self) -> UpsertUserByNameBuilder<ExactSecondaryGroupsTarget> {
    UpsertUserByNameBuilder {
      name: self.name,
      password: self.password,
      system: self.system,
      comment: self.comment,
      group: self.group,
      secondary_groups: ExactSecondaryGroupsTarget::new(),
      shell: self.shell,
      home: self.home,
      uid: self.uid,
    }
  }

  pub fn build(self) -> UpsertUserByName<SecondaryGroups> {
    UpsertUserByName {
      name: self.name,
      system: self.system,
      secondary_groups: self.secondary_groups,
      comment: self.comment,
      home: self.home,
      uid: self.uid,
      group: self.group,
      password: self.password,
      shell: self.shell,
    }
  }
}

impl UpsertUserByNameBuilder<ExactSecondaryGroupsTarget> {
  pub fn add_secondary_group(mut self, group: impl AsRef<str>) -> Self {
    self.secondary_groups.groups.insert(group.as_ref().to_string());
    self
  }

  pub fn remove_secondary_group(mut self, group: impl AsRef<str>) -> Self {
    self.secondary_groups.groups.remove(group.as_ref());
    self
  }
}

impl UpsertUserByNameBuilder<AnySecondaryGroupsTarget> {
  pub fn add_secondary_group(self, group: impl AsRef<str>) -> UpsertUserByNameBuilder<PartialSecondaryGroupsTarget> {
    let mut secondary_groups: PartialSecondaryGroupsTarget = PartialSecondaryGroupsTarget {
      groups: BTreeMap::new(),
    };
    secondary_groups.groups.insert(group.as_ref().to_string(), true);
    UpsertUserByNameBuilder {
      name: self.name,
      system: self.system,
      secondary_groups,
      comment: self.comment,
      home: self.home,
      uid: self.uid,
      group: self.group,
      password: self.password,
      shell: self.shell,
    }
  }

  pub fn remove_secondary_group(self, group: impl AsRef<str>) -> UpsertUserByNameBuilder<PartialSecondaryGroupsTarget> {
    let mut secondary_groups: PartialSecondaryGroupsTarget = PartialSecondaryGroupsTarget {
      groups: BTreeMap::new(),
    };
    secondary_groups.groups.insert(group.as_ref().to_string(), false);
    UpsertUserByNameBuilder {
      name: self.name,
      system: self.system,
      secondary_groups,
      comment: self.comment,
      home: self.home,
      uid: self.uid,
      group: self.group,
      password: self.password,
      shell: self.shell,
    }
  }
}

impl<SecondaryGroups: SecondaryGroupsTarget + Send + Sync> UpsertUserByNameBuilder<SecondaryGroups> {
  pub async fn run<H>(self, host: &H) -> <UpsertUserByName<SecondaryGroups> as AsyncFn<&H>>::Output
  where
    H: ExecHost + LinuxUserHost,
    H::User: Debug,
  {
    self.build().run(host).await
  }
}

pub struct UpsertGroupByNameBuilder {
  name: String,
  gid: Option<Gid>,
}

impl UpsertGroupByNameBuilder {
  pub fn new(name: &str) -> Self {
    Self {
      name: name.into(),
      gid: None,
    }
  }

  pub fn gid(mut self, gid: impl Into<Gid>) -> Self {
    self.gid = Some(gid.into());
    self
  }

  pub fn build(self) -> UpsertGroupByName {
    UpsertGroupByName {
      name: self.name,
      gid: self.gid,
    }
  }

  pub async fn run<H: ExecHost + LinuxUserHost>(self, host: &H) -> <UpsertGroupByName as AsyncFn<&H>>::Output {
    self.build().run(host).await
  }
}
