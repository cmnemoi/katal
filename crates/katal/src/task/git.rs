use crate::task::fs::EnsureDir;
use crate::task::{AsyncFn, NamedTask, TaskName, TaskResult, TaskSuccess};
use async_trait::async_trait;
use asys::common::fs::FsHost;
use asys::linux::fs::{LinuxFsHost, LinuxMetadata};
use asys::linux::user::LinuxUserHost;
use asys::ExecHost;
use git2::{AutotagOption, FetchOptions, Object, Oid, Repository, RepositoryInitOptions, ResetType};
use serde::{Deserialize, Serialize};
use std::path::PathBuf;
use thiserror::Error;

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub struct GitCheckout {
  /// Path to the local git repository
  pub path: PathBuf,
  /// Remote URL for the repository
  pub remote: String,
  /// Git reference to checkout
  pub r#ref: String,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize, Error)]
pub enum GitCheckoutError {
  #[error("failed to initialize target directory")]
  EnsureDir(String),
  #[error("failed to initialize repository: {0}")]
  InitRepo(String),
  #[error("failed to create anonymous remote repository reference: {0}")]
  Remote(String),
  #[error("git fetch failed: {0}")]
  Fetch(String),
  #[error("no commit found for the provided reference")]
  CommitNotFound,
  #[error("failed to reset repository state: {0}")]
  Reset(String),
}

#[async_trait]
impl<'h, H> AsyncFn<&'h H> for GitCheckout
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
{
  type Output = TaskResult<(), GitCheckoutError>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let mut changed = false;

    changed = EnsureDir::new(self.path.clone())
      .run(host)
      .await
      .map_err(|e| GitCheckoutError::EnsureDir(e.to_string()))?
      .changed
      || changed;

    let mut init_opts = RepositoryInitOptions::new();
    init_opts.mkpath(false);
    init_opts.mkdir(false);
    init_opts.external_template(false);
    // init_opts.origin_url(&self.remote);
    let repository =
      Repository::init_opts(&self.path, &init_opts).map_err(|e| GitCheckoutError::InitRepo(e.to_string()))?;

    let mut remote = repository
      .remote_anonymous(&self.remote)
      .map_err(|e| GitCheckoutError::Remote(e.to_string()))?;
    {
      // TODO: Switch to shallow fetch once <https://github.com/libgit2/libgit2/issues/3058> is fixed
      // TODO: Also keep an eye on <https://github.com/libgit2/libgit2/issues/6135>
      let mut fetch_opts = FetchOptions::new();
      fetch_opts.download_tags(AutotagOption::All);
      // TODO: Avoid hard-coding common branch names
      let all_refs: [&str; 5] = ["master", "develop", "main", "dev", &self.r#ref];
      remote
        .fetch(&all_refs, Some(&mut fetch_opts), None)
        .map_err(|e| GitCheckoutError::Fetch(e.to_string()))?;
    }

    let resolved = resolve_commit(&repository, &self.r#ref).ok_or(GitCheckoutError::CommitNotFound)?;
    repository
      .reset(&resolved, ResetType::Hard, None)
      .map_err(|e| GitCheckoutError::Reset(e.to_string()))?;

    Ok(TaskSuccess { changed, output: () })
  }
}

impl<'h, H> NamedTask<&'h H> for GitCheckout
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
{
  const NAME: TaskName = TaskName::new("katal::task::git::GitCheckout");
}

fn resolve_commit<'r>(repository: &'r Repository, git_ref: &'r str) -> Option<Object<'r>> {
  {
    if let Ok(commit_oid) = Oid::from_str(git_ref) {
      if let Ok(commit) = repository.find_commit(commit_oid) {
        return Some(commit.into_object());
      }
    }
  }
  let tag_ref = repository.resolve_reference_from_short_name(git_ref).ok()?;
  let tag_oid = tag_ref.target()?;
  let tag = repository.find_tag(tag_oid).ok()?;
  Some(tag.into_object())
}
