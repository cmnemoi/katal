use crate::etwin::channel::{
  ChannelBorgbase, ChannelDomain, ChannelFallbackServer, ChannelPaths, ChannelPostgres, ChannelTarget,
};
use crate::pm2::{Pm2App, Pm2Ecosystem};
use crate::task::fs::{EnsureDir, EnsureDirSymlink, EnsureFile};
use crate::task::git::GitCheckout;
use crate::task::nginx::{NginxAvailableSite, NginxEnableSite};
use crate::task::nvm::NvmEnv;
use crate::task::systemd::{SystemdState, SystemdUnit};
use crate::task::{exec_as, AsyncFn, NamedTask, TaskName, TaskResult, TaskSuccess};
use async_trait::async_trait;
use asys::common::fs::FsHost;
use asys::linux::fs::{FileMode, LinuxFsHost, LinuxMetadata, ResolveError};
use asys::linux::user::{Gid, GroupRef, LinuxUser, LinuxUserHost, Uid, UserRef};
use asys::local_linux::LocalLinux;
use asys::{Command, ExecHost};
use serde::{Deserialize, Serialize};
use std::fmt;
use std::fmt::Debug;
use std::path::PathBuf;

const SKIP_BUILD: bool = false;
const SKIP_CHECKOUT: bool = false;

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct DinorpgAppTarget<S: AsRef<str> = String> {
  pub name: S,
  pub user_name: S,
  pub group_name: S,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct DinorpgApp<S: AsRef<str> = String> {
  name: S,
  user_name: S,
  group_name: S,
  uid: Uid,
  gid: Gid,
  home: PathBuf,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct DinorpgChannelTarget<S: AsRef<str> = String> {
  pub inner: ChannelTarget<S>,
  pub vault: DinorpgVault,
  pub etwin_uri: String,
  pub etwin_client_id: String,
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize, Serialize)]
pub struct DinorpgVault {
  pub secret_key: String,
  pub etwin_client_secret: String,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct DinorpgChannel<User: LinuxUser, S: AsRef<str> = String> {
  name: S,
  user: User,
  domain: ChannelDomain<S>,
  paths: ChannelPaths,
  main_port: u16,
  fallback_server: ChannelFallbackServer,
  postgres: ChannelPostgres,
  nvm: NvmEnv,
  vault: DinorpgVault,
  borg: Option<ChannelBorgbase>,
  etwin_uri: String,
  etwin_client_id: String,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct DinorpgReleaseTarget<S: AsRef<str> = String> {
  pub channel: DinorpgChannelTarget,
  pub repository: S,
  pub git_ref: String,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct DinorpgRelease<User: LinuxUser, S: AsRef<str> = String> {
  release_dir: PathBuf,
  repository: S,
  repo_dir: PathBuf,
  channel: DinorpgChannel<User, S>,
  git_ref: String,
}

#[derive(Serialize, Deserialize)]
pub struct DeployDinorpg<S: AsRef<str> = String> {
  pub pass: S,
  pub target: DinorpgReleaseTarget,
}

#[async_trait]
impl<'h, H, S> AsyncFn<&'h H> for DeployDinorpg<S>
where
  H: ExecHost,
  S: AsRef<str> + Send + Sync,
{
  type Output = TaskResult<(), String>;

  async fn run(&self, _host: &'h H) -> Self::Output {
    let input = DeployDinorpgAsRoot {
      target: self.target.clone(),
    };
    exec_as::<LocalLinux, DeployDinorpgAsRoot>(UserRef::ROOT, Some(self.pass.as_ref()), &input)
      .await
      .map_err(|e| e.to_string())?
  }
}

impl<'h, H: 'h + ExecHost> NamedTask<&'h H> for DeployDinorpg {
  const NAME: TaskName = TaskName::new("dinorpg::DeployDinorpg");
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct DeployDinorpgAsRoot {
  target: DinorpgReleaseTarget,
}

#[async_trait]
impl<'h, H> AsyncFn<&'h H> for DeployDinorpgAsRoot
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  H::User: Debug + Clone + Send + Sync,
  H::Group: Debug + Send + Sync,
  <H as FsHost>::Metadata: LinuxMetadata,
{
  type Output = TaskResult<(), String>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let res = self.target.run(host).await;
    if let Err(e) = res {
      panic!("{:?}", e);
    }
    let res = res.unwrap();

    Ok(TaskSuccess {
      changed: res.changed,
      output: (),
    })
  }
}

impl<'h, H> NamedTask<&'h H> for DeployDinorpgAsRoot
where
  H: 'h + ExecHost + LinuxUserHost + LinuxFsHost,
  H::User: Debug + Clone + Send + Sync,
  H::Group: Debug + Send + Sync,
  <H as FsHost>::Metadata: LinuxMetadata,
{
  const NAME: TaskName = TaskName::new("dinorpg::DeployDinorpgAsRoot");
}

#[async_trait]
impl<'h, H> AsyncFn<&'h H> for DinorpgReleaseTarget
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  H::User: Debug + Clone + Send + Sync,
  H::Group: Debug + Send + Sync,
  <H as FsHost>::Metadata: LinuxMetadata,
{
  type Output = TaskResult<DinorpgRelease<H::User>, anyhow::Error>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let res = self.channel.run(host).await?;
    let channel: DinorpgChannel<H::User> = res.output;
    let mut changed = res.changed;

    let release_dir = channel.paths.releases.join(&self.git_ref);

    eprintln!("Create directory: {}", release_dir.display());
    changed = EnsureDir::new(release_dir.clone())
      .owner(UserRef::Id(channel.user.uid()))
      .group(GroupRef::Id(channel.user.gid()))
      .mode(FileMode::OWNER_ALL | FileMode::ALL_READ | FileMode::ALL_EXEC)
      .run(host)
      .await?
      .changed
      || changed;

    let repo_dir = release_dir.join("repo");

    let br = BuildDinorpgRelease {
      repo_dir: repo_dir.clone(),
      remote: self.repository.clone(),
      git_ref: self.git_ref.clone(),
      backend_config: config::BackendConfig::from_channel(&channel),
      frontend_config: config::FrontendConfig::from_channel_and_commit(&channel, self.git_ref.clone()),
    };

    if !SKIP_BUILD {
      changed = exec_as::<LocalLinux, _>(UserRef::Id(channel.user.uid()), None, &br)
        .await?
        .unwrap()
        .changed
        || changed;
    }

    let release = DinorpgRelease {
      release_dir,
      repository: self.repository.clone(),
      repo_dir,
      channel,
      git_ref: self.git_ref.clone(),
    };

    changed = (UpdateActiveRelease {
      release: release.clone(),
    })
    .run(host)
    .await?
    .changed
      || changed;

    Ok(TaskSuccess {
      changed,
      output: release,
    })
  }
}

#[async_trait]
impl<'h, H> AsyncFn<&'h H> for DinorpgChannelTarget
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  H::Group: Send + Sync,
  H::User: Send + Sync + Debug + Clone,
  <H as FsHost>::Metadata: LinuxMetadata,
{
  type Output = TaskResult<DinorpgChannel<H::User>, anyhow::Error>;

  async fn run(&self, host: &'h H) -> Self::Output {
    eprintln!("Dinorpg: Create channel");
    let res = self.inner.run(host).await?;

    eprintln!("Dinorpg: Channel ready");
    let output = DinorpgChannel {
      name: res.output.name,
      user: res.output.user,
      domain: res.output.domain.expect("Expected associated domain name"),
      paths: res.output.paths,
      main_port: res.output.main_port.expect("Expected main port"),
      fallback_server: res.output.fallback_server.expect("Expected fallback server"),
      postgres: res.output.postgres.expect("Expected Postgres database"),
      nvm: res.output.nvm.expect("Expected NVM environment"),
      vault: self.vault.clone(),
      borg: res.output.borg,
      etwin_uri: self.etwin_uri.clone(),
      etwin_client_id: self.etwin_client_id.clone(),
    };

    Ok(TaskSuccess {
      changed: res.changed,
      output,
    })
  }
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct BuildDinorpgRelease {
  repo_dir: PathBuf,
  remote: String,
  git_ref: String,
  backend_config: config::BackendConfig,
  frontend_config: config::FrontendConfig,
}

#[async_trait]
impl<'h, H: ExecHost + LinuxUserHost + LinuxFsHost> AsyncFn<&'h H> for BuildDinorpgRelease
where
  <H as FsHost>::Metadata: LinuxMetadata,
{
  type Output = Result<TaskSuccess<()>, ()>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let mut changed: bool;
    if !SKIP_CHECKOUT {
      let gc = GitCheckout {
        path: self.repo_dir.clone(),
        remote: self.remote.clone(),
        r#ref: self.git_ref.clone(),
      };

      eprintln!("Checkout repository: {:?} {:?}", self.remote, self.git_ref);
      let res = gc.run(host).await.unwrap();
      changed = res.changed;
    } else {
      changed = false;
    }

    let backend_dir = self.repo_dir.join("ed-be");
    let frontend_dir = self.repo_dir.join("ed-ui");

    eprintln!("Install Node dependencies");
    let cmd = Command::new("yarn")
      .arg("install")
      .arg("--immutable")
      .current_dir(self.repo_dir.to_str().unwrap());
    host.exec(&cmd).unwrap();

    eprintln!("Configure the frontend build");
    EnsureFile::new(frontend_dir.join(".env.production"))
      .content(self.frontend_config.to_string())
      .mode(FileMode::OWNER_READ | FileMode::OWNER_WRITE)
      .run(host)
      .await
      .unwrap();

    eprintln!("Build the project");
    let cmd = Command::new("yarn")
      .arg("run")
      .arg("build")
      .env("NODE_ENV", "production")
      .current_dir(self.repo_dir.to_str().unwrap());
    host.exec(&cmd).unwrap();

    eprintln!("Write backend config");
    changed = EnsureFile::new(backend_dir.join("config_production.toml"))
      .content(self.backend_config.to_string())
      // .owner(app.uid)
      // .group(app.gid)
      .mode(FileMode::OWNER_READ | FileMode::OWNER_WRITE)
      .run(host)
      .await
      .unwrap()
      .changed
      || changed;

    Ok(TaskSuccess { changed, output: () })
  }
}

impl<'h, H: 'h + ExecHost + LinuxUserHost + LinuxFsHost> NamedTask<&'h H> for BuildDinorpgRelease
where
  <H as FsHost>::Metadata: LinuxMetadata,
{
  const NAME: TaskName = TaskName::new("dinorpg::BuildDinorpgRelease");
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize)]
pub struct UpdateActiveRelease<User: LinuxUser, S: AsRef<str> = String> {
  release: DinorpgRelease<User, S>,
}

#[async_trait]
impl<'h, H, S> AsyncFn<&'h H> for UpdateActiveRelease<H::User, S>
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Send + Sync,
  S: AsRef<str> + Send + Sync,
{
  type Output = TaskResult<(), anyhow::Error>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let uid = self.release.channel.user.uid();
    let gid = self.release.channel.user.gid();

    let service_name = format!("{}.service", self.release.channel.name.as_ref());

    let mut changed = false;
    let active_release_link = &self.release.channel.paths.active_release_link;
    let previous_release_link = &self.release.channel.paths.previous_release_link;
    let old_release_dir = {
      match host.resolve(active_release_link) {
        Ok(old) => Some(old),
        Err(ResolveError::NotFound) => None,
        Err(e) => return Err(e.into()),
      }
    };
    let new_release_dir = host.resolve(&self.release.release_dir)?;

    if let Some(old_release_dir) = old_release_dir {
      if old_release_dir != new_release_dir {
        eprintln!("Start to disable old release");
        // There is already an active release but it does not match the target
        // release: stop the old version before proceeding to free the channel
        // resources (database, port)
        EnsureDirSymlink::new(previous_release_link.clone())
          .points_to(old_release_dir.clone())
          .owner(uid)
          .group(gid)
          .mode(FileMode::ALL_READ | FileMode::ALL_EXEC)
          .run(host)
          .await?;

        SystemdUnit::new(&service_name)
          .state(SystemdState::Inactive)
          .run(host)
          .await?;
        eprintln!("Old release disabled");

        // if let Some(borg) = self.release.channel.borg.as_ref() {
        //   eprintln!("Creating archive before release");
        //   let backup_dir = self.release.channel.paths.home.join("backup");
        //   EnsureDir::new(backup_dir.clone()).run(host).await?;
        //   let example_file = backup_dir.join("etwin.pgdump");
        //   EnsureFile::new(example_file.clone()).content("Hey").run(host).await?;
        //
        //   changed = exec_as::<LocalLinux, _>(
        //     UserRef::Id(uid),
        //     None,
        //     &CreateBorgArchive::new_from_time(
        //       borg.password.clone(),
        //       borg.ssh_priv_key.clone(),
        //       borg.repo.clone(),
        //       backup_dir,
        //     ),
        //   )
        //   .await?
        //   .unwrap()
        //   .changed
        //     || changed;
        // } else {
        //   eprintln!("Skipping pre-release archive: no borg config");
        // }

        changed = true;
      }
    }

    // TODO: Trigger backup

    eprintln!("UpgradingDatabase");
    changed = exec_as::<LocalLinux, _>(
      UserRef::Id(uid),
      None,
      &UpgradeDinorpgDatabase {
        repo_dir: self.release.repo_dir.clone(),
        postgres: self.release.channel.postgres.clone(),
      },
    )
    .await?
    .unwrap()
    .changed
      || changed;

    if let Some(_borg) = self.release.channel.borg.as_ref() {
      eprintln!("WARNING: backups are not supported for DinoRPG");
    }

    eprintln!("Generating PM2 ecosystem");
    let ecosystem = Pm2Ecosystem {
      apps: vec![{
        Pm2App::new(
          self.release.channel.name.as_ref().to_string(),
          self.release.repo_dir.join("ed-be/dist/server.js"),
        )
        .env("NODE_ENV", "production")
        .env("PORT", self.release.channel.main_port.to_string())
        // .env("NVM_DIR", self.release.channel.app.home.join(".nvm").to_str().unwrap())
        .cwd(self.release.repo_dir.join("ed-be"))
        .node_args("--experimental-wasm-modules")
        .interpreter("node@16.13.1")
      }],
    };

    let mut ecosystem = serde_json::to_string_pretty(&ecosystem).unwrap();
    ecosystem.push('\n');

    eprintln!("Writing PM2 ecosystem");
    changed = EnsureFile::new(new_release_dir.join("ecosystem.config.json"))
      .content(ecosystem)
      .owner(uid)
      .group(gid)
      .mode(FileMode::ALL_READ)
      .run(host)
      .await?
      .changed
      || changed;

    eprintln!("Generating Systemd unit");
    let service = DinorpgService::from_channel(&self.release.channel);
    eprintln!("Writing Systemd unit");
    changed = EnsureFile::new(PathBuf::from("/etc/systemd/system").join(&service_name))
      .content(service.to_string())
      .owner(Uid::ROOT)
      .mode(FileMode::ALL_READ)
      .run(host)
      .await?
      .changed
      || changed;

    eprintln!("Generating nginx config");
    let nginx_config = get_nginx_config(&self.release.channel);

    eprintln!("Marking release as active");
    EnsureDirSymlink::new(active_release_link.clone())
      .points_to(new_release_dir.clone())
      .owner(uid)
      .group(gid)
      .mode(FileMode::ALL_READ | FileMode::ALL_EXEC)
      .run(host)
      .await?;

    eprintln!("Starting service");
    changed = SystemdUnit::new(&service_name)
      .enabled(true)
      .state(SystemdState::Active)
      .run(host)
      .await?
      .changed
      || changed;

    eprintln!("Writing nginx config");
    NginxAvailableSite::new(self.release.channel.name.as_ref(), nginx_config)
      .run(host)
      .await?;

    eprintln!("Enabling nginx config");
    NginxEnableSite::new(self.release.channel.name.as_ref())
      .run(host)
      .await?;

    Ok(TaskSuccess { changed, output: () })
  }
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct UpgradeDinorpgDatabase {
  repo_dir: PathBuf,
  postgres: ChannelPostgres,
}

#[async_trait]
impl<'h, H> AsyncFn<&'h H> for UpgradeDinorpgDatabase
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
{
  type Output = Result<TaskSuccess<()>, ()>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let changed = false;

    // NODE_VERSION=16 ~/.nvm/nvm-exec NODE_ENV=production yarn run migration
    // All it does is basically adding `/home/demurgos/.nvm/versions/node/v16.17.1/bin` to the path.
    // demurgos@red /home/demurgos
    // $ nvm which 16
    // /home/demurgos/.nvm/versions/node/v16.17.1/bin/node
    // demurgos@red /home/demurgos
    // $ nvm which 17
    // N/A: version "v17" is not yet installed.
    //
    // You need to run "nvm install 17" to install it before using it.
    // demurgos@red /home/demurgos
    // $ echo $?
    // 1
    let cmd = Command::new("yarn")
      .arg("run")
      .arg("migration")
      .env("NODE_ENV", "production")
      .current_dir(self.repo_dir.to_str().unwrap());
    host.exec(&cmd).unwrap();

    Ok(TaskSuccess { changed, output: () })
  }
}

impl<'h, H> NamedTask<&'h H> for UpgradeDinorpgDatabase
where
  H: 'h + ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
{
  const NAME: TaskName = TaskName::new("dinorpg::UpgradeDinorpgDatabase");
}

mod config {
  use crate::etwin::dinorpg::DinorpgChannel;
  use asys::linux::user::LinuxUser;
  use serde::{Deserialize, Serialize};
  use std::collections::BTreeMap;
  use std::fmt;
  use url::Url;

  #[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
  pub struct BackendConfig {
    pub general: GeneralConfig,
    pub oauth: OauthConfig,
    pub db: DbConfig,
    pub jwt: JwtConfig,
    pub shop: ShopConfig,
    pub player: PlayerConfig,
    pub admin: BTreeMap<String, String>,
  }

  impl BackendConfig {
    pub fn from_channel(channel: &DinorpgChannel<impl LinuxUser>) -> Self {
      const SECONDS_PER_DAY: u32 = 86400;
      let protocol = if channel.domain.cert.is_some() { "https" } else { "http" };
      let uri = format!("{}://{}", protocol, channel.domain.main.as_str());
      Self {
        general: GeneralConfig {
          eternaltwin_uri: Url::parse(&channel.etwin_uri).unwrap(),
          eternaltwin_docker_uri: Url::parse(&channel.etwin_uri).unwrap(),
          server_uri: Url::parse(&uri).unwrap(),
          front_uri: Url::parse(&uri).unwrap(),
        },
        oauth: OauthConfig {
          client_id: channel.etwin_client_id.clone(),
          client_secret: channel.vault.etwin_client_secret.clone(),
          authorization_uri: "oauth/authorize".to_string(),
          token_uri: "oauth/token".to_string(),
          callback_uri: "authentication".to_string(),
        },
        db: DbConfig {
          host: channel.postgres.host.clone(),
          db_name: channel.postgres.name.clone(),
          // TODO: Avoid admin credentials
          user: channel.postgres.admin.name.to_string(),
          password: channel.postgres.admin.password.clone(),
        },
        jwt: JwtConfig {
          secret_key: channel.vault.secret_key.clone(),
          expiration: 30 * SECONDS_PER_DAY,
        },
        shop: ShopConfig {
          dinoz_in_shop: 15,
          buyable_quetzu: 6,
        },
        player: PlayerConfig { initial_money: 200000 },
        admin: BTreeMap::from_iter(
          [
            ("jolu".to_string(), "e84dfa4a-6732-432f-97a9-24c42e02763f".to_string()),
            ("biosha".to_string(), "eb989f16-94a4-47ab-a4bb-151c3f529fac".to_string()),
            ("jahaa".to_string(), "02f5f8cc-1c92-499a-a5eb-645beef22d33".to_string()),
          ]
          .into_iter(),
        ),
      }
    }
  }

  impl fmt::Display for BackendConfig {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
      writeln!(f, "{}", toml::to_string_pretty(&self).unwrap())
    }
  }

  #[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
  pub struct GeneralConfig {
    #[serde(rename = "eternalTwinPublicUri")]
    pub eternaltwin_uri: Url,
    #[serde(rename = "eternalTwinServerUri")]
    pub eternaltwin_docker_uri: Url,
    #[serde(rename = "serverUri")]
    pub server_uri: Url,
    #[serde(rename = "frontUri")]
    pub front_uri: Url,
  }

  #[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
  pub struct OauthConfig {
    #[serde(rename = "clientId")]
    pub client_id: String,
    #[serde(rename = "clientSecret")]
    pub client_secret: String,
    #[serde(rename = "authorizationUri")]
    pub authorization_uri: String,
    #[serde(rename = "tokenUri")]
    pub token_uri: String,
    #[serde(rename = "callbackUri")]
    pub callback_uri: String,
  }

  #[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
  pub struct DbConfig {
    pub host: String,
    #[serde(rename = "dbName")]
    pub db_name: String,
    pub user: String,
    pub password: String,
  }

  #[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
  pub struct JwtConfig {
    #[serde(rename = "secretKey")]
    pub secret_key: String,
    pub expiration: u32,
  }

  #[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
  pub struct ShopConfig {
    #[serde(rename = "dinozInShop")]
    pub dinoz_in_shop: u32,
    #[serde(rename = "buyableQuetzu")]
    pub buyable_quetzu: u32,
  }

  #[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
  pub struct PlayerConfig {
    #[serde(rename = "initialMoney")]
    pub initial_money: u32,
  }

  #[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
  pub struct FrontendConfig {
    pub channel: String,
    pub commit: String,
    pub api_url: Url,
  }

  impl FrontendConfig {
    pub fn from_channel_and_commit(channel: &DinorpgChannel<impl LinuxUser>, commit: String) -> Self {
      let protocol = if channel.domain.cert.is_some() { "https" } else { "http" };
      let uri = format!("{}://{}", protocol, channel.domain.main.as_str());
      Self {
        channel: channel.name.clone(),
        commit,
        api_url: Url::parse(&uri).unwrap(),
      }
    }
  }

  impl fmt::Display for FrontendConfig {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
      writeln!(f, "VITE_API_URL={}", self.api_url)?;
      writeln!(f, "VITE_API_RELEASE_COMMIT={}", self.commit)?;
      writeln!(f, "VITE_API_RELEASE_CHANNEL={}", self.channel)?;
      writeln!(f, "NODE_ENV=production")?;
      Ok(())
    }
  }
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
struct DinorpgService<S: AsRef<str> = String> {
  channel_name: S,
  user: S,
  nvm_dir: PathBuf,
  pm2_dir: PathBuf,
  pid_file: PathBuf,
  ecosystem_file: PathBuf,
  working_dir: PathBuf,
}

impl DinorpgService<String> {
  pub fn from_channel<User, S>(channel: &DinorpgChannel<User, S>) -> Self
  where
    User: LinuxUser,
    S: AsRef<str>,
  {
    Self {
      channel_name: channel.name.as_ref().to_string(),
      user: channel.user.name().to_string(),
      nvm_dir: channel.nvm.home.clone(),
      pid_file: channel.paths.home.join(".pm2/pm2.pid"),
      pm2_dir: channel.paths.home.join(".pm2"),
      ecosystem_file: channel.paths.home.join("active/ecosystem.config.json"),
      working_dir: channel.paths.home.join("active/repo"),
    }
  }
}

impl<S: AsRef<str>> fmt::Display for DinorpgService<S> {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    write!(
      f,
      r#"[Unit]
Description=PM2 for Dinorpg ({channel})
Documentation=https://pm2.keymetrics.io/
After=network.target
Requires=postgresql.service

[Service]
Type=forking
User={user}
LimitNOFILE=infinity
LimitNPROC=infinity
LimitCORE=infinity
Environment=PATH=/usr/local/sbin:/usr/local/bin:/usr/bin:/usr/bin/site_perl:/usr/bin/vendor_perl:/usr/bin/core_perl:/usr/bin:/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin
Environment=PM2_HOME={pm2_dir}
Environment=NVM_DIR={nvm_dir}
PIDFile={pid_file}
Restart=on-failure

WorkingDirectory={working_dir}
ExecStart=/usr/bin/pm2 start {ecosystem}
ExecReload=/usr/bin/pm2 reload {ecosystem}
ExecStop=/usr/bin/pm2 stop {ecosystem}

[Install]
WantedBy=multi-user.target
"#,
      channel = self.channel_name.as_ref(),
      user = self.user.as_ref(),
      pm2_dir = self.pm2_dir.display(),
      nvm_dir = self.nvm_dir.display(),
      pid_file = self.pid_file.display(),
      ecosystem = self.ecosystem_file.display(),
      working_dir = self.working_dir.display(),
    )
  }
}

pub fn get_nginx_config<S: AsRef<str>>(channel: &DinorpgChannel<impl LinuxUser, S>) -> String {
  let upstream_name = channel.name.as_ref().to_string().replace('.', "_");
  let access_log = channel.paths.logs.join("main.nginx.access.log");
  let error_log = channel.paths.logs.join("main.nginx.error.log");
  let static_dir = channel.paths.home.join("active/repo/ed-ui/dist");

  let fallback_server = format!(
    r#"# Fallback server
server {{
  # HTTP port
  listen {fallback_port};
  listen [::]:{fallback_port};

  # Hide nginx version in `Server` header.
  server_tokens off;
  add_header Referrer-Policy same-origin;
  add_header X-Content-Type-Options nosniff;
  add_header X-XSS-Protection "1; mode=block";

  index index.html;

  root {fallback_dir};
}}
"#,
    fallback_port = channel.fallback_server.port,
    fallback_dir = channel.fallback_server.dir.to_str().unwrap(),
  );

  let upstream_server = format!(
    r#"# Define target of the reverse-proxy
upstream {upstream_name} {{
  server [::1]:{port};
  server [::1]:{fallback_port} backup;
  keepalive 8;
}}
"#,
    upstream_name = upstream_name,
    port = channel.main_port,
    fallback_port = channel.fallback_server.port,
  );

  let main_directives = format!(
    r#"  # Space-separated hostnames (wildcard * is accepted)
  server_name {domain};

  access_log {access_log};
  error_log {error_log};
  client_max_body_size 100M;

  root {static_dir};

  location ~* ^/(?:api|api-docs)(?:/|$) {{
    # Upstream reference
    proxy_pass http://{upstream_name};

    # Configuration of the proxy
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection 'upgrade';
    proxy_set_header Host $host;
    proxy_set_header X-NginX-Proxy true;
    proxy_cache_bypass $http_upgrade;
    proxy_redirect off;
  }}

  location / {{
    try_files $uri /index.html;
    gzip_static on;
  }}
"#,
    domain = channel.domain.main.as_ref(),
    access_log = access_log.to_str().unwrap(),
    error_log = error_log.to_str().unwrap(),
    static_dir = static_dir.to_str().unwrap(),
    upstream_name = upstream_name,
  );

  match channel.domain.cert.as_ref() {
    Some(https) => format!(
      r#"# This configuration sets a reverse proxy for a specific domain

{fallback_server}

{upstream_server}

# HTTPS
server {{
  # HTTPS port
  listen 443 ssl;
  listen [::]:443 ssl;

  ssl_certificate {cert};
  ssl_certificate_key {cert_key};

  # # HSTS (ngx_http_headers_module is required) (63072000 seconds)
  # add_header Strict-Transport-Security "max-age=63072000" always;

  {main_directives}
}}

# HTTP
server {{
  # HTTP port
  listen 80;
  listen [::]:80;
  server_name {domain};
  return 301 https://$host$request_uri;
}}

# www.
server {{
  listen 80;
  listen 443 ssl;
  listen [::]:80;
  listen [::]:443 ssl;

  server_name www.{domain};

  ssl_certificate {cert};
  ssl_certificate_key {cert_key};

  return 307 https://{domain}$request_uri;
}}

"#,
      fallback_server = fallback_server,
      upstream_server = upstream_server,
      main_directives = main_directives,
      domain = channel.domain.main.as_ref(),
      cert = https.fullchain_cert_path.to_str().unwrap(),
      cert_key = https.cert_key_path.to_str().unwrap(),
    ),
    None => format!(
      r#"# This configuration sets a reverse proxy for a specific domain

{fallback_server}

{upstream_server}

# HTTP
server {{
  # HTTP port
  listen 80;
  listen [::]:80;

  {main_directives}
}}

# www.
server {{
  listen 80;
  listen [::]:80;

  server_name www.{domain};

  return 307 http://{domain}$request_uri;
}}

"#,
      fallback_server = fallback_server,
      upstream_server = upstream_server,
      main_directives = main_directives,
      domain = channel.domain.main.as_ref(),
    ),
  }
}
