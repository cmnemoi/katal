use crate::etwin::channel::{
  ChannelDomain, ChannelFallbackServer, ChannelPaths, ChannelPhp, ChannelPostgres, ChannelTarget, PostgresCredentials,
};
use crate::task::fs::{EnsureDir, EnsureDirSymlink, EnsureFile};
use crate::task::git::GitCheckout;
use crate::task::nginx::{NginxAvailableSite, NginxEnableSite};
use crate::task::postgres::PostgresUrl;
use crate::task::{exec_as, AsyncFn, NamedTask, TaskName, TaskResult, TaskSuccess};
use async_trait::async_trait;
use asys::common::fs::FsHost;
use asys::linux::fs::{FileMode, LinuxFsHost, LinuxMetadata, ResolveError};
use asys::linux::user::{GroupRef, LinuxUser, LinuxUserHost, UserRef};
use asys::local_linux::LocalLinux;
use asys::{Command, ExecHost};
use serde::{Deserialize, Serialize};
use std::fmt;
use std::fmt::Debug;
use std::path::PathBuf;
use url::Url;

const SKIP_BUILD: bool = false;
const PG_VERSION: u8 = 14;

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct EmushChannelTarget<S: AsRef<str> = String> {
  pub inner: ChannelTarget<S>,
  pub vault: EmushVault,
  pub etwin_uri: Url,
  pub etwin_client_id: String,
  pub admin_user_id: Option<String>,
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize, Serialize)]
pub struct EmushVault {
  pub etwin_client_secret: String,
  pub app_secret: String,
  pub access_passphrase: String,
  pub discord_webhook: Option<String>,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct EmushChannel<User: LinuxUser, S: AsRef<str> = String> {
  name: S,
  user: User,
  domain: ChannelDomain<S>,
  paths: ChannelPaths,
  fallback_server: ChannelFallbackServer,
  postgres: ChannelPostgres,
  node: PathBuf,
  php: ChannelPhp,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct EmushReleaseTarget<S: AsRef<str> = String> {
  pub channel: EmushChannelTarget,
  pub repository: S,
  pub git_ref: String,
  /// Flag indicating that the DB was reset recently and needs to be initialized
  pub is_after_db_reset: bool,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct EmushRelease<User: LinuxUser, S: AsRef<str> = String> {
  release_dir: PathBuf,
  repository: S,
  repo_dir: PathBuf,
  channel: EmushChannel<User, S>,
  git_ref: String,
  /// Flag indicating that the DB was reset recently and needs to be initialized
  is_after_db_reset: bool,
}

#[derive(Serialize, Deserialize)]
pub struct DeployEmush<S: AsRef<str> = String> {
  pub pass: S,
  pub target: EmushReleaseTarget,
}

#[async_trait]
impl<'h, H: ExecHost, S: AsRef<str> + Send + Sync> AsyncFn<&'h H> for DeployEmush<S> {
  type Output = TaskResult<(), String>;

  async fn run(&self, _host: &'h H) -> Self::Output {
    let input = DeployEmushAsRoot {
      target: self.target.clone(),
    };
    exec_as::<LocalLinux, DeployEmushAsRoot>(UserRef::ROOT, Some(self.pass.as_ref()), &input)
      .await
      .map_err(|e| e.to_string())?
  }
}

impl<'h, H: 'h + ExecHost> NamedTask<&'h H> for DeployEmush {
  const NAME: TaskName = TaskName::new("emush::DeployEmush");
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct DeployEmushAsRoot {
  target: EmushReleaseTarget,
}

#[async_trait]
impl<'h, H: ExecHost + LinuxUserHost + LinuxFsHost> AsyncFn<&'h H> for DeployEmushAsRoot
where
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Debug + Clone + Send + Sync,
  H::Group: Debug + Send + Sync,
{
  type Output = TaskResult<(), String>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let res = self.target.run(host).await;
    if let Err(e) = res {
      panic!("{:?}", e);
    }
    let res = res.unwrap();

    Ok(TaskSuccess {
      changed: res.changed,
      output: (),
    })
  }
}

impl<'h, H> NamedTask<&'h H> for DeployEmushAsRoot
where
  H: 'h + ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Debug + Clone + Send + Sync,
  H::Group: Debug + Send + Sync,
{
  const NAME: TaskName = TaskName::new("emush::DeployEmushAsRoot");
}

#[async_trait]
impl<'h, H> AsyncFn<&'h H> for EmushReleaseTarget
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Debug + Clone + Send + Sync,
  H::Group: Debug + Send + Sync,
{
  type Output = TaskResult<EmushRelease<H::User>, anyhow::Error>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let res = self.channel.run(host).await?;
    let channel = res.output;
    let mut changed = res.changed;

    let release_dir = channel.paths.releases.join(&self.git_ref);

    eprintln!("Create directory: {}", release_dir.display());
    changed = EnsureDir::new(release_dir.clone())
      .owner(UserRef::Id(channel.user.uid()))
      .group(GroupRef::Id(channel.user.gid()))
      .mode(FileMode::OWNER_ALL | FileMode::ALL_READ | FileMode::ALL_EXEC)
      .run(host)
      .await?
      .changed
      || changed;

    let repo_dir = release_dir.join("repo");

    let protocol = if channel.domain.cert.is_some() { "https" } else { "http" };
    let uri = format!("{}://{}", protocol, channel.domain.main.as_str());

    let br = BuildEmushRelease {
      node: channel.node.clone(),
      repo_dir: repo_dir.clone(),
      remote: self.repository.clone(),
      git_ref: self.git_ref.clone(),
      backend_config: BackendConfig {
        app_secret: self.channel.vault.app_secret.to_string(),
        db_credentials: channel.postgres.main.clone(),
        db_name: channel.postgres.name.clone(),
        db_version: PG_VERSION,
        access_passphrase: self.channel.vault.access_passphrase.clone(),
        identity_server: self.channel.etwin_uri.clone(),
        oauth_callback: Url::parse(format!("{}/oauth/callback", uri.as_str()).as_str())?,
        oauth_client_id: self.channel.etwin_client_id.clone(),
        oauth_client_secret: self.channel.vault.etwin_client_secret.clone(),
        admin_user_id: self.channel.admin_user_id.clone(),
        discord_webhook: self.channel.vault.discord_webhook.clone(),
        channel_name: self.channel.inner.name.clone(),
      },
      frontend_config: FrontendConfig {
        vue_app_i18n_locale: "fr".to_string(),
        vue_app_i18n_fallback_locale: "en".to_string(),
        vue_app_uri: Url::parse(uri.as_str())?,
        vue_app_api_uri: Url::parse(format!("{}/api/v1/", uri.as_str()).as_str())?,
        vue_app_oauth_uri: Url::parse(format!("{}/oauth", uri.as_str()).as_str())?,
        vue_app_api_release_commit: self.git_ref.clone().to_string(),
        vue_app_api_release_channel: channel.name.clone(),
      },
    };

    if !SKIP_BUILD {
      changed = exec_as::<LocalLinux, _>(UserRef::Id(channel.user.uid()), None, &br)
        .await?
        .unwrap()
        .changed
        || changed;
    }

    let release = EmushRelease {
      release_dir,
      repository: self.repository.clone(),
      repo_dir,
      channel,
      git_ref: self.git_ref.clone(),
      is_after_db_reset: self.is_after_db_reset,
    };

    changed = (UpdateActiveEmushRelease {
      release: release.clone(),
    })
    .run(host)
    .await?
    .changed
      || changed;

    // eprintln!("Reapply Postgres permissions");
    // changed = PostgresDbPermissions::new(
    //   &release.channel.postgres.name,
    //   release.channel.postgres.admin.name.clone(),
    //   release.channel.postgres.main.name.clone(),
    //   release.channel.postgres.read.name.clone(),
    // )
    // .run(host)
    // .await?
    // .changed
    //   || changed;

    Ok(TaskSuccess {
      changed,
      output: release,
    })
  }
}

#[async_trait]
impl<'h, H> AsyncFn<&'h H> for EmushChannelTarget
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  H::Group: Send + Sync,
  H::User: Send + Sync + Debug,
  <H as FsHost>::Metadata: LinuxMetadata,
{
  type Output = TaskResult<EmushChannel<H::User>, anyhow::Error>;

  async fn run(&self, host: &'h H) -> Self::Output {
    eprintln!("eMush: Create channel");
    let res = self.inner.run(host).await?;

    eprintln!("Channel ready");
    let output = EmushChannel {
      name: res.output.name,
      user: res.output.user,
      domain: res.output.domain.expect("Expected associated domain name"),
      paths: res.output.paths,
      fallback_server: res.output.fallback_server.expect("Expected fallback server"),
      postgres: res.output.postgres.expect("Expected Postgres database"),
      node: res.output.node.expect("Expected Node environment"),
      php: res.output.php.expect("Expected PHP environment"),
    };

    Ok(TaskSuccess {
      changed: res.changed,
      output,
    })
  }
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct BuildEmushRelease {
  node: PathBuf,
  repo_dir: PathBuf,
  remote: String,
  git_ref: String,
  backend_config: BackendConfig,
  frontend_config: FrontendConfig,
}

#[async_trait]
impl<'h, H> AsyncFn<&'h H> for BuildEmushRelease
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Send + Sync,
{
  type Output = Result<TaskSuccess<()>, ()>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let gc = GitCheckout {
      path: self.repo_dir.clone(),
      remote: self.remote.clone(),
      r#ref: self.git_ref.clone(),
    };

    eprintln!("Checkout repository: {:?} {:?}", self.remote, self.git_ref);
    let res = gc.run(host).await.unwrap();
    let mut changed = res.changed;

    let api_dir = self.repo_dir.join("Api");
    let app_dir = self.repo_dir.join("App");
    let jwt_dir = self.repo_dir.join("Api/config/jwt");

    eprintln!("Install PHP dependencies");
    let cmd = Command::new("composer")
      .arg("install")
      .arg("--no-interaction")
      .arg("--no-ansi")
      .current_dir(api_dir.to_str().unwrap());
    host.exec(&cmd).unwrap();

    let node_dir = self.node.parent().expect("failed to retrieve node directory");
    let path_env = match std::env::var("PATH").as_deref().ok() {
      Some(p) => format!("{}:{p}", node_dir.display()),
      None => node_dir.display().to_string(),
    };

    eprintln!("Install Node dependencies");
    let cmd = Command::new("yarn")
      .arg("install")
      .env("PATH", path_env.as_str())
      .current_dir(app_dir.to_str().unwrap());
    host.exec(&cmd).unwrap();

    eprintln!("Create directory: {}", jwt_dir.display());
    changed = EnsureDir::new(jwt_dir.clone()).run(host).await.map_err(drop)?.changed || changed;

    eprintln!("Refresh JWT private key");
    let cmd = Command::new("openssl")
      .arg("genrsa")
      .arg("-out")
      .arg("private.pem")
      .current_dir(jwt_dir.to_str().unwrap());
    host.exec(&cmd).unwrap();

    eprintln!("Refresh JWT public key");
    let cmd = Command::new("openssl")
      .arg("rsa")
      .arg("-in")
      .arg("private.pem")
      .arg("-out")
      .arg("public.pem")
      .arg("-pubout")
      .current_dir(jwt_dir.to_str().unwrap());
    host.exec(&cmd).unwrap();

    eprintln!("Apply backend config");
    EnsureFile::new(api_dir.join(".env.local"))
      .content(get_backend_config(&self.backend_config))
      .mode(FileMode::OWNER_READ | FileMode::OWNER_WRITE)
      .run(host)
      .await
      .unwrap();

    eprintln!("Configure the frontend build");
    EnsureFile::new(app_dir.join(".env"))
      .content(self.frontend_config.to_string())
      .mode(FileMode::OWNER_READ | FileMode::OWNER_WRITE)
      .run(host)
      .await
      .unwrap();

    eprintln!("Build the frontend");
    let cmd = Command::new("yarn")
      .arg("run")
      .arg("build")
      .env("PATH", path_env)
      .env("NODE_ENV", "production")
      .current_dir(app_dir.to_str().unwrap());
    host.exec(&cmd).unwrap();

    Ok(TaskSuccess { changed, output: () })
  }
}

impl<'h, H> NamedTask<&'h H> for BuildEmushRelease
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Send + Sync,
{
  const NAME: TaskName = TaskName::new("emush::BuildEmushRelease");
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize)]
pub struct UpdateActiveEmushRelease<User: LinuxUser, S: AsRef<str> = String> {
  release: EmushRelease<User, S>,
}

#[async_trait]
impl<'h, H, S> AsyncFn<&'h H> for UpdateActiveEmushRelease<H::User, S>
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Send + Sync,
  S: AsRef<str> + Send + Sync,
{
  type Output = TaskResult<(), anyhow::Error>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let mut changed = false;
    let active_release_link = self.release.channel.paths.home.join("active");
    let previous_release_link = self.release.channel.paths.home.join("previous");
    let old_release_dir = {
      match host.resolve(&active_release_link) {
        Ok(old) => Some(old),
        Err(ResolveError::NotFound) => None,
        Err(e) => return Err(e.into()),
      }
    };
    let new_release_dir = host.resolve(&self.release.release_dir)?;

    if let Some(old_release_dir) = old_release_dir {
      if old_release_dir != new_release_dir {
        eprintln!("Start to disable old release");
        // There is already an active release but it does not match the target
        // release: stop the old version before proceeding to free the channel
        // resources (database, port)
        EnsureDirSymlink::new(previous_release_link.clone())
          .points_to(old_release_dir.clone())
          .owner(self.release.channel.user.uid())
          .group(self.release.channel.user.gid())
          .mode(FileMode::ALL_READ | FileMode::ALL_EXEC)
          .run(host)
          .await?;

        eprintln!("Old release disabled");
        changed = true;
      }
    }

    eprintln!("UpgradingDatabase");
    changed = exec_as::<LocalLinux, _>(
      UserRef::Id(self.release.channel.user.uid()),
      None,
      &UpgradeEmushDatabase {
        repo_dir: self.release.repo_dir.clone(),
        db_name: self.release.channel.postgres.name.clone(),
        db_admin: self.release.channel.postgres.admin.clone(),
        is_after_db_reset: self.release.is_after_db_reset,
      },
    )
    .await?
    .unwrap()
    .changed
      || changed;

    eprintln!("Generating nginx config");
    let nginx_config = get_nginx_config(&self.release.channel);
    eprintln!("Writing nginx config");
    NginxAvailableSite::new(self.release.channel.name.as_ref(), nginx_config)
      .run(host)
      .await?;
    eprintln!("Enabling nginx config");
    NginxEnableSite::new(self.release.channel.name.as_ref())
      .run(host)
      .await?;

    eprintln!("Marking release as active");
    EnsureDirSymlink::new(active_release_link.clone())
      .points_to(new_release_dir.clone())
      .owner(self.release.channel.user.uid())
      .group(self.release.channel.user.gid())
      .mode(FileMode::ALL_READ | FileMode::ALL_EXEC)
      .run(host)
      .await?;

    Ok(TaskSuccess { changed, output: () })
  }
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct UpgradeEmushDatabase {
  repo_dir: PathBuf,
  db_name: String,
  db_admin: PostgresCredentials,
  is_after_db_reset: bool,
}

#[async_trait]
impl<'h, H: ExecHost + LinuxUserHost + LinuxFsHost> AsyncFn<&'h H> for UpgradeEmushDatabase
where
  <H as FsHost>::Metadata: LinuxMetadata,
{
  type Output = Result<TaskSuccess<()>, ()>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let api_dir = self.repo_dir.join("Api");
    let console_bin = api_dir.join("bin/console");

    let admin_db_url = PostgresUrl::new(
      "localhost",
      5432,
      self.db_name.as_str(),
      self.db_admin.name.as_str(),
      self.db_admin.password.as_str(),
      Some(PG_VERSION),
    )
    .unwrap()
    .to_string();

    let cmd = Command::new(console_bin.to_str().unwrap())
      .arg("mush:migrate")
      .env("DATABASE_URL", &admin_db_url)
      .current_dir(api_dir.to_str().unwrap());
    host.exec(&cmd).unwrap();

    Ok(TaskSuccess {
      changed: true,
      output: (),
    })
  }
}

impl<'h, H: 'h + ExecHost + LinuxUserHost + LinuxFsHost> NamedTask<&'h H> for UpgradeEmushDatabase
where
  <H as FsHost>::Metadata: LinuxMetadata,
{
  const NAME: TaskName = TaskName::new("emush::UpgradeEmushDatabase");
}

pub fn get_nginx_config<U: LinuxUser, S: AsRef<str>>(channel: &EmushChannel<U, S>) -> String {
  let access_log = channel.paths.logs.join("main.nginx.access.log");
  let error_log = channel.paths.logs.join("main.nginx.error.log");
  let web_root = channel.paths.home.join("active/repo/Api/public");
  let static_root = channel.paths.home.join("active/repo/App/dist");

  let main_directives = format!(
    r#"  # Space-separated hostnames (wildcard * is accepted)
  server_name {domain};

  access_log {access_log};
  error_log {error_log};
  client_max_body_size 5M;

  root {web_root};

  location ~* ^/(?:api/v1|oauth)(?:/|$) {{
    # try to serve file directly, fallback to index.php
    try_files $uri /index.php$is_args$args;
  }}

  location ~ ^/index\.php(/|$) {{
    fastcgi_pass unix:{php_fpm_socket};
    fastcgi_split_path_info ^(.+\.php)(/.*)$;
    include fastcgi_params;

    # optionally set the value of the environment variables used in the application
    # fastcgi_param APP_ENV prod;
    # fastcgi_param APP_SECRET <app-secret-id>;
    # fastcgi_param DATABASE_URL "mysql://db_user:db_pass@host:3306/db_name";

    # When you are using symlinks to link the document root to the
    # current version of your application, you should pass the real
    # application path instead of the path to the symlink to PHP
    # FPM.
    # Otherwise, PHP's OPcache may not properly detect changes to
    # your PHP files (see https://github.com/zendtech/ZendOptimizerPlus/issues/126
    # for more information).
    fastcgi_param SCRIPT_FILENAME $realpath_root$fastcgi_script_name;
    fastcgi_param DOCUMENT_ROOT $realpath_root;
    # Prevents URIs that include the front controller. This will 404:
    # http://domain.tld/index.php/some-path
    # Remove the internal directive to allow URIs like this
    internal;
  }}

  # return 404 for all other php files not matching the front controller
  # this prevents access to other php files you don't want to be accessible.
  location ~ \.php$ {{
    return 404;
  }}

  location / {{
    root {static_root};
    index index.html;
    try_files $uri /index.html;
  }}
"#,
    domain = channel.domain.main.as_ref(),
    access_log = access_log.to_str().unwrap(),
    error_log = error_log.to_str().unwrap(),
    web_root = web_root.to_str().unwrap(),
    static_root = static_root.to_str().unwrap(),
    php_fpm_socket = channel.php.socket.to_str().unwrap(),
  );

  match channel.domain.cert.as_ref() {
    Some(https) => format!(
      r#"# HTTPS
server {{
  # HTTPS port
  listen 443 ssl;
  listen [::]:443 ssl;

  ssl_certificate {cert};
  ssl_certificate_key {cert_key};

  # # HSTS (ngx_http_headers_module is required) (63072000 seconds)
  # add_header Strict-Transport-Security "max-age=63072000" always;

  {main_directives}
}}

# HTTP
server {{
  # HTTP port
  listen 80;
  listen [::]:80;
  server_name {domain};
  return 301 https://$host$request_uri;
}}

# www.
server {{
  listen 80;
  listen 443 ssl;
  listen [::]:80;
  listen [::]:443 ssl;

  server_name www.{domain};

  ssl_certificate {cert};
  ssl_certificate_key {cert_key};

  return 307 https://{domain}$request_uri;
}}

"#,
      main_directives = main_directives,
      domain = channel.domain.main.as_ref(),
      cert = https.fullchain_cert_path.to_str().unwrap(),
      cert_key = https.cert_key_path.to_str().unwrap(),
    ),
    None => format!(
      r#"# HTTP
server {{
  # HTTP port
  listen 80;
  listen [::]:80;

  {main_directives}
}}

# www.
server {{
  listen 80;
  listen [::]:80;

  server_name www.{domain};

  return 307 http://{domain}$request_uri;
}}

"#,
      main_directives = main_directives,
      domain = channel.domain.main.as_ref(),
    ),
  }
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
struct BackendConfig<S: AsRef<str> = String> {
  app_secret: S,
  db_credentials: PostgresCredentials,
  db_name: S,
  db_version: u8,
  access_passphrase: S,
  identity_server: Url,
  oauth_callback: Url,
  oauth_client_id: S,
  oauth_client_secret: S,
  admin_user_id: Option<S>,
  discord_webhook: Option<S>,
  channel_name: S,
}

fn get_backend_config(config: &BackendConfig) -> String {
  format!(
    r##"# In all environments, the following files are loaded if they exist,
# the latter taking precedence over the former:
#
#  * .env                contains default values for the environment variables needed by the app
#  * .env.local          uncommitted file with local overrides
#  * .env.$APP_ENV       committed environment-specific defaults
#  * .env.$APP_ENV.local uncommitted environment-specific overrides
#
# Real environment variables win over .env files.
#
# DO NOT DEFINE PRODUCTION SECRETS IN THIS FILE NOR IN ANY OTHER COMMITTED FILES.
#
# Run "composer dump-env prod" to compile .env files for production use (requires symfony/flex >=1.2).
# https://symfony.com/doc/current/best_practices.html#use-environment-variables-for-infrastructure-configuration

###> symfony/framework-bundle ###
APP_ENV=prod
APP_SECRET={app_secret}
#TRUSTED_PROXIES=127.0.0.0/8,10.0.0.0/8,172.16.0.0/12,192.168.0.0/16
#TRUSTED_HOSTS='^(localhost|example\.com)$'
###< symfony/framework-bundle ###

###> doctrine/doctrine-bundle ###
# Format described at https://www.doctrine-project.org/projects/doctrine-dbal/en/latest/reference/configuration.html#connecting-using-a-url
# For an SQLite database, use: "sqlite:///%kernel.project_dir%/var/data.db"
# For a PostgreSQL database, use: "postgresql://db_user:db_password@127.0.0.1:5432/db_name?serverVersion=11&charset=utf8"
# IMPORTANT: You MUST configure your server version, either here or in config/packages/doctrine.yaml
DATABASE_URL={db_url}
###< doctrine/doctrine-bundle ###

###> lexik/jwt-authentication-bundle ###
JWT_SECRET_KEY=%kernel.project_dir%/config/jwt/private.pem
JWT_PUBLIC_KEY=%kernel.project_dir%/config/jwt/public.pem
JWT_PASSPHRASE=
###< lexik/jwt-authentication-bundle ###

###> nelmio/cors-bundle ###
CORS_ALLOW_ORIGIN=^https?://([a-z]\.)*localhost(:[0-9]+)?$
###< nelmio/cors-bundle ###

IDENTITY_SERVER_URI="{identity_server}"
OAUTH_CALLBACK="{oauth_callback}"
OAUTH_AUTHORIZATION_URI="{oauth_authorization_uri}"
OAUTH_TOKEN_URI="{oauth_token_uri}"
OAUTH_CLIENT_ID="{oauth_client_id}"
OAUTH_SECRET_ID="{oauth_client_secret}"

ALPHA_PASSPHRASE="{access_passphrase}"
ADMIN="{admin_user_id}"

LOG_DISCORD_WEBHOOK_URL={discord_webhook}
LOG_DISCORD_LOG_LEVEL=400
LOG_DISCORD_ENVIRONMENT_NAME={channel_name}
"##,
    app_secret = config.app_secret,
    db_url = PostgresUrl::new(
      "localhost",
      5432,
      config.db_name.as_str(),
      config.db_credentials.name.as_str(),
      config.db_credentials.password.as_str(),
      Some(config.db_version)
    )
    .unwrap(),
    access_passphrase = config.access_passphrase,
    identity_server = config.identity_server,
    oauth_callback = config.oauth_callback,
    oauth_authorization_uri = {
      let mut u = config.identity_server.clone();
      {
        let mut s = u.path_segments_mut().unwrap();
        s.push("oauth");
        s.push("authorize");
      }
      u
    },
    oauth_token_uri = {
      let mut u = config.identity_server.clone();
      {
        let mut s = u.path_segments_mut().unwrap();
        s.push("oauth");
        s.push("token");
      }
      u
    },
    oauth_client_id = config.oauth_client_id,
    oauth_client_secret = config.oauth_client_secret,
    admin_user_id = config.admin_user_id.as_deref().unwrap_or(""),
    discord_webhook = config.discord_webhook.as_deref().unwrap_or(""),
    channel_name = config.channel_name,
  )
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
struct FrontendConfig<S: AsRef<str> = String> {
  vue_app_i18n_locale: S,
  vue_app_i18n_fallback_locale: S,
  vue_app_uri: Url,
  vue_app_api_uri: Url,
  vue_app_oauth_uri: Url,
  vue_app_api_release_commit: S,
  vue_app_api_release_channel: S,
}

impl<S: AsRef<str>> fmt::Display for FrontendConfig<S> {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    write!(
      f,
      r#"VUE_APP_I18N_LOCALE={i18n_locale}
VUE_APP_I18N_FALLBACK_LOCALE={i18n_fallback_locale}
VUE_APP_URL={url}
VUE_APP_API_URL={api_url}
VUE_APP_OAUTH_URL={oauth_url}
VUE_APP_API_RELEASE_COMMIT={vue_app_api_release_commit}
VUE_APP_API_RELEASE_CHANNEL={vue_app_api_release_channel}
"#,
      i18n_locale = self.vue_app_i18n_locale.as_ref(),
      i18n_fallback_locale = self.vue_app_i18n_fallback_locale.as_ref(),
      url = self.vue_app_uri,
      api_url = self.vue_app_api_uri,
      oauth_url = self.vue_app_oauth_uri,
      vue_app_api_release_commit = self.vue_app_api_release_commit.as_ref(),
      vue_app_api_release_channel = self.vue_app_api_release_channel.as_ref(),
    )?;
    Ok(())
  }
}
