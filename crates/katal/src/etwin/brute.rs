use crate::discord_webhook::DiscordWebhook;
use crate::etwin::channel::{
  ChannelBorgbase, ChannelDomain, ChannelFallbackServer, ChannelPaths, ChannelPostgres, ChannelTarget,
};
use crate::pm2::{Pm2App, Pm2Ecosystem};
use crate::task::fs::{EnsureDir, EnsureDirSymlink, EnsureFile};
use crate::task::git::GitCheckout;
use crate::task::nginx::{NginxAvailableSite, NginxEnableSite};
use crate::task::postgres::PostgresUrl;
use crate::task::systemd::{SystemdState, SystemdUnit};
use crate::task::{exec_as, AsyncFn, NamedTask, TaskName, TaskResult, TaskSuccess};
use async_trait::async_trait;
use asys::common::fs::{FsHost, ReadFileError};
use asys::linux::fs::{FileMode, LinuxFsHost, LinuxMetadata, ResolveError};
use asys::linux::user::{Gid, GroupRef, LinuxUser, LinuxUserHost, Uid, UserRef};
use asys::local_linux::LocalLinux;
use asys::{Command, ExecHost};
use serde::{Deserialize, Serialize};
use std::fmt;
use std::fmt::Debug;
use std::path::PathBuf;
use url::Url;

const SKIP_CHECKOUT: bool = false;

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct BruteAppTarget<S: AsRef<str> = String> {
  pub name: S,
  pub user_name: S,
  pub group_name: S,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct BruteApp<S: AsRef<str> = String> {
  name: S,
  user_name: S,
  group_name: S,
  uid: Uid,
  gid: Gid,
  home: PathBuf,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct BruteChannelTarget<S: AsRef<str> = String> {
  pub inner: ChannelTarget<S>,
  pub vault: BruteVault,
  pub etwin_uri: Url,
  pub etwin_client_id: String,
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize, Serialize)]
pub struct BruteVault {
  pub etwin_client_secret: String,
  pub discord_webhook: Option<DiscordWebhook>,
  pub logs_webhook: Option<DiscordWebhook>,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct BruteChannel<User: LinuxUser, S: AsRef<str> = String> {
  name: S,
  user: User,
  domain: ChannelDomain<S>,
  paths: ChannelPaths,
  main_port: u16,
  fallback_server: ChannelFallbackServer,
  postgres: ChannelPostgres,
  node: PathBuf,
  vault: BruteVault,
  borg: Option<ChannelBorgbase>,
  etwin_uri: Url,
  etwin_client_id: String,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct BruteReleaseTarget<S: AsRef<str> = String> {
  pub channel: BruteChannelTarget,
  pub repository: S,
  pub git_ref: String,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct BruteRelease<User: LinuxUser, S: AsRef<str> = String> {
  release_dir: PathBuf,
  repository: S,
  repo_dir: PathBuf,
  channel: BruteChannel<User, S>,
  git_ref: String,
}

#[derive(Serialize, Deserialize)]
pub struct DeployBrute<S: AsRef<str> = String> {
  pub pass: S,
  pub target: BruteReleaseTarget,
}

#[async_trait]
impl<'h, H, S> AsyncFn<&'h H> for DeployBrute<S>
where
  H: ExecHost,
  S: AsRef<str> + Send + Sync,
{
  type Output = TaskResult<(), String>;

  async fn run(&self, _host: &'h H) -> Self::Output {
    let input = DeployBruteAsRoot {
      target: self.target.clone(),
    };
    exec_as::<LocalLinux, DeployBruteAsRoot>(UserRef::ROOT, Some(self.pass.as_ref()), &input)
      .await
      .map_err(|e| e.to_string())?
  }
}

impl<'h, H: 'h + ExecHost> NamedTask<&'h H> for DeployBrute {
  const NAME: TaskName = TaskName::new("brute::DeployBrute");
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct DeployBruteAsRoot {
  target: BruteReleaseTarget,
}

#[async_trait]
impl<'h, H> AsyncFn<&'h H> for DeployBruteAsRoot
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  H::User: Debug + Clone + Send + Sync,
  H::Group: Debug + Send + Sync,
  <H as FsHost>::Metadata: LinuxMetadata,
{
  type Output = TaskResult<(), String>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let res = self.target.run(host).await;
    if let Err(e) = res {
      panic!("{:?}", e);
    }
    let res = res.unwrap();

    Ok(TaskSuccess {
      changed: res.changed,
      output: (),
    })
  }
}

impl<'h, H> NamedTask<&'h H> for DeployBruteAsRoot
where
  H: 'h + ExecHost + LinuxUserHost + LinuxFsHost,
  H::User: Debug + Clone + Send + Sync,
  H::Group: Debug + Send + Sync,
  <H as FsHost>::Metadata: LinuxMetadata,
{
  const NAME: TaskName = TaskName::new("brute::DeployBruteAsRoot");
}

#[async_trait]
impl<'h, H> AsyncFn<&'h H> for BruteReleaseTarget
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  H::User: Debug + Clone + Send + Sync,
  H::Group: Debug + Send + Sync,
  <H as FsHost>::Metadata: LinuxMetadata,
{
  type Output = TaskResult<BruteRelease<H::User>, anyhow::Error>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let res = self.channel.run(host).await?;
    let channel: BruteChannel<H::User> = res.output;
    let mut changed = res.changed;

    let release_dir = channel.paths.releases.join(&self.git_ref);

    eprintln!("Create directory: {}", release_dir.display());
    changed = EnsureDir::new(release_dir.clone())
      .owner(UserRef::Id(channel.user.uid()))
      .group(GroupRef::Id(channel.user.gid()))
      .mode(FileMode::OWNER_ALL | FileMode::ALL_READ | FileMode::ALL_EXEC)
      .run(host)
      .await?
      .changed
      || changed;

    let witness = release_dir.join("ok");

    let witness_exists = match host.read_file(witness.as_path()) {
      Ok(_) => true,
      Err(ReadFileError::NotFound) => false,
      Err(e) => {
        panic!("failed to check witness file: {e:?}")
      }
    };

    let repo_dir = release_dir.join("repo");

    let release = BruteRelease {
      release_dir,
      repository: self.repository.clone(),
      repo_dir: repo_dir.clone(),
      channel: channel.clone(),
      git_ref: self.git_ref.clone(),
    };

    if witness_exists {
      eprintln!(
        "bailing-out, commit already downloaded (assuming the release already completed) (todo: better tracking)"
      );
      return Ok(TaskSuccess {
        changed,
        output: release,
      });
    }

    let br = BuildBruteRelease {
      node: channel.node.clone(),
      repo_dir: repo_dir.clone(),
      remote: self.repository.clone(),
      git_ref: self.git_ref.clone(),
      backend_config: config::BackendConfig::from_channel(&channel, self.git_ref.clone()),
    };

    changed = exec_as::<LocalLinux, _>(UserRef::Id(channel.user.uid()), None, &br)
      .await?
      .unwrap()
      .changed
      || changed;

    changed = (UpdateActiveBruteRelease {
      release: release.clone(),
    })
    .run(host)
    .await?
    .changed
      || changed;

    changed = EnsureFile::new(witness).content([]).run(host).await.unwrap().changed || changed;

    Ok(TaskSuccess {
      changed,
      output: release,
    })
  }
}

#[async_trait]
impl<'h, H> AsyncFn<&'h H> for BruteChannelTarget
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  H::Group: Send + Sync,
  H::User: Send + Sync + Debug + Clone,
  <H as FsHost>::Metadata: LinuxMetadata,
{
  type Output = TaskResult<BruteChannel<H::User>, anyhow::Error>;

  async fn run(&self, host: &'h H) -> Self::Output {
    eprintln!("Brute: Create channel");
    let res = self.inner.run(host).await?;

    eprintln!("Channel ready");
    let output = BruteChannel {
      name: res.output.name,
      user: res.output.user,
      domain: res.output.domain.expect("Expected associated domain name"),
      paths: res.output.paths,
      main_port: res.output.main_port.expect("Expected main port"),
      fallback_server: res.output.fallback_server.expect("Expected fallback server"),
      postgres: res.output.postgres.expect("Expected Postgres database"),
      node: res.output.node.expect("Expected Node environment"),
      vault: self.vault.clone(),
      borg: res.output.borg,
      etwin_uri: self.etwin_uri.clone(),
      etwin_client_id: self.etwin_client_id.clone(),
    };

    Ok(TaskSuccess {
      changed: res.changed,
      output,
    })
  }
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct BuildBruteRelease {
  node: PathBuf,
  repo_dir: PathBuf,
  remote: String,
  git_ref: String,
  backend_config: config::BackendConfig,
}

#[async_trait]
impl<'h, H> AsyncFn<&'h H> for BuildBruteRelease
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  H::User: Send + Sync,
  <H as FsHost>::Metadata: LinuxMetadata,
{
  type Output = Result<TaskSuccess<()>, ()>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let mut changed: bool;
    if !SKIP_CHECKOUT {
      let gc = GitCheckout {
        path: self.repo_dir.clone(),
        remote: self.remote.clone(),
        r#ref: self.git_ref.clone(),
      };

      eprintln!("Checkout repository: {:?} {:?}", self.remote, self.git_ref);
      let res = gc.run(host).await.unwrap();
      changed = res.changed;
    } else {
      changed = false;
    }

    let backend_dir = self.repo_dir.join("server");

    let node_dir = self.node.parent().expect("failed to retrieve node directory");
    let path_env = match std::env::var("PATH").as_deref().ok() {
      Some(p) => format!("{}:{p}", node_dir.display()),
      None => node_dir.display().to_string(),
    };

    eprintln!("Install Node dependencies");
    let cmd = Command::new("yarn")
      .arg("install")
      .arg("--immutable")
      .env("PATH", path_env.as_str())
      .env("NODE_ENV", "production")
      .current_dir(self.repo_dir.to_str().unwrap());
    host.exec(&cmd).unwrap();

    eprintln!("Write backend config (root)");
    changed = EnsureFile::new(self.repo_dir.join(".env"))
      .content(self.backend_config.to_string())
      // .owner(app.uid)
      // .group(app.gid)
      .mode(FileMode::OWNER_READ | FileMode::OWNER_WRITE)
      .run(host)
      .await
      .unwrap()
      .changed
      || changed;

    eprintln!("Write backend config (server)");
    changed = EnsureFile::new(backend_dir.join(".env"))
      .content(self.backend_config.to_string())
      // .owner(app.uid)
      // .group(app.gid)
      .mode(FileMode::OWNER_READ | FileMode::OWNER_WRITE)
      .run(host)
      .await
      .unwrap()
      .changed
      || changed;

    eprintln!("Build the project");
    let cmd = Command::new("yarn")
      .arg("run")
      .arg("build")
      .env("PATH", path_env)
      .env("NODE_ENV", "production")
      .current_dir(self.repo_dir.to_str().unwrap());
    host.exec(&cmd).unwrap();

    Ok(TaskSuccess { changed, output: () })
  }
}

impl<'h, H> NamedTask<&'h H> for BuildBruteRelease
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  H::User: Send + Sync,
  <H as FsHost>::Metadata: LinuxMetadata,
{
  const NAME: TaskName = TaskName::new("brute::BuildBruteRelease");
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize)]
pub struct UpdateActiveBruteRelease<User: LinuxUser, S: AsRef<str> = String> {
  release: BruteRelease<User, S>,
}

#[async_trait]
impl<'h, H, S> AsyncFn<&'h H> for UpdateActiveBruteRelease<H::User, S>
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Send + Sync,
  S: AsRef<str> + Send + Sync,
{
  type Output = TaskResult<(), anyhow::Error>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let uid = self.release.channel.user.uid();
    let gid = self.release.channel.user.gid();

    let service_name = format!("{}.service", self.release.channel.name.as_ref());

    let mut changed = false;
    let active_release_link = &self.release.channel.paths.active_release_link;
    let previous_release_link = &self.release.channel.paths.previous_release_link;
    let old_release_dir = {
      match host.resolve(active_release_link) {
        Ok(old) => Some(old),
        Err(ResolveError::NotFound) => None,
        Err(e) => return Err(e.into()),
      }
    };
    let new_release_dir = host.resolve(&self.release.release_dir)?;

    if let Some(old_release_dir) = old_release_dir {
      if old_release_dir != new_release_dir {
        eprintln!("Start to disable old release");
        // There is already an active release but it does not match the target
        // release: stop the old version before proceeding to free the channel
        // resources (database, port)
        EnsureDirSymlink::new(previous_release_link.clone())
          .points_to(old_release_dir.clone())
          .owner(uid)
          .group(gid)
          .mode(FileMode::ALL_READ | FileMode::ALL_EXEC)
          .run(host)
          .await?;

        SystemdUnit::new(&service_name)
          .state(SystemdState::Inactive)
          .run(host)
          .await?;
        eprintln!("Old release disabled");

        // if let Some(borg) = self.release.channel.borg.as_ref() {
        //   eprintln!("Creating archive before release");
        //   let backup_dir = self.release.channel.paths.home.join("backup");
        //   EnsureDir::new(backup_dir.clone()).run(host).await?;
        //   let example_file = backup_dir.join("etwin.pgdump");
        //   EnsureFile::new(example_file.clone()).content("Hey").run(host).await?;
        //
        //   changed = exec_as::<LocalLinux, _>(
        //     UserRef::Id(uid),
        //     None,
        //     &CreateBorgArchive::new_from_time(
        //       borg.password.clone(),
        //       borg.ssh_priv_key.clone(),
        //       borg.repo.clone(),
        //       backup_dir,
        //     ),
        //   )
        //   .await?
        //   .unwrap()
        //   .changed
        //     || changed;
        // } else {
        //   eprintln!("Skipping pre-release archive: no borg config");
        // }

        changed = true;
      }
    }

    // TODO: Trigger backup

    eprintln!("UpgradingDatabase");
    changed = exec_as::<LocalLinux, _>(
      UserRef::Id(uid),
      None,
      &UpgradeBruteDatabase {
        node: self.release.channel.node.clone(),
        repo_dir: self.release.repo_dir.clone(),
        postgres: self.release.channel.postgres.clone(),
      },
    )
    .await?
    .unwrap()
    .changed
      || changed;

    if let Some(_borg) = self.release.channel.borg.as_ref() {
      eprintln!("WARNING: backups are not supported for Brute");
    }

    eprintln!("Generating PM2 ecosystem");
    let ecosystem = Pm2Ecosystem {
      apps: vec![{
        Pm2App::new(
          self.release.channel.name.as_ref().to_string(),
          self.release.repo_dir.join("server/lib/server.js"),
        )
        .env("NODE_ENV", "production")
        .cwd(self.release.repo_dir.join("server"))
        .interpreter(self.release.channel.node.to_str().expect("invalid node path"))
      }],
    };

    let mut ecosystem = serde_json::to_string_pretty(&ecosystem).unwrap();
    ecosystem.push('\n');

    eprintln!("Writing PM2 ecosystem");
    changed = EnsureFile::new(new_release_dir.join("ecosystem.config.json"))
      .content(ecosystem)
      .owner(uid)
      .group(gid)
      .mode(FileMode::ALL_READ)
      .run(host)
      .await?
      .changed
      || changed;

    eprintln!("Generating Systemd unit");
    let service = BruteService::from_channel(&self.release.channel);
    eprintln!("Writing Systemd unit");
    changed = EnsureFile::new(PathBuf::from("/etc/systemd/system").join(&service_name))
      .content(service.to_string())
      .owner(Uid::ROOT)
      .mode(FileMode::ALL_READ)
      .run(host)
      .await?
      .changed
      || changed;

    eprintln!("Generating nginx config");
    let nginx_config = get_nginx_config(&self.release.channel);

    eprintln!("Marking release as active");
    EnsureDirSymlink::new(active_release_link.clone())
      .points_to(new_release_dir.clone())
      .owner(uid)
      .group(gid)
      .mode(FileMode::ALL_READ | FileMode::ALL_EXEC)
      .run(host)
      .await?;

    eprintln!("Starting service");
    changed = SystemdUnit::new(&service_name)
      .enabled(true)
      .state(SystemdState::Active)
      .run(host)
      .await?
      .changed
      || changed;

    eprintln!("Writing nginx config");
    NginxAvailableSite::new(self.release.channel.name.as_ref(), nginx_config)
      .run(host)
      .await?;

    eprintln!("Enabling nginx config");
    NginxEnableSite::new(self.release.channel.name.as_ref())
      .run(host)
      .await?;

    Ok(TaskSuccess { changed, output: () })
  }
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct UpgradeBruteDatabase {
  node: PathBuf,
  repo_dir: PathBuf,
  postgres: ChannelPostgres,
}

#[async_trait]
impl<'h, H> AsyncFn<&'h H> for UpgradeBruteDatabase
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
{
  type Output = Result<TaskSuccess<()>, ()>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let changed = false;

    let node_dir = self.node.parent().expect("failed to retrieve node directory");
    let path_env = match std::env::var("PATH").as_deref().ok() {
      Some(p) => format!("{}:{p}", node_dir.display()),
      None => node_dir.display().to_string(),
    };

    let database_url = PostgresUrl::new(
      &self.postgres.host,
      self.postgres.port,
      &self.postgres.name,
      &self.postgres.admin.name,
      &self.postgres.admin.password,
      None,
    )
    .expect("failed to build admin Postgres URL");

    let cmd = Command::new("yarn")
      .arg("run")
      .arg("db:sync:prod")
      .env("PATH", path_env.as_str())
      .env("NODE_ENV", "production")
      .env("DATABASE_URL", database_url.to_string())
      .current_dir(self.repo_dir.to_str().unwrap());
    host.exec(&cmd).unwrap();

    Ok(TaskSuccess { changed, output: () })
  }
}

impl<'h, H> NamedTask<&'h H> for UpgradeBruteDatabase
where
  H: 'h + ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
{
  const NAME: TaskName = TaskName::new("brute::UpgradeBruteDatabase");
}

mod config {
  use crate::discord_webhook::DiscordWebhook;
  use crate::etwin::brute::BruteChannel;
  use crate::task::postgres::PostgresUrl;
  use asys::linux::user::LinuxUser;
  use serde::{Deserialize, Serialize};
  use std::fmt;
  use url::Url;

  #[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
  pub struct BackendConfig<S: AsRef<str> = String> {
    // Database
    pub database_url: PostgresUrl,
    // Server
    pub self_url: S,
    pub port: u16,
    // Etwin
    pub etwin_url: Url,
    pub etwin_client_id: S,
    pub etwin_client_secret: S,
    pub release_commit: S,
    pub release_channel: S,
    pub discord_webhook: Option<DiscordWebhook<S>>,
    pub logs_webhook: Option<DiscordWebhook<S>>,
  }

  impl BackendConfig {
    pub fn from_channel(channel: &BruteChannel<impl LinuxUser>, git_ref: String) -> Self {
      let protocol = if channel.domain.cert.is_some() { "https" } else { "http" };
      let self_url = format!("{}://{}", protocol, channel.domain.main.as_str());
      Self {
        database_url: PostgresUrl::new(
          channel.postgres.host.as_str(),
          channel.postgres.port,
          channel.postgres.name.as_str(),
          channel.postgres.main.name.as_str(),
          channel.postgres.main.password.as_str(),
          None,
        )
        .expect("failed to build postgres URL"),
        self_url,
        port: channel.main_port,
        etwin_url: channel.etwin_uri.clone(),
        etwin_client_id: channel.etwin_client_id.clone(),
        etwin_client_secret: channel.vault.etwin_client_secret.clone(),
        release_commit: git_ref,
        release_channel: channel.name.clone(),
        discord_webhook: channel.vault.discord_webhook.clone(),
        logs_webhook: channel.vault.logs_webhook.clone(),
      }
    }
  }

  impl<S: AsRef<str>> fmt::Display for BackendConfig<S> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
      writeln!(f, "DATABASE_URL={}", &self.database_url)?;
      writeln!(f, "SELF_URL={}", self.self_url.as_ref())?;
      writeln!(f, "PORT={}", self.port)?;
      writeln!(f, "ETWIN_URL={}", self.etwin_url.as_str())?;
      writeln!(f, "ETWIN_CLIENT_ID={}", self.etwin_client_id.as_ref())?;
      writeln!(f, "ETWIN_CLIENT_SECRET={}", self.etwin_client_secret.as_ref())?;
      writeln!(f, "RELEASE_COMMIT={}", self.release_commit.as_ref())?;
      writeln!(f, "RELEASE_CHANNEL={}", self.release_channel.as_ref())?;
      if let Some(discord_webhook) = self.discord_webhook.as_ref() {
        writeln!(f, "DISCORD_WEBHOOK_ID={}", discord_webhook.id.as_ref())?;
        writeln!(f, "DISCORD_WEBHOOK_TOKEN={}", discord_webhook.token.as_ref())?;
      }
      if let Some(logs_webhook) = self.logs_webhook.as_ref() {
        writeln!(f, "DISCORD_LOGS_WEBHOOK_ID={}", logs_webhook.id.as_ref())?;
        writeln!(f, "DISCORD_LOGS_WEBHOOK_TOKEN={}", logs_webhook.token.as_ref())?;
      }
      Ok(())
    }
  }
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
struct BruteService<S: AsRef<str> = String> {
  channel_name: S,
  user: S,
  pm2_dir: PathBuf,
  pid_file: PathBuf,
  ecosystem_file: PathBuf,
  working_dir: PathBuf,
}

impl BruteService<String> {
  pub fn from_channel<User, S>(channel: &BruteChannel<User, S>) -> Self
  where
    User: LinuxUser,
    S: AsRef<str>,
  {
    Self {
      channel_name: channel.name.as_ref().to_string(),
      user: channel.user.name().to_string(),
      pid_file: channel.paths.home.join(".pm2/pm2.pid"),
      pm2_dir: channel.paths.home.join(".pm2"),
      ecosystem_file: channel.paths.home.join("active/ecosystem.config.json"),
      working_dir: channel.paths.home.join("active/repo"),
    }
  }
}

impl<S: AsRef<str>> fmt::Display for BruteService<S> {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    write!(
      f,
      r#"[Unit]
Description=PM2 for Brute ({channel})
Documentation=https://pm2.keymetrics.io/
After=network.target
Requires=postgresql.service

[Service]
Type=forking
User={user}
LimitNOFILE=infinity
LimitNPROC=infinity
LimitCORE=infinity
Environment=PATH=/usr/local/sbin:/usr/local/bin:/usr/bin:/usr/bin/site_perl:/usr/bin/vendor_perl:/usr/bin/core_perl:/usr/bin:/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin
Environment=PM2_HOME={pm2_dir}
PIDFile={pid_file}
Restart=on-failure

WorkingDirectory={working_dir}
ExecStart=/usr/bin/pm2 start {ecosystem}
ExecReload=/usr/bin/pm2 reload {ecosystem}
ExecStop=/usr/bin/pm2 stop {ecosystem}

[Install]
WantedBy=multi-user.target
"#,
      channel = self.channel_name.as_ref(),
      user = self.user.as_ref(),
      pm2_dir = self.pm2_dir.display(),
      pid_file = self.pid_file.display(),
      ecosystem = self.ecosystem_file.display(),
      working_dir = self.working_dir.display(),
    )
  }
}

pub fn get_nginx_config<S: AsRef<str>>(channel: &BruteChannel<impl LinuxUser, S>) -> String {
  let upstream_name = channel.name.as_ref().to_string().replace('.', "_");
  let access_log = channel.paths.logs.join("main.nginx.access.log");
  let error_log = channel.paths.logs.join("main.nginx.error.log");
  let static_dir = channel.paths.home.join("active/repo/client/build");

  let fallback_server = format!(
    r#"# Fallback server
server {{
  # HTTP port
  listen {fallback_port};
  listen [::]:{fallback_port};

  # Hide nginx version in `Server` header.
  server_tokens off;
  add_header Referrer-Policy same-origin;
  add_header X-Content-Type-Options nosniff;
  add_header X-XSS-Protection "1; mode=block";

  index index.html;

  root {fallback_dir};
}}
"#,
    fallback_port = channel.fallback_server.port,
    fallback_dir = channel.fallback_server.dir.to_str().unwrap(),
  );

  let upstream_server = format!(
    r#"# Define target of the reverse-proxy
upstream {upstream_name} {{
  server [::1]:{port};
  server [::1]:{fallback_port} backup;
  keepalive 8;
}}
"#,
    upstream_name = upstream_name,
    port = channel.main_port,
    fallback_port = channel.fallback_server.port,
  );

  let main_directives = format!(
    r#"  # Space-separated hostnames (wildcard * is accepted)
  server_name {domain};

  access_log {access_log};
  error_log {error_log};
  client_max_body_size 5M;

  root {static_dir};

  location ~* ^/(?:api)(?:/|$) {{
    # Upstream reference
    proxy_pass http://{upstream_name};

    # Configuration of the proxy
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection 'upgrade';
    proxy_set_header Host $host;
    proxy_set_header X-NginX-Proxy true;
    proxy_cache_bypass $http_upgrade;
    proxy_redirect off;
  }}

  location / {{
    try_files $uri /index.html;
    gzip_static on;
  }}
"#,
    domain = channel.domain.main.as_ref(),
    access_log = access_log.to_str().unwrap(),
    error_log = error_log.to_str().unwrap(),
    static_dir = static_dir.to_str().unwrap(),
    upstream_name = upstream_name,
  );

  match channel.domain.cert.as_ref() {
    Some(https) => format!(
      r#"# This configuration sets a reverse proxy for a specific domain

{fallback_server}

{upstream_server}

# HTTPS
server {{
  # HTTPS port
  listen 443 ssl;
  listen [::]:443 ssl;

  ssl_certificate {cert};
  ssl_certificate_key {cert_key};

  # # HSTS (ngx_http_headers_module is required) (63072000 seconds)
  # add_header Strict-Transport-Security "max-age=63072000" always;

  {main_directives}
}}

# HTTP
server {{
  # HTTP port
  listen 80;
  listen [::]:80;
  server_name {domain};
  return 301 https://$host$request_uri;
}}

# www.
server {{
  listen 80;
  listen 443 ssl;
  listen [::]:80;
  listen [::]:443 ssl;

  server_name www.{domain};

  ssl_certificate {cert};
  ssl_certificate_key {cert_key};

  return 307 https://{domain}$request_uri;
}}

"#,
      fallback_server = fallback_server,
      upstream_server = upstream_server,
      main_directives = main_directives,
      domain = channel.domain.main.as_ref(),
      cert = https.fullchain_cert_path.to_str().unwrap(),
      cert_key = https.cert_key_path.to_str().unwrap(),
    ),
    None => format!(
      r#"# This configuration sets a reverse proxy for a specific domain

{fallback_server}

{upstream_server}

# HTTP
server {{
  # HTTP port
  listen 80;
  listen [::]:80;

  {main_directives}
}}

# www.
server {{
  listen 80;
  listen [::]:80;

  server_name www.{domain};

  return 307 http://{domain}$request_uri;
}}

"#,
      fallback_server = fallback_server,
      upstream_server = upstream_server,
      main_directives = main_directives,
      domain = channel.domain.main.as_ref(),
    ),
  }
}
