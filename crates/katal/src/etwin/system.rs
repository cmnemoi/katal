use crate::config::{
  ChannelConfig, RawAppConfig, RawAutoChannelConfig, RawAutoVault, RawBruteChannelConfig, RawBruteVault, RawConfig,
  RawDinorpgChannelConfig, RawDinorpgVault, RawEmushChannelConfig, RawEmushVault, RawEpopotamoChannelConfig,
  RawEpopotamoVault, RawFrutibandasChannelConfig, RawKingdomChannelConfig, RawKingdomVault, RawNeoparcChannelConfig,
  RawNeoparcVault, RawStateConfig, RawVaultConfig, StateAction,
};
use crate::discord_webhook::DiscordWebhook;
use crate::etwin::auto::{AutoChannelTarget, AutoReleaseTarget, AutoVault, DeployAuto};
use crate::etwin::brute::{BruteChannelTarget, BruteReleaseTarget, BruteVault, DeployBrute};
use crate::etwin::channel::{BorgTarget, CertTarget, ChannelDomainTarget, ChannelPostgresTarget, ChannelTarget};
use crate::etwin::dinorpg::{DeployDinorpg, DinorpgChannelTarget, DinorpgReleaseTarget, DinorpgVault};
use crate::etwin::emush::{DeployEmush, EmushChannelTarget, EmushReleaseTarget, EmushVault};
use crate::etwin::epopotamo::{DeployEpopotamo, EpopotamoChannelTarget, EpopotamoReleaseTarget, EpopotamoVault};
use crate::etwin::frutibandas::{DeployFrutibandas, FrutibandasChannelTarget, FrutibandasReleaseTarget};
use crate::etwin::kingdom::{DeployKingdom, KingdomChannelTarget, KingdomReleaseTarget, KingdomVault};
use crate::etwin::neoparc::{DeployNeoparc, NeoparcChannelTarget, NeoparcReleaseTarget, NeoparcVault};
use crate::job_runner::opaque_data::BincodeBuf;
use crate::task::fused::{AcquireIdempotencyToken, StateAndHost, StatefulHost};
use crate::task::pacman::PacmanSync;
use crate::task::php::PhpAvailable;
use crate::task::{exec_as_rec, AsyncFn, NamedTask, TaskName, TaskResult, TaskRunnerRegistry, TaskSuccess};
use async_trait::async_trait;
use asys::common::fs::FsHost;
use asys::linux::fs::{LinuxFsHost, LinuxMetadata};
use asys::linux::user::{LinuxUserHost, UserRef};
use asys::local_linux::LocalLinux;
use asys::ExecHost;
use katal_state::IdempotencyKey;
use serde::{Deserialize, Serialize};
use std::fmt::Debug;
use std::time::Duration;
use thiserror::Error;
use url::Url;

#[derive(Serialize, Deserialize)]
pub struct SyncSystem {
  pub config: RawConfig,
}

#[async_trait]
impl<'h, H: StatefulHost + ExecHost + Send + Sync> AsyncFn<&'h H> for SyncSystem {
  type Output = TaskResult<(), String>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let registry: TaskRunnerRegistry<'h, BincodeBuf, &'h H, BincodeBuf> = {
      let mut r = TaskRunnerRegistry::new();
      r.register::<AcquireIdempotencyToken>();
      r
    };

    let registry = Box::new(registry.bind(host));

    let input = SyncSystemAsRoot {
      config: self.config.clone(),
    };
    exec_as_rec::<StateAndHost<LocalLinux, ()>, SyncSystemAsRoot>(
      UserRef::ROOT,
      Some(
        self
          .config
          .vault
          .as_ref()
          .ok_or_else(|| "missing vault".to_string())?
          .sudo_password
          .as_deref()
          .ok_or_else(|| "missing sudo_password".to_string())?,
      ),
      &input,
      registry,
    )
    .await
    .unwrap()
  }
}

impl<'h, H: 'h + StatefulHost + ExecHost> NamedTask<&'h H> for SyncSystem {
  const NAME: TaskName = TaskName::new("system::SyncSystem");
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct SyncSystemAsRoot {
  pub config: RawConfig,
}

pub enum SyncSystemAsRootError {}

#[async_trait]
impl<'h, H: StatefulHost + ExecHost + LinuxUserHost + LinuxFsHost> AsyncFn<&'h H> for SyncSystemAsRoot
where
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Debug + Clone + Send + Sync,
  H::Group: Debug + Send + Sync,
{
  type Output = TaskResult<(), String>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let changed = InitializeSystem.run(host).await.map_err(|e| e.to_string())?.changed;

    let (apps, vault) = match (self.config.apps.as_ref(), self.config.vault.as_ref()) {
      (Some(apps), Some(vault)) => (apps, vault),
      _ => return Ok(TaskSuccess { changed, output: () }),
    };
    let sudo_password = vault.sudo_password.as_deref().expect("missing sudo_password");

    if let Some(app_config) = apps.brute.as_ref() {
      for (channel_name, channel_config) in &app_config.channels {
        let vault = vault
          .brute
          .get(channel_name)
          .unwrap_or_else(|| panic!("Missing vault for {}", channel_name));
        let task = SyncBrute {
          sudo_password,
          channel_name,
          app_config,
          channel_config,
          vault,
        };
        match task.run(host).await {
          Ok(_) => eprintln!("Ok"),
          Err(e) => eprintln!("Error: {e}"),
        };
      }
    }

    if let Some(app_config) = apps.dinorpg.as_ref() {
      for (channel_name, channel_config) in &app_config.channels {
        let vault = vault
          .dinorpg
          .get(channel_name)
          .unwrap_or_else(|| panic!("Missing vault for {}", channel_name));
        let task = SyncDinorpg {
          borgbase_api_key: self.config.vault.as_ref().and_then(|v| v.borgbase.as_deref()),
          sudo_password,
          channel_name,
          app_config,
          channel_config,
          vault,
        };
        match task.run(host).await {
          Ok(_) => eprintln!("Ok"),
          Err(e) => eprintln!("Error: {e}"),
        };
      }
    }

    if let Some(app_config) = apps.emush.as_ref() {
      for (channel_name, channel_config) in &app_config.channels {
        let vault = vault
          .emush
          .get(channel_name)
          .unwrap_or_else(|| panic!("Missing vault for {}", channel_name));
        let task = SyncEmush {
          sudo_password,
          channel_name,
          app_config,
          channel_config,
          vault,
        };
        match task.run(host).await {
          Ok(_) => eprintln!("Ok"),
          Err(e) => eprintln!("Error: {e}"),
        };
      }
    }

    if let Some(app_config) = apps.epopotamo.as_ref() {
      for (channel_name, channel_config) in &app_config.channels {
        let vault = vault
          .epopotamo
          .get(channel_name)
          .unwrap_or_else(|| panic!("Missing vault for {}", channel_name));
        let task = SyncEpopotamo {
          sudo_password,
          channel_name,
          app_config,
          channel_config,
          vault,
        };
        match task.run(host).await {
          Ok(_) => eprintln!("Ok"),
          Err(e) => eprintln!("Error: {e}"),
        };
      }
    }

    if let Some(app_config) = apps.frutibandas.as_ref() {
      for (channel_name, channel_config) in &app_config.channels {
        let task = SyncFrutibandas {
          sudo_password,
          channel_name,
          app_config,
          channel_config,
        };
        match task.run(host).await {
          Ok(_) => eprintln!("Ok"),
          Err(e) => eprintln!("Error: {e}"),
        };
      }
    }

    let auto_targets: [(
      _,
      _,
      for<'a> fn(&'a RawVaultConfig, &'a str) -> Option<&'a RawAutoVault>,
    ); 2] = [
      (
        "dinocard",
        apps.dinocard.as_ref(),
        |vault: &RawVaultConfig, channel: &str| vault.dinocard.get(channel),
      ),
      (
        "kadokadeo",
        apps.kadokadeo.as_ref(),
        |vault: &RawVaultConfig, channel: &str| vault.kadokadeo.get(channel),
      ),
    ];

    for (app_name, app_config, get_vault) in auto_targets {
      let app_config = match app_config {
        Some(app_config) => app_config,
        None => continue,
      };
      for (channel_name, channel_config) in &app_config.channels {
        let vault = get_vault(vault, channel_name.as_str())
          .unwrap_or_else(|| panic!("Missing vault for {app_name}.{channel_name}"));
        let task = SyncAuto {
          app_name,
          sudo_password,
          channel_name,
          app_config,
          channel_config,
          vault,
        };
        match task.run(host).await {
          Ok(_) => eprintln!("Ok"),
          Err(e) => eprintln!("Error: {e}"),
        };
      }
    }

    if let Some(app_config) = apps.kingdom.as_ref() {
      for (channel_name, channel_config) in &app_config.channels {
        let vault = vault
          .kingdom
          .get(channel_name)
          .unwrap_or_else(|| panic!("Missing vault for {}", channel_name));
        let task = SyncKingdom {
          sudo_password,
          channel_name,
          app_config,
          channel_config,
          vault,
        };
        match task.run(host).await {
          Ok(_) => eprintln!("Ok"),
          Err(e) => eprintln!("Error: {e}"),
        };
      }
    }

    if let Some(app_config) = apps.neoparc.as_ref() {
      for (channel_name, channel_config) in &app_config.channels {
        let vault = vault
          .neoparc
          .get(channel_name)
          .unwrap_or_else(|| panic!("Missing vault for {}", channel_name));
        let task = SyncNeoparc {
          sudo_password,
          channel_name,
          app_config,
          channel_config,
          vault,
        };
        match task.run(host).await {
          Ok(_) => eprintln!("Ok"),
          Err(e) => eprintln!("Error: {e}"),
        };
      }
    }

    Ok(TaskSuccess { changed, output: () })
  }
}

impl<'h, H> NamedTask<&'h H> for SyncSystemAsRoot
where
  H: 'h + StatefulHost + ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Clone + Debug + Sync,
  H::Group: Debug + Sync + Send,
{
  const NAME: TaskName = TaskName::new("system::SyncSystemAsRoot");
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct InitializeSystem;

#[derive(Debug, Clone, PartialEq, Eq, Error, Serialize, Deserialize)]
pub enum InitializeSystemError {
  #[error("failed to acquire idempotency token")]
  AcquireIdempotencyToken(String),
  #[error("pacman upgrade failed")]
  Pacman(String),
}

#[async_trait]
impl<'h, H: StatefulHost + ExecHost + LinuxUserHost + LinuxFsHost> AsyncFn<&'h H> for InitializeSystem
where
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Debug + Clone + Sync,
{
  type Output = TaskResult<(), InitializeSystemError>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let key = IdempotencyKey {
      name: "system::InitializeSystem".to_string(),
      expiry: Some(Duration::from_secs(3600)),
    };

    let token = host
      .acquire_idempotency_token(key)
      .await
      .map_err(InitializeSystemError::AcquireIdempotencyToken)?;
    if token.is_none() {
      eprintln!("failed to acquire token for system upgrade");
      return Ok(TaskSuccess {
        changed: false,
        output: (),
      });
    }

    eprintln!("system upgrade: start");
    PacmanSync::new()
      .refresh(true)
      .sys_upgrade(true)
      .needed(true)
      .run(host)
      .await
      .map_err(|e| InitializeSystemError::Pacman(e.to_string()))?;
    eprintln!("system upgrade: done");

    Ok(TaskSuccess {
      changed: true,
      output: (),
    })
  }
}

impl<'h, H> NamedTask<&'h H> for InitializeSystem
where
  H: 'h + StatefulHost + ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Clone + Debug + Sync,
{
  const NAME: TaskName = TaskName::new("system::InitializeSystem");
}

#[derive(Debug, Clone)]
pub struct SyncAuto<'a> {
  pub app_name: &'a str,
  pub sudo_password: &'a str,
  pub channel_name: &'a str,
  pub app_config: &'a RawAppConfig<RawAutoChannelConfig>,
  pub channel_config: &'a RawAutoChannelConfig,
  pub vault: &'a RawAutoVault,
}

#[derive(Debug, Error)]
pub enum SyncAutoError {
  #[error("inner error: {0}")]
  Inner(String),
}

#[async_trait]
impl<'a, 'h, H> AsyncFn<&'h H> for SyncAuto<'a>
where
  H: 'h + StatefulHost + ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Clone + Debug + Sync,
{
  type Output = TaskResult<(), SyncAutoError>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let app_name = self.app_name;
    let sudo_password = self.sudo_password;
    let channel_name = self.channel_name;
    let app_config = self.app_config;
    let channel_config = self.channel_config;
    let vault = self.vault;

    let full_name = format!("{}.{}", app_name, channel_name);
    eprintln!("Syncing: {full_name}");

    let is_reset = is_reset(host, full_name.as_str(), channel_config.state()).await;
    if is_reset {
      eprintln!("State will be reset");
    }

    let channel_target = {
      let mut target = ChannelTarget::new(full_name.clone());
      target.domain = Some(ChannelDomainTarget {
        main: channel_config.domain.clone(),
        www: true,
        cert: channel_config.https.as_ref().map(|conf| CertTarget {
          name: full_name.clone(),
          email: conf.email.clone(),
        }),
      });
      target.main_port = Some(channel_config.main_port);
      target.fallback_server = Some(channel_config.fallback_port);
      target.postgres = Some(ChannelPostgresTarget {
        admin_password: vault.db_admin_password.clone(),
        main_password: vault.db_main_password.clone(),
        read_password: vault.db_read_password.clone(),
        reset: is_reset,
      });
      target.node = Some("^18.10.0".parse().expect("valid version version requirement"));
      target
    };
    let channel_target = AutoChannelTarget {
      inner: channel_target,
      vault: AutoVault {
        secret: vault.secret.clone(),
        etwin_client_secret: vault.etwin_client_secret.clone(),
      },
      etwin_uri: Url::parse(channel_config.etwin_uri.as_str()).expect("invalid etwin_uri"),
      etwin_client_id: channel_config.etwin_client_id.clone(),
    };
    let release_target = AutoReleaseTarget {
      channel: channel_target,
      repository: app_config.repository.clone(),
      git_ref: channel_config.release.clone(),
    };
    let deploy_task = DeployAuto {
      pass: sudo_password,
      target: release_target,
    };
    deploy_task.run(host).await.map_err(SyncAutoError::Inner)
  }
}

impl<'a, 'h, H> NamedTask<&'h H> for SyncAuto<'a>
where
  H: 'h + StatefulHost + ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Clone + Debug + Sync,
{
  const NAME: TaskName = TaskName::new("system::SyncAuto");
}

#[derive(Debug, Clone)]
pub struct SyncBrute<'a> {
  pub sudo_password: &'a str,
  pub channel_name: &'a str,
  pub app_config: &'a RawAppConfig<RawBruteChannelConfig>,
  pub channel_config: &'a RawBruteChannelConfig,
  pub vault: &'a RawBruteVault,
}

#[derive(Debug, Error)]
pub enum SyncBruteError {
  #[error("inner error: {0}")]
  Inner(String),
}

#[async_trait]
impl<'a, 'h, H: StatefulHost + ExecHost + LinuxUserHost + LinuxFsHost> AsyncFn<&'h H> for SyncBrute<'a>
where
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Debug + Clone + Sync,
{
  type Output = TaskResult<(), SyncBruteError>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let app_name = "brute";
    let sudo_password = self.sudo_password;
    let channel_name = self.channel_name;
    let app_config = self.app_config;
    let channel_config = self.channel_config;
    let vault = self.vault;

    let full_name = format!("{}.{}", app_name, channel_name);
    eprintln!("Syncing: {full_name}");

    let is_reset = is_reset(host, full_name.as_str(), channel_config.state()).await;
    if is_reset {
      eprintln!("State will be reset");
    }

    let channel_target = {
      let mut target = ChannelTarget::new(full_name.clone());
      target.domain = Some(ChannelDomainTarget {
        main: channel_config.domain.clone(),
        www: true,
        cert: channel_config.https.as_ref().map(|conf| CertTarget {
          name: full_name.clone(),
          email: conf.email.clone(),
        }),
      });
      target.main_port = Some(channel_config.main_port);
      target.fallback_server = Some(channel_config.fallback_port);
      target.postgres = Some(ChannelPostgresTarget {
        admin_password: vault.db_admin_password.clone(),
        main_password: vault.db_main_password.clone(),
        read_password: vault.db_read_password.clone(),
        reset: is_reset,
      });
      target.node = Some("=16.13.1".parse().expect("valid version version requirement"));
      target
    };
    let discord_webhook = if let Some(id) = channel_config.discord_webhook_id.clone() {
      let token = vault
        .discord_webhook_token
        .clone()
        .expect("missing discord_webhook_token");
      Some(DiscordWebhook { id, token })
    } else {
      None
    };
    let logs_webhook = if let Some(id) = channel_config.logs_webhook_id.clone() {
      let token = vault.logs_webhook_token.clone().expect("missing logs_webhook_token");
      Some(DiscordWebhook { id, token })
    } else {
      None
    };
    let brute_channel_target = BruteChannelTarget {
      inner: channel_target,
      vault: BruteVault {
        etwin_client_secret: vault.etwin_client_secret.clone(),
        discord_webhook,
        logs_webhook,
      },
      etwin_uri: channel_config.etwin_uri.parse().expect("invalid etwin URI"),
      etwin_client_id: channel_config.etwin_client_id.clone(),
    };
    let release_target = BruteReleaseTarget {
      channel: brute_channel_target,
      repository: app_config.repository.clone(),
      git_ref: channel_config.release.clone(),
    };
    let deploy_task = DeployBrute {
      pass: sudo_password,
      target: release_target,
    };
    deploy_task.run(host).await.map_err(SyncBruteError::Inner)
  }
}

impl<'a, 'h, H> NamedTask<&'h H> for SyncBrute<'a>
where
  H: 'h + StatefulHost + ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Clone + Debug + Sync,
{
  const NAME: TaskName = TaskName::new("system::SyncBrute");
}

#[derive(Debug, Clone)]
pub struct SyncDinorpg<'a> {
  pub sudo_password: &'a str,
  pub borgbase_api_key: Option<&'a str>,
  pub channel_name: &'a str,
  pub app_config: &'a RawAppConfig<RawDinorpgChannelConfig>,
  pub channel_config: &'a RawDinorpgChannelConfig,
  pub vault: &'a RawDinorpgVault,
}

#[derive(Debug, Error)]
pub enum SyncDinorpgError {
  #[error("inner error: {0}")]
  Inner(String),
}

#[async_trait]
impl<'a, 'h, H> AsyncFn<&'h H> for SyncDinorpg<'a>
where
  H: 'h + StatefulHost + ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Clone + Debug + Sync,
{
  type Output = TaskResult<(), SyncDinorpgError>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let app_name = "dinorpg";
    let sudo_password = self.sudo_password;
    let channel_name = self.channel_name;
    let app_config = self.app_config;
    let channel_config = self.channel_config;
    let vault = self.vault;

    let full_name = format!("{}.{}", app_name, channel_name);
    eprintln!("Syncing: {full_name}");

    let is_reset = is_reset(host, full_name.as_str(), channel_config.state()).await;
    if is_reset {
      eprintln!("State will be reset");
    }

    let channel_target = {
      let mut target = ChannelTarget::new(full_name.clone());
      target.domain = Some(ChannelDomainTarget {
        main: channel_config.domain.clone(),
        www: true,
        cert: channel_config.https.as_ref().map(|conf| CertTarget {
          name: full_name.clone(),
          email: conf.email.clone(),
        }),
      });
      target.main_port = Some(channel_config.main_port);
      target.fallback_server = Some(channel_config.fallback_port);
      target.postgres = Some(ChannelPostgresTarget {
        admin_password: vault.db_admin_password.clone(),
        main_password: vault.db_main_password.clone(),
        read_password: vault.db_read_password.clone(),
        reset: is_reset,
      });
      target.nvm = true;
      if let Some(borgbase_api_key) = self.borgbase_api_key {
        target.borg = Some(BorgTarget {
          borgbase_api_key: borgbase_api_key.to_string(),
          repo_password: vault.borgbase_repo.clone(),
        });
      }
      target
    };
    let channel_target = DinorpgChannelTarget {
      inner: channel_target,
      vault: DinorpgVault {
        secret_key: vault.secret_key.clone(),
        etwin_client_secret: vault.etwin_client_secret.clone(),
      },
      etwin_uri: channel_config.etwin_uri.clone(),
      etwin_client_id: channel_config.etwin_client_id.clone(),
    };
    let release_target = DinorpgReleaseTarget {
      channel: channel_target,
      repository: app_config.repository.clone(),
      git_ref: channel_config.release.clone(),
    };
    let deploy_task = DeployDinorpg {
      pass: sudo_password,
      target: release_target,
    };
    deploy_task.run(host).await.map_err(SyncDinorpgError::Inner)
  }
}

impl<'a, 'h, H> NamedTask<&'h H> for SyncDinorpg<'a>
where
  H: 'h + StatefulHost + ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Clone + Debug + Sync,
{
  const NAME: TaskName = TaskName::new("system::SyncDinorpg");
}

#[derive(Debug, Clone)]
pub struct SyncEmush<'a> {
  pub sudo_password: &'a str,
  pub channel_name: &'a str,
  pub app_config: &'a RawAppConfig<RawEmushChannelConfig>,
  pub channel_config: &'a RawEmushChannelConfig,
  pub vault: &'a RawEmushVault,
}

#[derive(Debug, Error)]
pub enum SyncEmushError {
  #[error("inner error: {0}")]
  Inner(String),
}

#[async_trait]
impl<'a, 'h, H> AsyncFn<&'h H> for SyncEmush<'a>
where
  H: 'h + StatefulHost + ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Clone + Debug + Sync,
{
  type Output = TaskResult<(), SyncEmushError>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let app_name = "emush";
    let sudo_password = self.sudo_password;
    let channel_name = self.channel_name;
    let app_config = self.app_config;
    let channel_config = self.channel_config;
    let vault = self.vault;

    let full_name = format!("{}.{}", app_name, channel_name);
    eprintln!("Syncing: {full_name}");

    let is_reset = is_reset(host, full_name.as_str(), channel_config.state()).await;
    if is_reset {
      eprintln!("State will be reset");
    }

    let channel_target = {
      let mut target = ChannelTarget::new(full_name.clone());
      target.domain = Some(ChannelDomainTarget {
        main: channel_config.domain.clone(),
        www: true,
        cert: channel_config.https.as_ref().map(|conf| CertTarget {
          name: full_name.clone(),
          email: conf.email.clone(),
        }),
      });
      target.main_port = Some(channel_config.main_port);
      target.fallback_server = Some(channel_config.fallback_port);
      target.postgres = Some(ChannelPostgresTarget {
        admin_password: vault.db_admin_password.clone(),
        main_password: vault.db_main_password.clone(),
        read_password: vault.db_read_password.clone(),
        reset: is_reset,
      });
      target.nvm = true;
      target.node = Some("^18.10.0".parse().expect("valid version version requirement"));
      target.php = Some(PhpAvailable {
        ext_curl: true,
        ext_ctype: false,
        ext_iconv: true,
        ext_intl: true,
        ext_pdo_pgsql: true,
        ext_pgsql: true,
        ext_sodium: true,
        ext_zip: true,
      });
      target
    };
    let channel_target = EmushChannelTarget {
      inner: channel_target,
      vault: EmushVault {
        etwin_client_secret: vault.etwin_client_secret.clone(),
        app_secret: vault.app_secret.clone(),
        access_passphrase: vault.access_passphrase.clone(),
        discord_webhook: vault.discord_webhook.clone(),
      },
      etwin_uri: Url::parse(&channel_config.etwin_uri).expect("invalid etwin_uri"),
      etwin_client_id: channel_config.etwin_client_id.clone(),
      admin_user_id: channel_config.admin_user_id.clone(),
    };
    let release_target = EmushReleaseTarget {
      channel: channel_target,
      repository: app_config.repository.clone(),
      git_ref: channel_config.release.clone(),
      is_after_db_reset: is_reset,
    };
    let deploy_task = DeployEmush {
      pass: sudo_password,
      target: release_target,
    };
    deploy_task.run(host).await.map_err(SyncEmushError::Inner)
  }
}

impl<'a, 'h, H> NamedTask<&'h H> for SyncEmush<'a>
where
  H: 'h + StatefulHost + ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Clone + Debug + Sync,
{
  const NAME: TaskName = TaskName::new("system::SyncEmush");
}

#[derive(Debug, Clone)]
pub struct SyncEpopotamo<'a> {
  pub sudo_password: &'a str,
  pub channel_name: &'a str,
  pub app_config: &'a RawAppConfig<RawEpopotamoChannelConfig>,
  pub channel_config: &'a RawEpopotamoChannelConfig,
  pub vault: &'a RawEpopotamoVault,
}

#[derive(Debug, Error)]
pub enum SyncEpopotamoError {
  #[error("inner error: {0}")]
  Inner(String),
}

#[async_trait]
impl<'a, 'h, H> AsyncFn<&'h H> for SyncEpopotamo<'a>
where
  H: 'h + StatefulHost + ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Clone + Debug + Sync,
{
  type Output = TaskResult<(), SyncEpopotamoError>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let app_name = "epopotamo";
    let sudo_password = self.sudo_password;
    let channel_name = self.channel_name;
    let app_config = self.app_config;
    let channel_config = self.channel_config;
    let vault = self.vault;

    let full_name = format!("{}.{}", app_name, channel_name);
    eprintln!("Syncing: {full_name}");

    let is_reset = is_reset(host, full_name.as_str(), channel_config.state()).await;
    if is_reset {
      eprintln!("State will be reset");
    }

    let channel_target = {
      let mut target = ChannelTarget::new(full_name.clone());
      target.domain = Some(ChannelDomainTarget {
        main: channel_config.domain.clone(),
        www: true,
        cert: channel_config.https.as_ref().map(|conf| CertTarget {
          name: full_name.clone(),
          email: conf.email.clone(),
        }),
      });
      target.main_port = Some(channel_config.main_port);
      target.fallback_server = Some(channel_config.fallback_port);
      target.postgres = Some(ChannelPostgresTarget {
        admin_password: vault.db_admin_password.clone(),
        main_password: vault.db_main_password.clone(),
        read_password: vault.db_read_password.clone(),
        reset: is_reset,
      });
      target.php = Some(PhpAvailable {
        ext_curl: false,
        ext_ctype: false,
        ext_iconv: false,
        ext_intl: false,
        ext_pdo_pgsql: true,
        ext_pgsql: true,
        ext_sodium: false,
        ext_zip: false,
      });
      target
    };
    let channel_target = EpopotamoChannelTarget {
      inner: channel_target,
      vault: EpopotamoVault {
        etwin_client_secret: vault.etwin_client_secret.clone(),
      },
      etwin_uri: Url::parse(channel_config.etwin_uri.as_str()).expect("invalid etwin_uri"),
      etwin_client_id: channel_config.etwin_client_id.clone(),
    };
    let release_target = EpopotamoReleaseTarget {
      channel: channel_target,
      repository: app_config.repository.clone(),
      git_ref: channel_config.release.clone(),
    };
    let deploy_task = DeployEpopotamo {
      pass: sudo_password,
      target: release_target,
    };
    deploy_task.run(host).await.map_err(SyncEpopotamoError::Inner)
  }
}

impl<'a, 'h, H> NamedTask<&'h H> for SyncEpopotamo<'a>
where
  H: 'h + StatefulHost + ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Clone + Debug + Sync,
{
  const NAME: TaskName = TaskName::new("system::SyncEpopotamo");
}

#[derive(Debug, Clone)]
pub struct SyncKingdom<'a> {
  pub sudo_password: &'a str,
  pub channel_name: &'a str,
  pub app_config: &'a RawAppConfig<RawKingdomChannelConfig>,
  pub channel_config: &'a RawKingdomChannelConfig,
  pub vault: &'a RawKingdomVault,
}

#[derive(Debug, Error)]
pub enum SyncKingdomError {
  #[error("inner error: {0}")]
  Inner(String),
}

#[async_trait]
impl<'a, 'h, H> AsyncFn<&'h H> for SyncKingdom<'a>
where
  H: 'h + StatefulHost + ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Clone + Debug + Sync,
{
  type Output = TaskResult<(), SyncKingdomError>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let app_name = "kingdom";
    let sudo_password = self.sudo_password;
    let channel_name = self.channel_name;
    let app_config = self.app_config;
    let channel_config = self.channel_config;
    let vault = self.vault;

    let full_name = format!("{}.{}", app_name, channel_name);
    eprintln!("Syncing: {full_name}");

    let is_reset = is_reset(host, full_name.as_str(), channel_config.state()).await;
    if is_reset {
      eprintln!("State will be reset");
    }

    let channel_target = {
      let mut target = ChannelTarget::new(full_name.clone());
      target.domain = Some(ChannelDomainTarget {
        main: channel_config.domain.clone(),
        www: true,
        cert: channel_config.https.as_ref().map(|conf| CertTarget {
          name: full_name.clone(),
          email: conf.email.clone(),
        }),
      });
      target.main_port = Some(channel_config.main_port);
      target.fallback_server = Some(channel_config.fallback_port);
      target.postgres = Some(ChannelPostgresTarget {
        admin_password: vault.db_admin_password.clone(),
        main_password: vault.db_main_password.clone(),
        read_password: vault.db_read_password.clone(),
        reset: is_reset,
      });
      target.php = Some(PhpAvailable {
        ext_curl: false,
        ext_ctype: true,
        ext_iconv: true,
        ext_intl: false,
        ext_pdo_pgsql: true,
        ext_pgsql: false,
        ext_sodium: false,
        ext_zip: false,
      });
      target
    };
    let channel_target = KingdomChannelTarget {
      inner: channel_target,
      vault: KingdomVault {
        etwin_client_secret: vault.etwin_client_secret.clone(),
        app_secret: vault.app_secret.clone(),
      },
      etwin_uri: Url::parse(&channel_config.etwin_uri).expect("invalid etwin_uri"),
      etwin_client_id: channel_config.etwin_client_id.clone(),
    };
    let release_target = KingdomReleaseTarget {
      channel: channel_target,
      repository: app_config.repository.clone(),
      git_ref: channel_config.release.clone(),
    };
    let deploy_task = DeployKingdom {
      pass: sudo_password,
      target: release_target,
    };
    deploy_task.run(host).await.map_err(SyncKingdomError::Inner)
  }
}

impl<'a, 'h, H> NamedTask<&'h H> for SyncKingdom<'a>
where
  H: 'h + StatefulHost + ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Clone + Debug + Sync,
{
  const NAME: TaskName = TaskName::new("system::SyncKingdom");
}

#[derive(Debug, Clone)]
pub struct SyncNeoparc<'a> {
  pub sudo_password: &'a str,
  pub channel_name: &'a str,
  pub app_config: &'a RawAppConfig<RawNeoparcChannelConfig>,
  pub channel_config: &'a RawNeoparcChannelConfig,
  pub vault: &'a RawNeoparcVault,
}

#[derive(Debug, Error)]
pub enum SyncNeoparcError {
  #[error("inner error: {0}")]
  Inner(String),
}

#[async_trait]
impl<'a, 'h, H: StatefulHost + ExecHost + LinuxUserHost + LinuxFsHost> AsyncFn<&'h H> for SyncNeoparc<'a>
where
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Debug + Clone + Sync,
{
  type Output = TaskResult<(), SyncNeoparcError>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let app_name = "neoparc";
    let sudo_password = self.sudo_password;
    let channel_name = self.channel_name;
    let app_config = self.app_config;
    let channel_config = self.channel_config;
    let vault = self.vault;

    let full_name = format!("{}.{}", app_name, channel_name);
    eprintln!("Syncing: {full_name}");

    let is_reset = is_reset(host, full_name.as_str(), channel_config.state()).await;
    if is_reset {
      eprintln!("State will be reset");
    }

    let channel_target = {
      let mut target = ChannelTarget::new(full_name.clone());
      target.domain = Some(ChannelDomainTarget {
        main: channel_config.domain.clone(),
        www: true,
        cert: channel_config.https.as_ref().map(|conf| CertTarget {
          name: full_name.clone(),
          email: conf.email.clone(),
        }),
      });
      target.main_port = Some(channel_config.main_port);
      target.fallback_server = Some(channel_config.fallback_port);
      target.postgres = Some(ChannelPostgresTarget {
        admin_password: vault.db_admin_password.clone(),
        main_password: vault.db_main_password.clone(),
        read_password: vault.db_read_password.clone(),
        reset: is_reset,
      });
      target.nvm = true;
      target
    };
    let neoparc_channel_target = NeoparcChannelTarget {
      inner: channel_target,
      vault: NeoparcVault {
        salt: vault.salt.clone(),
        seed: vault.seed.clone(),
        test: vault.test.clone(),
        etwin_client_secret: vault.etwin_client_secret.clone(),
      },
      etwin_uri: channel_config.etwin_uri.clone(),
      etwin_client_id: channel_config.etwin_client_id.clone(),
    };
    let release_target = NeoparcReleaseTarget {
      channel: neoparc_channel_target,
      repository: app_config.repository.clone(),
      git_ref: channel_config.release.clone(),
    };
    let deploy_task = DeployNeoparc {
      pass: sudo_password,
      target: release_target,
    };
    deploy_task.run(host).await.map_err(SyncNeoparcError::Inner)
  }
}

impl<'a, 'h, H> NamedTask<&'h H> for SyncNeoparc<'a>
where
  H: 'h + StatefulHost + ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Clone + Debug + Sync,
{
  const NAME: TaskName = TaskName::new("system::SyncNeoparc");
}

#[derive(Debug, Clone)]
pub struct SyncFrutibandas<'a> {
  pub sudo_password: &'a str,
  pub channel_name: &'a str,
  pub app_config: &'a RawAppConfig<RawFrutibandasChannelConfig>,
  pub channel_config: &'a RawFrutibandasChannelConfig,
  // pub vault: &'a RawFrutibandasVault,
}

#[derive(Debug, Error)]
pub enum SyncFrutibandasError {
  #[error("inner error: {0}")]
  Inner(String),
}

#[async_trait]
impl<'a, 'h, H: StatefulHost + ExecHost + LinuxUserHost + LinuxFsHost> AsyncFn<&'h H> for SyncFrutibandas<'a>
where
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Debug + Clone + Sync,
{
  type Output = TaskResult<(), SyncFrutibandasError>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let app_name = "frutibandas";
    let sudo_password = self.sudo_password;
    let channel_name = self.channel_name;
    let app_config = self.app_config;
    let channel_config = self.channel_config;
    // let vault = self.vault;

    let full_name = format!("{}.{}", app_name, channel_name);
    eprintln!("Syncing: {full_name}");

    let is_reset = is_reset(host, full_name.as_str(), channel_config.state()).await;
    if is_reset {
      eprintln!("State will be reset");
    }

    let channel_target = {
      let mut target = ChannelTarget::new(full_name.clone());
      target.domain = Some(ChannelDomainTarget {
        main: channel_config.domain.clone(),
        www: true,
        cert: channel_config.https.as_ref().map(|conf| CertTarget {
          name: full_name.clone(),
          email: conf.email.clone(),
        }),
      });
      target.main_port = Some(channel_config.main_port);
      target.fallback_server = Some(channel_config.fallback_port);
      target
    };
    let frutibandas_channel_target = FrutibandasChannelTarget { inner: channel_target };
    let release_target = FrutibandasReleaseTarget {
      channel: frutibandas_channel_target,
      repository: app_config.repository.clone(),
      git_ref: channel_config.release.clone(),
    };
    let deploy_task = DeployFrutibandas {
      pass: sudo_password,
      target: release_target,
    };
    deploy_task.run(host).await.map_err(SyncFrutibandasError::Inner)
  }
}

impl<'a, 'h, H> NamedTask<&'h H> for SyncFrutibandas<'a>
where
  H: 'h + StatefulHost + ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Clone + Debug + Sync,
{
  const NAME: TaskName = TaskName::new("system::SyncFrutibandas");
}

async fn is_reset<H: StatefulHost>(host: &H, full_name: &str, state: Option<&RawStateConfig>) -> bool {
  let state_token = match state {
    Some(state) => {
      let state_token = IdempotencyKey {
        name: format!("{}.state.{}", full_name, &state.token),
        expiry: None,
      };
      host
        .acquire_idempotency_token(state_token)
        .await
        .expect("acquire state idempotency token")
    }
    None => None,
  };

  state.map(|s| s.action == StateAction::Reset).unwrap_or(false) && state_token.is_some()
}
