use crate::etwin::channel::{
  ChannelDomain, ChannelFallbackServer, ChannelPaths, ChannelPostgres, ChannelTarget, PostgresCredentials,
};
use crate::task::fs::{EnsureDir, EnsureDirSymlink, EnsureFile};
use crate::task::git::GitCheckout;
use crate::task::nginx::{NginxAvailableSite, NginxEnableSite};
use crate::task::nvm::NvmEnv;
use crate::task::systemd::{SystemdState, SystemdUnit};
use crate::task::{exec_as, AsyncFn, NamedTask, TaskName, TaskResult, TaskSuccess};
use async_trait::async_trait;
use asys::common::fs::FsHost;
use asys::linux::fs::{FileMode, LinuxFsHost, LinuxMetadata, ResolveError};
use asys::linux::user::{GroupRef, LinuxUser, LinuxUserHost, Uid, UserRef};
use asys::local_linux::LocalLinux;
use asys::{Command, ExecHost};
use serde::{Deserialize, Serialize};
use std::fmt;
use std::fmt::Debug;
use std::path::PathBuf;
use url::Url;

const SKIP_BUILD: bool = false;

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct NeoparcReleaseTarget<S: AsRef<str> = String> {
  pub channel: NeoparcChannelTarget,
  pub repository: S,
  pub git_ref: String,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct NeoparcChannelTarget<S: AsRef<str> = String> {
  pub inner: ChannelTarget<S>,
  pub vault: NeoparcVault,
  pub etwin_uri: String,
  pub etwin_client_id: String,
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize, Serialize)]
pub struct NeoparcVault {
  pub salt: String,
  pub seed: String,
  pub test: String,
  pub etwin_client_secret: String,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct NeoparcChannel<User: LinuxUser, S: AsRef<str> = String> {
  name: S,
  user: User,
  domain: ChannelDomain<S>,
  paths: ChannelPaths,
  fallback_server: ChannelFallbackServer,
  postgres: ChannelPostgres,
  main_port: u16,
  nvm: NvmEnv,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct NeoparcRelease<User: LinuxUser, S: AsRef<str> = String> {
  release_dir: PathBuf,
  repository: S,
  repo_dir: PathBuf,
  channel: NeoparcChannel<User, S>,
  git_ref: String,
}

#[derive(Serialize, Deserialize)]
pub struct DeployNeoparc<S: AsRef<str> = String> {
  pub pass: S,
  pub target: NeoparcReleaseTarget,
}

#[async_trait]
impl<'h, H: ExecHost, S: AsRef<str> + Send + Sync> AsyncFn<&'h H> for DeployNeoparc<S> {
  type Output = TaskResult<(), String>;

  async fn run(&self, _host: &'h H) -> Self::Output {
    let input = DeployNeoparcAsRoot {
      target: self.target.clone(),
    };
    exec_as::<LocalLinux, DeployNeoparcAsRoot>(UserRef::ROOT, Some(self.pass.as_ref()), &input)
      .await
      .map_err(|e| e.to_string())?
  }
}

impl<'h, H: 'h + ExecHost> NamedTask<&'h H> for DeployNeoparc {
  const NAME: TaskName = TaskName::new("neoparc::DeployNeoparc");
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct DeployNeoparcAsRoot {
  target: NeoparcReleaseTarget,
}

#[async_trait]
impl<'h, H: ExecHost + LinuxUserHost + LinuxFsHost> AsyncFn<&'h H> for DeployNeoparcAsRoot
where
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Debug + Clone + Send + Sync,
  H::Group: Debug + Send + Sync,
{
  type Output = TaskResult<(), String>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let res = self.target.run(host).await;
    if let Err(e) = res {
      panic!("{:?}", e);
    }
    let res = res.unwrap();

    Ok(TaskSuccess {
      changed: res.changed,
      output: (),
    })
  }
}

impl<'h, H> NamedTask<&'h H> for DeployNeoparcAsRoot
where
  H: 'h + ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Debug + Clone + Send + Sync,
  H::Group: Debug + Send + Sync,
{
  const NAME: TaskName = TaskName::new("neoparc::DeployNeoparcAsRoot");
}

#[async_trait]
impl<'h, H> AsyncFn<&'h H> for NeoparcReleaseTarget
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Debug + Clone + Send + Sync,
  H::Group: Debug + Send + Sync,
{
  type Output = TaskResult<NeoparcRelease<H::User>, anyhow::Error>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let res = self.channel.run(host).await?;
    let channel = res.output;
    let mut changed = res.changed;

    let release_dir = channel.paths.releases.join(&self.git_ref);

    eprintln!("Create directory: {}", release_dir.display());
    changed = EnsureDir::new(release_dir.clone())
      .owner(UserRef::Id(channel.user.uid()))
      .group(GroupRef::Id(channel.user.gid()))
      .mode(FileMode::OWNER_ALL | FileMode::ALL_READ | FileMode::ALL_EXEC)
      .run(host)
      .await?
      .changed
      || changed;

    let repo_dir = release_dir.join("repo");

    let protocol = if channel.domain.cert.is_some() { "https" } else { "http" };
    let uri = format!("{}://{}", protocol, channel.domain.main.as_str());

    let br = BuildNeoparcRelease {
      repo_dir: repo_dir.clone(),
      remote: self.repository.clone(),
      git_ref: self.git_ref.clone(),
      frontend_config: FrontendConfig {
        backend_root: Url::parse(&uri)?,
      },
      backend_config_admin: BackendConfig {
        db_url: format!(
          "jdbc:postgresql://{}:{}/{}",
          channel.postgres.host, channel.postgres.port, channel.postgres.name
        ),
        db_credentials: channel.postgres.admin.clone(),
        main_port: channel.main_port,
        backend_root: Url::parse(&uri)?,
        frontend_root: Url::parse(&uri)?,
        salt: self.channel.vault.salt.clone(),
        seed: self.channel.vault.seed.clone(),
        test: self.channel.vault.test.clone(),
        etwin_uri: Url::parse(self.channel.etwin_uri.as_str())?,
        etwin_client_id: self.channel.etwin_client_id.clone(),
        etwin_client_secret: self.channel.vault.etwin_client_secret.clone(),
      },
      backend_config_main: BackendConfig {
        db_url: format!(
          "jdbc:postgresql://{}:{}/{}",
          channel.postgres.host, channel.postgres.port, channel.postgres.name
        ),
        db_credentials: channel.postgres.main.clone(),
        main_port: channel.main_port,
        backend_root: Url::parse(&uri)?,
        frontend_root: Url::parse(&uri)?,
        salt: self.channel.vault.salt.clone(),
        seed: self.channel.vault.seed.clone(),
        test: self.channel.vault.test.clone(),
        etwin_uri: Url::parse(self.channel.etwin_uri.as_str())?,
        etwin_client_id: self.channel.etwin_client_id.clone(),
        etwin_client_secret: self.channel.vault.etwin_client_secret.clone(),
      },
    };

    if !SKIP_BUILD {
      changed = exec_as::<LocalLinux, _>(UserRef::Id(channel.user.uid()), None, &br)
        .await?
        .unwrap()
        .changed
        || changed;
    }

    let release = NeoparcRelease {
      release_dir,
      repository: self.repository.clone(),
      repo_dir,
      channel,
      git_ref: self.git_ref.clone(),
    };

    changed = (UpdateActiveNeoparcRelease {
      release: release.clone(),
    })
    .run(host)
    .await?
    .changed
      || changed;

    Ok(TaskSuccess {
      changed,
      output: release,
    })
  }
}

#[async_trait]
impl<'h, H> AsyncFn<&'h H> for NeoparcChannelTarget
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  H::Group: Send + Sync,
  H::User: Send + Sync + Debug,
  <H as FsHost>::Metadata: LinuxMetadata,
{
  type Output = TaskResult<NeoparcChannel<H::User>, anyhow::Error>;

  async fn run(&self, host: &'h H) -> Self::Output {
    eprintln!("Neoparc: Create channel");
    let res = self.inner.run(host).await?;

    eprintln!("Channel ready");
    let output = NeoparcChannel {
      name: res.output.name,
      user: res.output.user,
      domain: res.output.domain.expect("Expected associated domain name"),
      paths: res.output.paths,
      fallback_server: res.output.fallback_server.expect("Expected fallback server"),
      postgres: res.output.postgres.expect("Expected Postgres database"),
      nvm: res.output.nvm.expect("Expected NVM environment"),
      main_port: res.output.main_port.expect("Expected main port"),
    };

    Ok(TaskSuccess {
      changed: res.changed,
      output,
    })
  }
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct BuildNeoparcRelease {
  repo_dir: PathBuf,
  remote: String,
  git_ref: String,
  frontend_config: FrontendConfig,
  backend_config_admin: BackendConfig,
  backend_config_main: BackendConfig,
}

#[async_trait]
impl<'h, H> AsyncFn<&'h H> for BuildNeoparcRelease
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Send + Sync,
{
  type Output = Result<TaskSuccess<()>, ()>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let gc = GitCheckout {
      path: self.repo_dir.clone(),
      remote: self.remote.clone(),
      r#ref: self.git_ref.clone(),
    };

    eprintln!("Checkout repository: {:?} {:?}", self.remote, self.git_ref);
    let res = gc.run(host).await.unwrap();
    let changed = res.changed;

    let backend_dir = self.repo_dir.join("backend");
    let frontend_dir = self.repo_dir.join("frontend");

    eprintln!("Configure the backend build (admin)");
    EnsureFile::new(backend_dir.join("src/main/resources/application.yml"))
      .content(self.backend_config_admin.to_string())
      .mode(FileMode::OWNER_READ | FileMode::OWNER_WRITE)
      .run(host)
      .await
      .unwrap();

    eprintln!("Sync the DB");
    let cmd = Command::new(backend_dir.join("gradlew").to_str().unwrap())
      .arg("--no-daemon")
      .arg(":flywayMigrate")
      .current_dir(backend_dir.to_str().unwrap());
    host.exec(&cmd).unwrap();

    eprintln!("Configure the backend build (main)");
    EnsureFile::new(backend_dir.join("src/main/resources/application.yml"))
      .content(self.backend_config_main.to_string())
      .mode(FileMode::OWNER_READ | FileMode::OWNER_WRITE)
      .run(host)
      .await
      .unwrap();

    eprintln!("Build the backend");
    let cmd = Command::new(backend_dir.join("gradlew").to_str().unwrap())
      .arg("--no-daemon")
      .arg(":jar")
      .current_dir(backend_dir.to_str().unwrap());
    host.exec(&cmd).unwrap();

    eprintln!("Install Node dependencies");
    let cmd = Command::new("yarn")
      .arg("workspaces")
      .arg("focus")
      .current_dir(frontend_dir.to_str().unwrap());
    host.exec(&cmd).unwrap();

    eprintln!("Configure the frontend build");
    EnsureFile::new(frontend_dir.join(".env"))
      .content(self.frontend_config.to_string())
      .mode(FileMode::OWNER_READ | FileMode::OWNER_WRITE)
      .run(host)
      .await
      .unwrap();

    eprintln!("Build the frontend");
    let cmd = Command::new("yarn")
      .arg("run")
      .arg("build")
      .current_dir(frontend_dir.to_str().unwrap());
    host.exec(&cmd).unwrap();

    Ok(TaskSuccess { changed, output: () })
  }
}

impl<'h, H> NamedTask<&'h H> for BuildNeoparcRelease
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Send + Sync,
{
  const NAME: TaskName = TaskName::new("neoparc::BuildNeoparcRelease");
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize)]
pub struct UpdateActiveNeoparcRelease<User: LinuxUser, S: AsRef<str> = String> {
  release: NeoparcRelease<User, S>,
}

#[async_trait]
impl<'h, H, S> AsyncFn<&'h H> for UpdateActiveNeoparcRelease<H::User, S>
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Send + Sync,
  S: AsRef<str> + Send + Sync,
{
  type Output = TaskResult<(), anyhow::Error>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let service_name = format!("{}.service", self.release.channel.name.as_ref());

    let mut changed = false;
    let active_release_link = self.release.channel.paths.home.join("active");
    let previous_release_link = self.release.channel.paths.home.join("previous");
    let old_release_dir = {
      match host.resolve(&active_release_link) {
        Ok(old) => Some(old),
        Err(ResolveError::NotFound) => None,
        Err(e) => return Err(e.into()),
      }
    };
    let new_release_dir = host.resolve(&self.release.release_dir)?;

    if let Some(old_release_dir) = old_release_dir {
      if old_release_dir != new_release_dir {
        eprintln!("Start to disable old release");
        // There is already an active release but it does not match the target
        // release: stop the old version before proceeding to free the channel
        // resources (database, port)
        EnsureDirSymlink::new(previous_release_link.clone())
          .points_to(old_release_dir.clone())
          .owner(self.release.channel.user.uid())
          .group(self.release.channel.user.gid())
          .mode(FileMode::ALL_READ | FileMode::ALL_EXEC)
          .run(host)
          .await?;

        SystemdUnit::new(&service_name)
          .state(SystemdState::Inactive)
          .run(host)
          .await?;

        eprintln!("Old release disabled");
        changed = true;
      }
    }

    eprintln!("UpgradingDatabase");
    changed = exec_as::<LocalLinux, _>(
      UserRef::Id(self.release.channel.user.uid()),
      None,
      &UpgradeNeoparcDatabase {
        repo_dir: self.release.repo_dir.clone(),
      },
    )
    .await?
    .unwrap()
    .changed
      || changed;

    eprintln!("Generating Systemd unit");
    let backend_dir = self.release.channel.paths.home.join("active/repo/backend");
    let service = NeoparcService {
      channel_name: self.release.channel.name.as_ref().to_string(),
      backend_dir,
      user_name: self.release.channel.user.name().to_string(),
    };
    eprintln!("Writing Systemd unit");
    changed = EnsureFile::new(PathBuf::from("/etc/systemd/system").join(&service_name))
      .content(service.to_string())
      .owner(Uid::ROOT)
      .mode(FileMode::ALL_READ)
      .run(host)
      .await?
      .changed
      || changed;

    eprintln!("Generating nginx config");
    let nginx_config = get_nginx_config(&self.release.channel, self.release.git_ref.as_str());
    eprintln!("Writing nginx config");
    NginxAvailableSite::new(self.release.channel.name.as_ref(), nginx_config)
      .run(host)
      .await?;
    eprintln!("Enabling nginx config");
    NginxEnableSite::new(self.release.channel.name.as_ref())
      .run(host)
      .await?;

    eprintln!("Marking release as active");
    EnsureDirSymlink::new(active_release_link.clone())
      .points_to(new_release_dir.clone())
      .owner(self.release.channel.user.uid())
      .group(self.release.channel.user.gid())
      .mode(FileMode::ALL_READ | FileMode::ALL_EXEC)
      .run(host)
      .await?;

    eprintln!("Starting service");
    changed = SystemdUnit::new(&service_name)
      .enabled(true)
      .state(SystemdState::Active)
      .run(host)
      .await?
      .changed
      || changed;

    Ok(TaskSuccess { changed, output: () })
  }
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct UpgradeNeoparcDatabase {
  repo_dir: PathBuf,
}

#[async_trait]
impl<'h, H: ExecHost + LinuxUserHost + LinuxFsHost> AsyncFn<&'h H> for UpgradeNeoparcDatabase
where
  <H as FsHost>::Metadata: LinuxMetadata,
{
  type Output = Result<TaskSuccess<()>, ()>;

  async fn run(&self, _host: &'h H) -> Self::Output {
    Ok(TaskSuccess {
      changed: true,
      output: (),
    })
  }
}

impl<'h, H: 'h + ExecHost + LinuxUserHost + LinuxFsHost> NamedTask<&'h H> for UpgradeNeoparcDatabase
where
  <H as FsHost>::Metadata: LinuxMetadata,
{
  const NAME: TaskName = TaskName::new("neoparc::UpgradeNeoparcDatabase");
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
struct FrontendConfig {
  backend_root: Url,
}

impl fmt::Display for FrontendConfig {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    writeln!(f, "REACT_APP_HOST=0.0.0.0")?;
    writeln!(f, "HOST=0.0.0.0")?;
    writeln!(f, "REACT_APP_API_SERVER={}", &self.backend_root)?;
    Ok(())
  }
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
struct BackendConfig<S: AsRef<str> = String> {
  db_url: String,
  db_credentials: PostgresCredentials,
  main_port: u16,
  backend_root: Url,
  frontend_root: Url,
  salt: S,
  seed: S,
  test: S,
  etwin_uri: Url,
  etwin_client_id: S,
  etwin_client_secret: S,
}

impl<S: AsRef<str>> fmt::Display for BackendConfig<S> {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    let db_url = serde_json::to_string(self.db_url.as_str()).unwrap();
    let db_user = serde_json::to_string(self.db_credentials.name.as_str()).unwrap();
    let db_password = serde_json::to_string(self.db_credentials.password.as_str()).unwrap();
    writeln!(f, "spring:")?;
    writeln!(f, "  datasource:")?;
    writeln!(f, "    url: {}", db_url)?;
    writeln!(f, "    driver-class-name: org.postgresql.Driver")?;
    writeln!(f, "    username: {}", db_user)?;
    writeln!(f, "    password: {}", db_password)?;
    writeln!(f, "  jooq:")?;
    writeln!(f, "    sql-dialect: postgres")?;
    writeln!(f, "  elasticsearch:")?;
    writeln!(f, "    url: http://[::1]:9200")?;
    writeln!(f, "  main:")?;
    writeln!(f, "    allow-bean-definition-overriding: true")?;
    writeln!(f, "server:")?;
    writeln!(f, "  port: {}", self.main_port)?;
    writeln!(f, "  backend: {}", &self.backend_root)?;
    writeln!(f, "  frontend: {}", &self.frontend_root)?;
    writeln!(f, "  salt: {}", &self.salt.as_ref())?;
    writeln!(f, "  seed: {}", &self.seed.as_ref())?;
    writeln!(f, "  test: {}", &self.test.as_ref())?;
    writeln!(f, "etwin:")?;
    writeln!(f, "  uri: {}", &self.etwin_uri)?;
    writeln!(f, "  client-id: {}", self.etwin_client_id.as_ref())?;
    writeln!(f, "  client-secret: {}", self.etwin_client_secret.as_ref())?;
    Ok(())
  }
}

pub fn get_nginx_config<U: LinuxUser, S: AsRef<str>>(channel: &NeoparcChannel<U, S>, commit_hash: &str) -> String {
  assert_eq!(commit_hash.len(), 40);
  let upstream_name = channel.name.as_ref().to_string().replace('.', "_");
  let access_log = channel.paths.logs.join("main.nginx.access.log");
  let error_log = channel.paths.logs.join("main.nginx.error.log");
  let static_dir = channel.paths.home.join("active/repo/frontend/build");

  let fallback_server = format!(
    r#"# Fallback server
server {{
  # HTTP port
  listen {fallback_port};
  listen [::]:{fallback_port};

  # Hide nginx version in `Server` header.
  server_tokens off;
  add_header Referrer-Policy same-origin;
  add_header X-Content-Type-Options nosniff;
  add_header X-XSS-Protection "1; mode=block";

  index index.html;

  root {fallback_dir};
}}
"#,
    fallback_port = channel.fallback_server.port,
    fallback_dir = channel.fallback_server.dir.to_str().unwrap(),
  );

  let upstream_server = format!(
    r#"# Define target of the reverse-proxy
upstream {upstream_name} {{
  server [::1]:{port};
  server [::1]:{fallback_port} backup;
  keepalive 8;
}}
"#,
    upstream_name = upstream_name,
    port = channel.main_port,
    fallback_port = channel.fallback_server.port,
  );

  let main_directives = format!(
    r#"  # Space-separated hostnames (wildcard * is accepted)
  server_name {domain};

  access_log {access_log};
  error_log {error_log};
  client_max_body_size 100M;

  root {static_dir};

  location ~* ^/(?:api)(?:/|$) {{
    # Upstream reference
    proxy_pass http://{upstream_name};

    # Configuration of the proxy
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection 'upgrade';
    proxy_set_header Host $host;
    proxy_set_header X-NginX-Proxy true;
    proxy_cache_bypass $http_upgrade;
    proxy_redirect off;
  }}

  location /static {{
    gzip_static on;
    add_header Cache-Control "public,max-age=31536000,immutable";
    etag off;
  }}

  location / {{
    try_files $uri /index.html;
    add_header Cache-Control "no-cache";
    add_header ETag "{git_commit}";
  }}
"#,
    domain = channel.domain.main.as_ref(),
    access_log = access_log.to_str().unwrap(),
    error_log = error_log.to_str().unwrap(),
    static_dir = static_dir.to_str().unwrap(),
    upstream_name = upstream_name,
    git_commit = commit_hash,
  );

  match channel.domain.cert.as_ref() {
    Some(https) => format!(
      r#"# This configuration sets a reverse proxy for a specific domain

{fallback_server}

{upstream_server}

# HTTPS
server {{
  # HTTPS port
  listen 443 ssl;
  listen [::]:443 ssl;

  ssl_certificate {cert};
  ssl_certificate_key {cert_key};

  # # HSTS (ngx_http_headers_module is required) (63072000 seconds)
  # add_header Strict-Transport-Security "max-age=63072000" always;

  {main_directives}
}}

# HTTP
server {{
  # HTTP port
  listen 80;
  listen [::]:80;
  server_name {domain};
  return 301 https://$host$request_uri;
}}

# www.
server {{
  listen 80;
  listen 443 ssl;
  listen [::]:80;
  listen [::]:443 ssl;

  server_name www.{domain};

  ssl_certificate {cert};
  ssl_certificate_key {cert_key};

  return 307 https://{domain}$request_uri;
}}

"#,
      fallback_server = fallback_server,
      upstream_server = upstream_server,
      main_directives = main_directives,
      domain = channel.domain.main.as_ref(),
      cert = https.fullchain_cert_path.to_str().unwrap(),
      cert_key = https.cert_key_path.to_str().unwrap(),
    ),
    None => format!(
      r#"# This configuration sets a reverse proxy for a specific domain

{fallback_server}

{upstream_server}

# HTTP
server {{
  # HTTP port
  listen 80;
  listen [::]:80;

  {main_directives}
}}

# www.
server {{
  listen 80;
  listen [::]:80;

  server_name www.{domain};

  return 307 http://{domain}$request_uri;
}}

"#,
      fallback_server = fallback_server,
      upstream_server = upstream_server,
      main_directives = main_directives,
      domain = channel.domain.main.as_ref(),
    ),
  }
}
#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
struct NeoparcService<S: AsRef<str> = String> {
  channel_name: S,
  backend_dir: PathBuf,
  user_name: S,
}

impl<S: AsRef<str>> fmt::Display for NeoparcService<S> {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    let gradlew = self.backend_dir.join("gradlew");
    write!(
      f,
      r#"[Unit]
Description=Neoparc ({channel_name})
After=network.target

[Service]
Type=simple
User={user_name}
LimitNOFILE=infinity
LimitNPROC=infinity
LimitCORE=infinity
Environment=PATH=/usr/local/sbin:/usr/local/bin:/usr/bin:/usr/bin/site_perl:/usr/bin/vendor_perl:/usr/bin/core_perl:/usr/bin:/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin
Restart=on-failure

WorkingDirectory={working_directory}
ExecStart={gradlew} :run

[Install]
WantedBy=multi-user.target
"#,
      channel_name = self.channel_name.as_ref(),
      user_name = self.user_name.as_ref(),
      working_directory = self.backend_dir.display(),
      gradlew = gradlew.display(),
    )
  }
}
