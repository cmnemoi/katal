use crate::etwin::channel::{
  ChannelBorgbase, ChannelDataDir, ChannelDomain, ChannelFallbackServer, ChannelPaths, ChannelPostgres, ChannelTarget,
};
use crate::pm2::{Pm2App, Pm2Ecosystem};
use crate::task::fs::{EnsureDir, EnsureDirSymlink, EnsureFile};
use crate::task::git::GitCheckout;
use crate::task::nginx::{NginxAvailableSite, NginxEnableSite};
use crate::task::nvm::NvmEnv;
use crate::task::systemd::{SystemdState, SystemdUnit};
use crate::task::{exec_as, AsyncFn, NamedTask, TaskName, TaskResult, TaskSuccess};
use async_trait::async_trait;
use asys::common::fs::FsHost;
use asys::linux::fs::{FileMode, LinuxFsHost, LinuxMetadata, ResolveError};
use asys::linux::user::{Gid, GroupRef, LinuxUser, LinuxUserHost, Uid, UserRef};
use asys::local_linux::LocalLinux;
use asys::{Command, ExecHost};
use chrono::Duration;
use serde::{Deserialize, Serialize};
use std::fmt;
use std::fmt::Debug;
use std::path::PathBuf;

const SKIP_BUILD: bool = false;
const SKIP_CHECKOUT: bool = false;

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct EternalfestAppTarget<S: AsRef<str> = String> {
  pub name: S,
  pub user_name: S,
  pub group_name: S,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct EternalfestApp<S: AsRef<str> = String> {
  name: S,
  user_name: S,
  group_name: S,
  uid: Uid,
  gid: Gid,
  home: PathBuf,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct EternalfestChannelTarget<S: AsRef<str> = String> {
  pub inner: ChannelTarget<S>,
  pub vault: EternalfestVault,
  pub etwin_uri: String,
  pub etwin_client_id: String,
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize, Serialize)]
pub struct EternalfestVault {
  pub secret_key: String,
  pub cookie_key: String,
  pub etwin_client_secret: String,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct EternalfestChannel<User: LinuxUser, S: AsRef<str> = String> {
  name: S,
  user: User,
  domain: ChannelDomain<S>,
  paths: ChannelPaths,
  main_port: u16,
  fallback_server: ChannelFallbackServer,
  postgres: ChannelPostgres,
  data_dir: ChannelDataDir,
  nvm: NvmEnv,
  vault: EternalfestVault,
  borg: Option<ChannelBorgbase>,
  etwin_uri: String,
  etwin_client_id: String,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct EternalfestReleaseTarget<S: AsRef<str> = String> {
  pub channel: EternalfestChannelTarget,
  pub repository: S,
  pub git_ref: String,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct EternalfestRelease<User: LinuxUser, S: AsRef<str> = String> {
  release_dir: PathBuf,
  repository: S,
  repo_dir: PathBuf,
  channel: EternalfestChannel<User, S>,
  git_ref: String,
}

#[derive(Serialize, Deserialize)]
pub struct DeployEternalfest<S: AsRef<str> = String> {
  pub pass: S,
  pub target: EternalfestReleaseTarget,
}

#[async_trait]
impl<'h, H: ExecHost, S: AsRef<str> + Send + Sync> AsyncFn<&'h H> for DeployEternalfest<S> {
  type Output = ();

  async fn run(&self, _host: &'h H) -> Self::Output {
    let input = DeployEternalfestAsRoot {
      target: self.target.clone(),
    };
    exec_as::<LocalLinux, DeployEternalfestAsRoot>(UserRef::ROOT, Some(self.pass.as_ref()), &input)
      .await
      .unwrap();
  }
}

impl<'h, H: 'h + ExecHost> NamedTask<&'h H> for DeployEternalfest {
  const NAME: TaskName = TaskName::new("etwin::DeployEternalfest");
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct DeployEternalfestAsRoot {
  target: EternalfestReleaseTarget,
}

#[async_trait]
impl<'h, H> AsyncFn<&'h H> for DeployEternalfestAsRoot
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  H::User: Debug + Clone + Send + Sync,
  H::Group: Debug + Send + Sync,
  <H as FsHost>::Metadata: LinuxMetadata,
{
  type Output = TaskSuccess<()>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let res = self.target.run(host).await;
    if let Err(e) = res {
      panic!("{:?}", e);
    }
    let res = res.unwrap();

    TaskSuccess {
      changed: res.changed,
      output: (),
    }
  }
}

impl<'h, H> NamedTask<&'h H> for DeployEternalfestAsRoot
where
  H: 'h + ExecHost + LinuxUserHost + LinuxFsHost,
  H::User: Debug + Clone + Send + Sync,
  H::Group: Debug + Send + Sync,
  <H as FsHost>::Metadata: LinuxMetadata,
{
  const NAME: TaskName = TaskName::new("eternalfest::DeployEternalfestAsRoot");
}

#[async_trait]
impl<'h, H> AsyncFn<&'h H> for EternalfestReleaseTarget
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  H::User: Debug + Clone + Send + Sync,
  H::Group: Debug + Send + Sync,
  <H as FsHost>::Metadata: LinuxMetadata,
{
  type Output = TaskResult<EternalfestRelease<H::User>, anyhow::Error>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let res = self.channel.run(host).await?;
    let channel: EternalfestChannel<H::User> = res.output;
    let mut changed = res.changed;

    let release_dir = channel.paths.releases.join(&self.git_ref);

    eprintln!("Create directory: {}", release_dir.display());
    changed = EnsureDir::new(release_dir.clone())
      .owner(UserRef::Id(channel.user.uid()))
      .group(GroupRef::Id(channel.user.gid()))
      .mode(FileMode::OWNER_ALL | FileMode::ALL_READ | FileMode::ALL_EXEC)
      .run(host)
      .await?
      .changed
      || changed;

    let repo_dir = release_dir.join("repo");

    let br = BuildEternalfestRelease {
      repo_dir: repo_dir.clone(),
      remote: self.repository.clone(),
      git_ref: self.git_ref.clone(),
      config: config::Config::from_channel(&channel),
    };

    if !SKIP_BUILD {
      changed = exec_as::<LocalLinux, _>(UserRef::Id(channel.user.uid()), None, &br)
        .await?
        .unwrap()
        .changed
        || changed;
    }

    let release = EternalfestRelease {
      release_dir,
      repository: self.repository.clone(),
      repo_dir,
      channel,
      git_ref: self.git_ref.clone(),
    };

    changed = (UpdateActiveRelease {
      release: release.clone(),
    })
    .run(host)
    .await?
    .changed
      || changed;

    Ok(TaskSuccess {
      changed,
      output: release,
    })
  }
}

#[async_trait]
impl<'h, H> AsyncFn<&'h H> for EternalfestChannelTarget
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  H::Group: Send + Sync,
  H::User: Send + Sync + Debug,
  <H as FsHost>::Metadata: LinuxMetadata,
{
  type Output = TaskResult<EternalfestChannel<H::User>, anyhow::Error>;

  async fn run(&self, host: &'h H) -> Self::Output {
    eprintln!("Eternalfest: Create channel");
    let res = self.inner.run(host).await?;

    eprintln!("Eternalfest: Channel ready");
    let output = EternalfestChannel {
      name: res.output.name,
      user: res.output.user,
      domain: res.output.domain.expect("Expected associated domain name"),
      paths: res.output.paths,
      main_port: res.output.main_port.expect("Expected main port"),
      fallback_server: res.output.fallback_server.expect("Expected fallback server"),
      postgres: res.output.postgres.expect("Expected Postgres database"),
      data_dir: res.output.data_dir.expect("Expected data directory"),
      nvm: res.output.nvm.expect("Expected NVM environment"),
      vault: self.vault.clone(),
      borg: res.output.borg,
      etwin_uri: self.etwin_uri.clone(),
      etwin_client_id: self.etwin_client_id.clone(),
    };

    Ok(TaskSuccess {
      changed: res.changed,
      output,
    })
  }
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct BuildEternalfestRelease {
  repo_dir: PathBuf,
  remote: String,
  git_ref: String,
  config: config::Config,
}

#[async_trait]
impl<'h, H: ExecHost + LinuxUserHost + LinuxFsHost> AsyncFn<&'h H> for BuildEternalfestRelease
where
  <H as FsHost>::Metadata: LinuxMetadata,
{
  type Output = Result<TaskSuccess<()>, ()>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let mut changed: bool;
    if !SKIP_CHECKOUT {
      let gc = GitCheckout {
        path: self.repo_dir.clone(),
        remote: self.remote.clone(),
        r#ref: self.git_ref.clone(),
      };

      eprintln!("Checkout repository: {:?} {:?}", self.remote, self.git_ref);
      let res = gc.run(host).await.unwrap();
      changed = res.changed;
    } else {
      changed = false;
    }

    {
      let cmd = Command::new("cargo").arg("--version");
      let has_cargo = host.try_exec(&cmd).unwrap().termination.success();
      if !has_cargo {
        let cmd = Command::new("rustup")
          .arg("toolchain")
          .arg("install")
          .arg("stable")
          .arg("--profile")
          .arg("minimal");
        host.exec(&cmd).unwrap();
        let cmd = Command::new("rustup").arg("default").arg("stable");
        host.exec(&cmd).unwrap();
      }
    }

    eprintln!("Install Node dependencies");
    let cmd = Command::new("yarn")
      .arg("install")
      .arg("--immutable")
      .current_dir(self.repo_dir.to_str().unwrap());
    host.exec(&cmd).unwrap();

    eprintln!("Build Node project");
    let cmd = Command::new("yarn")
      .arg("run")
      .arg("build:production")
      .current_dir(self.repo_dir.to_str().unwrap());
    host.exec(&cmd).unwrap();

    eprintln!("Build CLI");
    let cmd = Command::new("cargo")
      .arg("build")
      .arg("--package")
      .arg("eternalfest_cli")
      .arg("--bins")
      .arg("--release")
      .current_dir(self.repo_dir.to_str().unwrap());
    host.exec(&cmd).unwrap();

    eprintln!("Write backend config");
    changed = EnsureFile::new(self.repo_dir.join("eternalfest.toml"))
      .content(self.config.to_string())
      // .owner(app.uid)
      // .group(app.gid)
      .mode(FileMode::OWNER_READ | FileMode::OWNER_WRITE)
      .run(host)
      .await
      .unwrap()
      .changed
      || changed;

    Ok(TaskSuccess { changed, output: () })
  }
}

impl<'h, H: 'h + ExecHost + LinuxUserHost + LinuxFsHost> NamedTask<&'h H> for BuildEternalfestRelease
where
  <H as FsHost>::Metadata: LinuxMetadata,
{
  const NAME: TaskName = TaskName::new("etwin::BuildEternalfestRelease");
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize)]
pub struct UpdateActiveRelease<User: LinuxUser, S: AsRef<str> = String> {
  release: EternalfestRelease<User, S>,
}

#[async_trait]
impl<'h, H, S> AsyncFn<&'h H> for UpdateActiveRelease<H::User, S>
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Send + Sync,
  S: AsRef<str> + Send + Sync,
{
  type Output = TaskResult<(), anyhow::Error>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let uid = self.release.channel.user.uid();
    let gid = self.release.channel.user.gid();

    let service_name = format!("{}.service", self.release.channel.name.as_ref());

    let mut changed = false;
    let active_release_link = &self.release.channel.paths.active_release_link;
    let previous_release_link = &self.release.channel.paths.previous_release_link;
    let old_release_dir = {
      match host.resolve(active_release_link) {
        Ok(old) => Some(old),
        Err(ResolveError::NotFound) => None,
        Err(e) => return Err(e.into()),
      }
    };
    let new_release_dir = host.resolve(&self.release.release_dir)?;

    if let Some(old_release_dir) = old_release_dir {
      if old_release_dir != new_release_dir {
        eprintln!("Start to disable old release");
        // There is already an active release but it does not match the target
        // release: stop the old version before proceeding to free the channel
        // resources (database, port)
        EnsureDirSymlink::new(previous_release_link.clone())
          .points_to(old_release_dir.clone())
          .owner(uid)
          .group(gid)
          .mode(FileMode::ALL_READ | FileMode::ALL_EXEC)
          .run(host)
          .await?;

        SystemdUnit::new(&service_name)
          .state(SystemdState::Inactive)
          .run(host)
          .await?;
        eprintln!("Old release disabled");

        // if let Some(borg) = self.release.channel.borg.as_ref() {
        //   eprintln!("Creating archive before release");
        //   let backup_dir = self.release.channel.paths.home.join("backup");
        //   EnsureDir::new(backup_dir.clone()).run(host).await?;
        //   let example_file = backup_dir.join("etwin.pgdump");
        //   EnsureFile::new(example_file.clone()).content("Hey").run(host).await?;
        //
        //   changed = exec_as::<LocalLinux, _>(
        //     UserRef::Id(uid),
        //     None,
        //     &CreateBorgArchive::new_from_time(
        //       borg.password.clone(),
        //       borg.ssh_priv_key.clone(),
        //       borg.repo.clone(),
        //       backup_dir,
        //     ),
        //   )
        //   .await?
        //   .unwrap()
        //   .changed
        //     || changed;
        // } else {
        //   eprintln!("Skipping pre-release archive: no borg config");
        // }

        changed = true;
      }
    }

    // TODO: Trigger backup

    eprintln!("UpgradingDatabase");
    changed = exec_as::<LocalLinux, _>(
      UserRef::Id(uid),
      None,
      &UpgradeEternalfestDatabase {
        repo_dir: self.release.repo_dir.clone(),
      },
    )
    .await?
    .unwrap()
    .changed
      || changed;

    if let Some(borg) = self.release.channel.borg.as_ref() {
      eprintln!("Generating Borg archive script");
      let create_borg_archive = format!(
        r#"#!/usr/bin/env bash
set -eu
BACKUP_UUID=$(head -c 256 /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1)
UTC_TIME=$(date --utc --iso-8601=seconds | sed 's/[^0-9]/-/g' | sed 's/-00-00$//')
ARCHIVE_DIR="{channel_home}/tmp/${{UTC_TIME}}.${{BACKUP_UUID}}"
cd "{repo_dir}"
mkdir -p "${{ARCHIVE_DIR}}"
{app_dump} "${{ARCHIVE_DIR}}"
cd "${{ARCHIVE_DIR}}"
SSH_ASKPASS_REQUIRE=never BORG_PASSPHRASE={borg_pass} borg create --compression auto,zstd --rsh "ssh -i {priv_key}" "{borg_repo}::${{UTC_TIME}}" "."
cd {channel_home}
rm -rf "${{ARCHIVE_DIR}}"
"#,
        channel_home = self.release.channel.paths.home.display(),
        repo_dir = self.release.repo_dir.display(),
        app_dump = self.release.repo_dir.join("target/release/dump").display(),
        borg_pass = borg.password.clone(),
        priv_key = borg.ssh_priv_key.to_str().unwrap(),
        borg_repo = &borg.repo.0,
      );

      eprintln!("Writing Borg archive script");
      let archive_script = new_release_dir.join("archive.sh");
      changed = EnsureFile::new(archive_script.clone())
        .content(&create_borg_archive)
        .owner(uid)
        .group(gid)
        .mode(FileMode::OWNER_READ | FileMode::OWNER_EXEC)
        .run(host)
        .await?
        .changed
        || changed;

      eprintln!("Enable borgmatic timer");
      {
        let user = self.release.channel.user.name();
        let name = self.release.channel.name.as_ref();
        let service_name = format!("{}.archive.service", name);
        let timer_name = format!("{}.archive.timer", name);

        let systemd_dir = PathBuf::from("/etc/systemd/system");
        let service = format!(
          r#"[Unit]
Description=Backup to Borg ({name})

[Service]
Type=oneshot
User={user}
WorkingDirectory={cwd}

ExecStart="{archive_script}"
"#,
          name = name,
          user = user,
          cwd = self.release.channel.paths.home.display(),
          archive_script = archive_script.display(),
        );
        let timer = format!(
          r#"[Unit]
Description=Timer for Borg ({name})

[Timer]
# Try to renew once a day, between 01:00 and 08:00
OnCalendar=*-*-* 01:00:00
RandomizedDelaySec={interval}
# Run immediately if the previous start time was missed (e.g. due to the server being powered off)
Persistent=true

[Install]
WantedBy=timers.target
"#,
          interval = Duration::hours(7).num_seconds(),
          name = name,
        );
        changed = EnsureFile::new(systemd_dir.join(&service_name))
          .content(service)
          .owner(Uid::ROOT)
          .mode(FileMode::ALL_READ)
          .run(host)
          .await?
          .changed
          || changed;
        changed = EnsureFile::new(systemd_dir.join(&timer_name))
          .content(timer)
          .owner(Uid::ROOT)
          .mode(FileMode::ALL_READ)
          .run(host)
          .await?
          .changed
          || changed;
        changed = SystemdUnit::new(&timer_name)
          .enabled(true)
          .state(SystemdState::Active)
          .run(host)
          .await?
          .changed
          || changed;
      }
    }

    eprintln!("Generating PM2 ecosystem");
    let ecosystem = Pm2Ecosystem {
      apps: vec![{
        Pm2App::new(
          self.release.channel.name.as_ref().to_string(),
          self.release.repo_dir.join("packages/website/main/main.js"),
        )
        .env("NODE_ENV", "production")
        // .env("NVM_DIR", self.release.channel.app.home.join(".nvm").to_str().unwrap())
        .cwd(&self.release.repo_dir)
        .node_args(format!(
          "--experimental-wasm-modules --require {} --experimental-loader {}",
          self.release.repo_dir.join(".pnp.cjs").display(),
          self.release.repo_dir.join(".pnp.loader.mjs").display(),
        ))
        .interpreter("node@18.10.0")
      }],
    };

    let mut ecosystem = serde_json::to_string_pretty(&ecosystem).unwrap();
    ecosystem.push('\n');

    eprintln!("Writing PM2 ecosystem");
    changed = EnsureFile::new(new_release_dir.join("ecosystem.config.json"))
      .content(ecosystem)
      .owner(uid)
      .group(gid)
      .mode(FileMode::ALL_READ)
      .run(host)
      .await?
      .changed
      || changed;

    eprintln!("Generating Systemd unit");
    let service = EternalfestService::from_channel(&self.release.channel);
    eprintln!("Writing Systemd unit");
    changed = EnsureFile::new(PathBuf::from("/etc/systemd/system").join(&service_name))
      .content(service.to_string())
      .owner(Uid::ROOT)
      .mode(FileMode::ALL_READ)
      .run(host)
      .await?
      .changed
      || changed;

    eprintln!("Generating nginx config");
    let nginx_config = get_nginx_config(&self.release.channel);

    eprintln!("Marking release as active");
    EnsureDirSymlink::new(active_release_link.clone())
      .points_to(new_release_dir.clone())
      .owner(uid)
      .group(gid)
      .mode(FileMode::ALL_READ | FileMode::ALL_EXEC)
      .run(host)
      .await?;

    eprintln!("Starting service");
    changed = SystemdUnit::new(&service_name)
      .enabled(true)
      .state(SystemdState::Active)
      .run(host)
      .await?
      .changed
      || changed;

    eprintln!("Writing nginx config");
    NginxAvailableSite::new(self.release.channel.name.as_ref(), nginx_config)
      .run(host)
      .await?;

    eprintln!("Enabling nginx config");
    NginxEnableSite::new(self.release.channel.name.as_ref())
      .run(host)
      .await?;

    Ok(TaskSuccess { changed, output: () })
  }
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct UpgradeEternalfestDatabase {
  repo_dir: PathBuf,
}

#[async_trait]
impl<'h, H> AsyncFn<&'h H> for UpgradeEternalfestDatabase
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
{
  type Output = Result<TaskSuccess<()>, ()>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let cmd = Command::new("yarn")
      .arg("run")
      .arg("db:upgrade")
      .current_dir(self.repo_dir.to_str().unwrap());
    host.exec(&cmd).unwrap();
    Ok(TaskSuccess {
      changed: true,
      output: (),
    })
  }
}

impl<'h, H> NamedTask<&'h H> for UpgradeEternalfestDatabase
where
  H: 'h + ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
{
  const NAME: TaskName = TaskName::new("eternalfest::UpgradeEternalfestDatabase");
}

mod config {
  use crate::etwin::eternalfest::EternalfestChannel;
  use asys::linux::user::LinuxUser;
  use serde::{Deserialize, Serialize};
  use std::fmt;
  use url::Url;

  #[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
  pub struct Config {
    pub eternalfest: EternalfestConfig,
    pub db: DbConfig,
    pub data: DataConfig,
    pub etwin: EtwinConfig,
  }

  impl Config {
    pub fn from_channel(channel: &EternalfestChannel<impl LinuxUser>) -> Self {
      let protocol = if channel.domain.cert.is_some() { "https" } else { "http" };
      let uri = format!("{}://{}", protocol, channel.domain.main.as_str());
      Self {
        eternalfest: EternalfestConfig {
          http_port: channel.main_port,
          external_uri: Url::parse(&uri).unwrap(),
          secret: channel.vault.secret_key.to_string(),
          cookie_key: channel.vault.cookie_key.to_string(),
        },
        db: DbConfig {
          host: channel.postgres.host.clone(),
          port: channel.postgres.port,
          name: channel.postgres.name.clone(),
          admin_user: channel.postgres.admin.name.to_string(),
          admin_password: channel.postgres.admin.password.clone(),
          user: channel.postgres.main.name.to_string(),
          password: channel.postgres.main.password.clone(),
        },
        data: DataConfig {
          root: channel.data_dir.file_uri().to_string(),
        },
        etwin: EtwinConfig {
          uri: channel.etwin_uri.clone(),
          oauth_client_id: channel.etwin_client_id.clone(),
          oauth_client_secret: channel.vault.etwin_client_secret.clone(),
        },
      }
    }
  }

  impl fmt::Display for Config {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
      writeln!(f, "{}", toml::to_string_pretty(&self).unwrap())
    }
  }

  #[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
  pub struct EternalfestConfig {
    pub http_port: u16,
    pub external_uri: Url,
    pub secret: String,
    pub cookie_key: String,
  }

  #[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
  pub struct DbConfig {
    pub host: String,
    pub port: u16,
    pub name: String,
    pub admin_user: String,
    pub admin_password: String,
    pub user: String,
    pub password: String,
  }

  #[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
  pub struct DataConfig {
    /// File URI, absolute or relative to the config file
    pub root: String,
  }

  #[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
  pub struct EtwinConfig {
    pub uri: String,
    pub oauth_client_id: String,
    pub oauth_client_secret: String,
  }
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
struct EternalfestService<S: AsRef<str> = String> {
  channel_name: S,
  user: S,
  nvm_dir: PathBuf,
  pm2_dir: PathBuf,
  pid_file: PathBuf,
  ecosystem_file: PathBuf,
  working_dir: PathBuf,
}

impl EternalfestService<String> {
  pub fn from_channel<User, S>(channel: &EternalfestChannel<User, S>) -> Self
  where
    User: LinuxUser,
    S: AsRef<str>,
  {
    Self {
      channel_name: channel.name.as_ref().to_string(),
      user: channel.user.name().to_string(),
      nvm_dir: channel.nvm.home.clone(),
      pid_file: channel.paths.home.join(".pm2/pm2.pid"),
      pm2_dir: channel.paths.home.join(".pm2"),
      ecosystem_file: channel.paths.home.join("active/ecosystem.config.json"),
      working_dir: channel.paths.home.join("active/repo"),
    }
  }
}

impl<S: AsRef<str>> fmt::Display for EternalfestService<S> {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    write!(
      f,
      r#"[Unit]
Description=PM2 for Eternalfest ({channel})
Documentation=https://pm2.keymetrics.io/
After=network.target
Requires=postgresql.service

[Service]
Type=forking
User={user}
LimitNOFILE=infinity
LimitNPROC=infinity
LimitCORE=infinity
Environment=PATH=/usr/local/sbin:/usr/local/bin:/usr/bin:/usr/bin/site_perl:/usr/bin/vendor_perl:/usr/bin/core_perl:/usr/bin:/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin
Environment=PM2_HOME={pm2_dir}
Environment=NVM_DIR={nvm_dir}
PIDFile={pid_file}
Restart=on-failure

WorkingDirectory={working_dir}
ExecStart=/usr/bin/pm2 start {ecosystem}
ExecReload=/usr/bin/pm2 reload {ecosystem}
ExecStop=/usr/bin/pm2 stop {ecosystem}

[Install]
WantedBy=multi-user.target
"#,
      channel = self.channel_name.as_ref(),
      user = self.user.as_ref(),
      pm2_dir = self.pm2_dir.display(),
      nvm_dir = self.nvm_dir.display(),
      pid_file = self.pid_file.display(),
      ecosystem = self.ecosystem_file.display(),
      working_dir = self.working_dir.display(),
    )
  }
}

pub fn get_nginx_config<S: AsRef<str>>(channel: &EternalfestChannel<impl LinuxUser, S>) -> String {
  let upstream_name = channel.name.as_ref().to_string().replace('.', "_");
  let access_log = channel.paths.logs.join("main.nginx.access.log");
  let error_log = channel.paths.logs.join("main.nginx.error.log");
  let static_dir = channel.paths.home.join("active/repo/packages/website/app/browser");

  let fallback_server = format!(
    r#"# Fallback server
server {{
  # HTTP port
  listen {fallback_port};
  listen [::]:{fallback_port};

  # Hide nginx version in `Server` header.
  server_tokens off;
  add_header Referrer-Policy same-origin;
  add_header X-Content-Type-Options nosniff;
  add_header X-XSS-Protection "1; mode=block";

  index index.html;

  root {fallback_dir};
}}
"#,
    fallback_port = channel.fallback_server.port,
    fallback_dir = channel.fallback_server.dir.to_str().unwrap(),
  );

  let upstream_server = format!(
    r#"# Define target of the reverse-proxy
upstream {upstream_name} {{
  server [::1]:{port};
  server [::1]:{fallback_port} backup;
  keepalive 8;
}}
"#,
    upstream_name = upstream_name,
    port = channel.main_port,
    fallback_port = channel.fallback_server.port,
  );

  let main_directives = format!(
    r#"  # Space-separated hostnames (wildcard * is accepted)
  server_name {domain};

  access_log {access_log};
  error_log {error_log};
  client_max_body_size 100M;

  root {static_dir};

  location / {{
    try_files $uri @eternalfestproxy;
    gzip_static on;
  }}

  location @eternalfestproxy {{
    # Upstream reference
    proxy_pass http://{upstream_name};

    # Configuration of the proxy
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection 'upgrade';
    proxy_set_header Host $host;
    proxy_set_header X-NginX-Proxy true;
    proxy_cache_bypass $http_upgrade;
    proxy_redirect off;
  }}
"#,
    domain = channel.domain.main.as_ref(),
    access_log = access_log.to_str().unwrap(),
    error_log = error_log.to_str().unwrap(),
    static_dir = static_dir.to_str().unwrap(),
    upstream_name = upstream_name,
  );

  match channel.domain.cert.as_ref() {
    Some(https) => format!(
      r#"# This configuration sets a reverse proxy for a specific domain

{fallback_server}

{upstream_server}

# HTTPS
server {{
  # HTTPS port
  listen 443 ssl;
  listen [::]:443 ssl;

  ssl_certificate {cert};
  ssl_certificate_key {cert_key};

  # # HSTS (ngx_http_headers_module is required) (63072000 seconds)
  # add_header Strict-Transport-Security "max-age=63072000" always;

  {main_directives}
}}

# HTTP
server {{
  # HTTP port
  listen 80;
  listen [::]:80;
  server_name {domain};
  return 301 https://$host$request_uri;
}}

# www.
server {{
  listen 80;
  listen 443 ssl;
  listen [::]:80;
  listen [::]:443 ssl;

  server_name www.{domain};

  ssl_certificate {cert};
  ssl_certificate_key {cert_key};

  return 307 https://{domain}$request_uri;
}}

"#,
      fallback_server = fallback_server,
      upstream_server = upstream_server,
      main_directives = main_directives,
      domain = channel.domain.main.as_ref(),
      cert = https.fullchain_cert_path.to_str().unwrap(),
      cert_key = https.cert_key_path.to_str().unwrap(),
    ),
    None => format!(
      r#"# This configuration sets a reverse proxy for a specific domain

{fallback_server}

{upstream_server}

# HTTP
server {{
  # HTTP port
  listen 80;
  listen [::]:80;

  {main_directives}
}}

# www.
server {{
  listen 80;
  listen [::]:80;

  server_name www.{domain};

  return 307 http://{domain}$request_uri;
}}

"#,
      fallback_server = fallback_server,
      upstream_server = upstream_server,
      main_directives = main_directives,
      domain = channel.domain.main.as_ref(),
    ),
  }
}
