/// Represents a target state for an item of type `Item`.
///
/// It must extend the `Default` and `From<Item>` traits with the following
/// semantics:
/// - The default value should create a target accepting any item state.
/// - The conversion from the item should create a target accepting only the
///   provided state.
pub trait Target<Item: ?Sized>: Default + for<'i> From<&'i Item> {
  /// Test if the provided item matches this target state.
  fn test(&self, item: &Item) -> bool;
}

// #[derive(Copy, Clone, Ord, PartialOrd, Eq, PartialEq, Hash)]
// pub enum Target1<Item: Clone> {
//   One(Item),
//   Any,
// }
//
// impl<Item: Clone> Target1<Item> {
//   pub const ANY: Self = Self::Any;
// }
//
// impl<Item: Clone> Default for Target1<Item> {
//   fn default() -> Self {
//     Self::Any
//   }
// }
//
// impl<'i, Item: Clone> From<&'i Item> for Target1<Item> {
//   fn from(item: &'i Item) -> Self {
//     Self::One(item.clone())
//   }
// }
//
// impl<Item: Clone> From<Item> for Target1<Item> {
//   fn from(item: Item) -> Self {
//     Self::One(item)
//   }
// }
