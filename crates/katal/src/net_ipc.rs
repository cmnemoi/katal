use serde::{Deserialize, Serialize};
use std::convert::TryFrom;
use std::marker::PhantomData;
use std::net::{Ipv6Addr, SocketAddrV6};
use tokio::io::{AsyncReadExt, AsyncWriteExt};
use tokio::net::tcp::{OwnedReadHalf, OwnedWriteHalf};
use tokio::net::{TcpListener, TcpStream};

pub struct NetIpcServer<T> {
  listener: TcpListener,
  phantom: PhantomData<T>,
}

impl<T> NetIpcServer<T> {
  pub async fn new() -> Result<(Self, String), anyhow::Error> {
    let addr = SocketAddrV6::new(Ipv6Addr::LOCALHOST, 0, 0, 0);
    let listener = TcpListener::bind(addr).await?;
    let name = format!("{}", listener.local_addr()?.port());
    let srv = Self {
      listener,
      phantom: PhantomData,
    };
    Ok((srv, name))
  }

  pub async fn accept<R>(&self) -> Result<(NetIpcReceiver<T>, NetIpcSender<R>), anyhow::Error> {
    let (stream, _) = self.listener.accept().await?;
    let (rx, tx) = stream.into_split();
    Ok((NetIpcReceiver::new(rx), NetIpcSender::new(tx)))
  }
}

pub struct NetIpcReceiver<T> {
  reader: OwnedReadHalf,
  phantom: PhantomData<T>,
  buffer: Vec<u8>,
}

impl<T> NetIpcReceiver<T> {
  fn new(reader: OwnedReadHalf) -> Self {
    Self {
      reader,
      phantom: PhantomData,
      buffer: Vec::new(),
    }
  }
}

type PayloadSize = u32;

impl<T> NetIpcReceiver<T>
where
  T: for<'de> Deserialize<'de>,
{
  /// Some: Received value
  /// None: End of stream
  pub async fn recv(&mut self) -> Result<Option<T>, anyhow::Error> {
    loop {
      if let Some(msg) = self.parse_next()? {
        return Ok(Some(msg));
      }
      let read_count = self.reader.read_buf(&mut self.buffer).await?;
      if read_count == 0 {
        // Connection closed by remote
        if self.buffer.is_empty() {
          return Ok(None);
        } else {
          return Err(anyhow::Error::msg("connection reset by peer"));
        }
      }
    }
  }

  fn parse_next(&mut self) -> Result<Option<T>, anyhow::Error> {
    let header_size = std::mem::size_of::<PayloadSize>();
    if self.buffer.len() < header_size {
      // Incomplete: missing payload size
      return Ok(None);
    }
    let payload_size = PayloadSize::from_le_bytes([self.buffer[0], self.buffer[1], self.buffer[2], self.buffer[3]]);
    let payload_size = usize::try_from(payload_size).unwrap();
    let full_size = header_size.checked_add(payload_size).expect("PayloadSizeOverflow");
    if self.buffer.len() < full_size {
      // Incomplete: missing payload
      return Ok(None);
    }
    let payload = &self.buffer[header_size..full_size];
    let msg: T = bincode::deserialize(payload)?;
    let mut i: usize = 0;
    self.buffer.retain(|_| {
      let keep = i >= full_size;
      i += 1;
      keep
    });
    Ok(Some(msg))
  }
}

pub struct NetIpcSender<T> {
  writer: OwnedWriteHalf,
  phantom: PhantomData<T>,
}

impl<T> NetIpcSender<T> {
  fn new(writer: OwnedWriteHalf) -> Self {
    Self {
      writer,
      phantom: PhantomData,
    }
  }

  pub async fn connect<R>(server_name: &str) -> Result<(NetIpcReceiver<R>, Self), anyhow::Error> {
    let port: u16 = server_name.parse()?;
    let addr = SocketAddrV6::new(Ipv6Addr::LOCALHOST, port, 0, 0);
    let stream = TcpStream::connect(addr).await?;
    let (rx, tx) = stream.into_split();
    Ok((NetIpcReceiver::new(rx), NetIpcSender::new(tx)))
  }
}

impl<T> NetIpcSender<T>
where
  T: Serialize,
{
  pub async fn send(&mut self, msg: &T) -> Result<(), anyhow::Error> {
    let bytes = bincode::serialize(msg)?;
    let payload_size = PayloadSize::try_from(bytes.len())?;
    self.writer.write_all(&payload_size.to_le_bytes()).await?;
    self.writer.write_all(&bytes).await?;
    Ok(())
  }
}
