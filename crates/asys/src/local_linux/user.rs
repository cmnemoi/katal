use crate::common::user::UserHost;
use crate::linux::user::{
  CreateGroupError, GetUserGroupsError, Gid, LinuxGroup, LinuxUser, LinuxUserHost, TryGroupFromIdError,
  TryGroupFromNameError, TryUserFromNameError, Uid, UpdateGroupError,
};
use crate::local_linux::LocalLinux;
use crate::{Command, ExecHost};
use std::ffi::CString;
use std::path::PathBuf;

impl UserHost for LocalLinux {}

impl LinuxUserHost for LocalLinux {
  type Group = LocalLinuxGroup;
  type User = LocalLinuxUser;

  fn create_group(&self, name: &str, gid: Option<Gid>) -> Result<(), CreateGroupError> {
    let mut cmd = Command::new("groupadd");
    if let Some(gid) = gid {
      cmd = cmd.arg("--gid").arg(gid.to_string());
    }
    cmd = cmd.arg(name);
    match self.exec(&cmd).map(drop) {
      Ok(()) => Ok(()),
      Err(e) => Err(CreateGroupError::Other(e.to_string())),
    }
  }

  fn update_group(&self, name: &str, gid: Option<Gid>) -> Result<(), UpdateGroupError> {
    let mut cmd = Command::new("groupmod");
    if let Some(gid) = gid {
      cmd = cmd.arg("--gid").arg(gid.to_string());
    }
    cmd = cmd.arg(name);
    match self.exec(&cmd).map(drop) {
      Ok(()) => Ok(()),
      Err(e) => Err(UpdateGroupError::Other(e.to_string())),
    }
  }

  fn try_group_from_name(&self, name: &str) -> Result<Option<Self::Group>, TryGroupFromNameError> {
    match nix::unistd::Group::from_name(name) {
      Ok(Some(nix)) => Ok(Some(LocalLinuxGroup { nix })),
      Ok(None) => Ok(None),
      Err(e) => Err(TryGroupFromNameError::Other(e.to_string())),
    }
  }

  fn try_group_from_id(&self, gid: Gid) -> Result<Option<Self::Group>, TryGroupFromIdError> {
    match nix::unistd::Group::from_gid(gid.into()) {
      Ok(Some(nix)) => Ok(Some(LocalLinuxGroup { nix })),
      Ok(None) => Ok(None),
      Err(e) => Err(TryGroupFromIdError::Other(e.to_string())),
    }
  }

  fn try_user_from_name(&self, name: &str) -> Result<Option<Self::User>, TryUserFromNameError> {
    match nix::unistd::User::from_name(name) {
      Ok(Some(nix)) => Ok(Some(LocalLinuxUser { nix })),
      Ok(None) => Ok(None),
      Err(e) => Err(TryUserFromNameError::Other(e.to_string())),
    }
  }

  fn get_user_groups(&self, user_name: &str, extra_group: Gid) -> Result<Vec<Gid>, GetUserGroupsError> {
    let user_name = match CString::new(user_name) {
      Ok(user_name) => user_name,
      Err(e) => return Err(GetUserGroupsError::Other(e.to_string())),
    };
    match nix::unistd::getgrouplist(user_name.as_c_str(), extra_group.into()) {
      Ok(groups) => Ok(groups.into_iter().map(Gid::from).collect()),
      Err(e) => Err(GetUserGroupsError::Other(e.to_string())),
    }
  }
}

#[derive(Debug, Clone, PartialEq)]
pub struct LocalLinuxGroup {
  nix: nix::unistd::Group,
}

impl LinuxGroup for LocalLinuxGroup {
  fn gid(&self) -> Gid {
    Gid::from(self.nix.gid)
  }

  fn name(&self) -> &str {
    &self.nix.name
  }
}

#[derive(Debug, Clone, PartialEq)]
pub struct LocalLinuxUser {
  nix: nix::unistd::User,
}

impl LinuxUser for LocalLinuxUser {
  fn name(&self) -> &str {
    &self.nix.name
  }

  fn uid(&self) -> Uid {
    Uid::from(self.nix.uid)
  }

  fn gid(&self) -> Gid {
    Gid::from(self.nix.gid)
  }

  fn shell(&self) -> PathBuf {
    self.nix.shell.clone()
  }

  fn home(&self) -> PathBuf {
    self.nix.dir.clone()
  }
}
