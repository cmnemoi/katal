use crate::{Command, CommandOutput, ExecHost, Host, NixHost};
use crossbeam::thread::{Scope, ScopedJoinHandle};
use std::io::Write;
use std::process::Stdio;

pub mod fs;
pub mod user;

/// Local Linux Host.
pub struct LocalLinux;

impl Host for LocalLinux {}

impl ExecHost for LocalLinux {
  fn try_exec(&self, command: &Command) -> Result<CommandOutput, anyhow::Error> {
    let mut cmd = std::process::Command::new(command.get_program());
    cmd.args(command.get_args());
    if let Some(d) = command.get_current_dir() {
      cmd.current_dir(d);
    }
    let env = command.get_env();
    if !env.inherit {
      cmd.env_clear();
    }
    for (k, v) in env.vars.iter() {
      if let Some(v) = v {
        cmd.env(k, v);
      } else {
        cmd.env_remove(k);
      }
    }

    let input = command.get_stdin();
    cmd.stdin(if input.is_empty() {
      Stdio::null()
    } else {
      Stdio::piped()
    });
    cmd.stdout(Stdio::piped());
    cmd.stderr(Stdio::piped());
    let mut child = cmd.spawn()?;
    crossbeam::thread::scope(move |s: &Scope| -> Result<CommandOutput, anyhow::Error> {
      let thread: Option<ScopedJoinHandle<()>> = if input.is_empty() {
        None
      } else {
        let stdin = child.stdin.take();
        let mut stdin = match stdin {
          Some(stdin) => stdin,
          None => return Err(anyhow::Error::msg("Failed to open stdin")),
        };
        let thread = s.spawn(move |_| {
          stdin.write_all(input).expect("Failed to write to stdin");
        });
        Some(thread)
      };
      let out = child.wait_with_output()?;
      if let Some(thread) = thread {
        thread.join().unwrap();
      }
      Ok(CommandOutput::from(out))
    })
    .unwrap()
  }
}

impl NixHost for LocalLinux {}
